Software
========

Voici la partie Software du projet GiveMeOne.

Ici se trouve tout le nécessaire pour comprendre les fonctionnalités des
armoires GMO.

Pour ne pas te perdre tu dois savoir que le code est séparé en 4 grandes
parties :

 - Le code du **serveur** constitué d'un unique fichier nommé **serveur.py** et écrit
   en `Python`
 - **Les modules** qui regroupe la génération des pages web et l'accès aux
   périphériques et à la base de donnée écrit également en Python
 - Le dossier **template** où l'on peut trouver les pages de l'administration, de
   l'interface utilisateur, de l'utilitaire d'installation. Tout ceci est écrit
   avec l'aide de `Jinja2` et le tryptique habituel du web
   (`HTML` - `CSS` - `Javascript`)
 - Le dossier static qui regroupe également des fichiers contenant du code
   `HTML` - `CSS` - `Javascript`
 - Et enfin 2 fichiers permettants de stocker des informations : **param.json** et
   **gmo.db**

Pour conclure voici un petit graphique résumant les interactions entre les
différents dossiers de :doc:`software`.

.. graphviz::

    digraph {

       "serveur" -> "blueprints";
       "serveur" -> "GMO";
       "blueprints" -> "templates";
       "blueprints" -> "GMO";
       "templates" -> "static";
       "GMO" -> "bdd.db";
       "GMO" -> "param.json";

     }

Dans la suite de cette page vous trouverez des informations complémentaires sur
chacun des dossiers évoqué ci-dessous.

Serveur
-------

Tout commence avec le :doc:`serveur` qui appelle tous les modules nécessaires
au bon fonctionnement de l'armoire. Il agit comme le coeur de l'application.

Modules
-------

Dans ce répertoires se trouvent les modules qui sont répartis en 2 dossiers :

 - Le premier, :doc:`blueprints`, regroupe tous les modules générant les
   interfaces utilisateurs.
 - Le second, :doc:`GMO`, rassemble tous les modules dialogant avec les
   périphériques, et regroupant l'intelligence du projet.

Templates
---------

Le dossier :doc:`template` regroupe les différentes pages web et est appelé par
le :doc:`blueprints` correspondant à chaque page.

Le travail est ainsi partagé entre les deux dossiers :

:doc:`blueprints` traite l'information et passe des variables à :doc:`template`
qui met en forme l'information afin de la présenter à l'utilisateur.

Static
------

Enfin les templates peuvent être amené à intégrer des fichiers provenant de
:doc:`static` (du CSS, JS, images, ...)

Le reste
--------

Les fichiers :doc:`gmo.db` et :doc:`param.json` ne sont eux pas visibles dans le
dépot car généré lors du premier lancement de :doc:`serveur`.

Documentation Software
----------------------

.. toctree::
   :maxdepth: 2

   GMO
   arduino
   blueprints
   serveur
   template
   static
   param.json
   gmo.db

.. graphviz::

   digraph {

      "serveur" -> "blueprints.ui.index";
      "serveur" -> "blueprints.ui.get_product";
      "serveur" -> "blueprints.ui.product_issue";


      "serveur" -> "blueprints.common.api";
      "serveur" -> "blueprints.common.get_files";

      "serveur" -> "blueprints.dashboard.main";
      "serveur" -> "blueprints.dashboard.produit";
      "serveur" -> "blueprints.dashboard.categorie";
      "serveur" -> "blueprints.dashboard.statistique";
      "serveur" -> "blueprints.dashboard.timeline";
      "serveur" -> "blueprints.dashboard.user";

      "serveur" -> "GMO.rfid";
      "serveur" -> "GMO.user";

      "blueprints.dashboard.categorie" -> "GMO.categorie";
      "blueprints.dashboard.main" -> "GMO.config";
      "blueprints.dashboard.main" -> "GMO.product";
      "blueprints.dashboard.produit" -> "GMO.product";
      "blueprints.dashboard.produit" -> "GMO.big_brother";
      "blueprints.dashboard.statistique" -> "GMO.product";
      "blueprints.dashboard.statistique" -> "GMO.user";
      "blueprints.dashboard.timeline" -> "GMO.product";
      "blueprints.dashboard.user" -> "GMO.user";

      "blueprints.common.api" -> "GMO.user";
      "blueprints.common.api" -> "GMO.product";
      "blueprints.common.api" -> "GMO.categorie";
      "blueprints.common.get_files" -> "GMO.categorie";
      "blueprints.common.get_files" -> "GMO.user";

      "blueprints.ui.get_product" -> "GMO.product";
      "blueprints.ui.get_product" -> "GMO.casier";
      "blueprints.ui.index" -> "GMO.categorie";

      "GMO.camera" -> "GMO.config";
      "GMO.product" -> "GMO.bdd";
      "GMO.product" -> "GMO.big_brother";
      "GMO.product" -> "GMO.casier";
      "GMO.rfid" -> "GMO.config";
      "GMO.user" -> "GMO.bdd";
      "GMO.user" -> "GMO.camera";
      "GMO.user" -> "GMO.rfid";
      "GMO.armoire" -> "GMO.bdd";
      "GMO.big_brother" -> "GMO.bdd"
      "GMO.casier" -> "GMO.bdd"
      "GMO.categorie" -> "GMO.bdd"


      subgraph "cluster_blueprints_ui" { label="blueprints.ui"; "blueprints.ui.index"; "blueprints.ui.get_product"; "blueprints.ui.product_issue";}
      subgraph "cluster_blueprints_common" { label="blueprints.common"; "blueprints.common.api"; "blueprints.common.get_files";}
      subgraph "cluster_blueprints_dashboard" { label="blueprints.dashboard"; "blueprints.dashboard.main"; "blueprints.dashboard.produit"; "blueprints.dashboard.categorie"; "blueprints.dashboard.statistique"; "blueprints.dashboard.timeline"; "blueprints.dashboard.user";}
      subgraph "cluster_GMO" { label="GMO"; "GMO.categorie"; "GMO.casier"; "GMO.big_brother"; "GMO.armoire"; "GMO.rfid"; "GMO.user"; "GMO.bdd"; "GMO.config"; "GMO.product"; "GMO.camera";}
   }
