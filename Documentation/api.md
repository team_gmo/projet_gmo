Guideline API
=============

L'interface d'administration n'est disponible que sur une armoire maître
En cas de coupure du lien avec armoire maître, l'armoire esclave passe en mode hs.

Au démarrage on choisit d'être une armoire esclave donc une seul bdd et full requête
vers l'armoire maître. ==> **SIMPLE**

Ce qu'une armoire aurait en BDD :
Ses casiers


Cas d'utilisation
-----------------

1. Un utilisateur s'enregistre dans l'armoire maître
    * Toutes les armoires esclaves reçoivent la data

2. Un utilisateur s'enregistre dans une armoire esclave
    * Envoie de la data à l'armoire maîtresse
    * Execution du **cas 1**

3. Un utilisateur souhaite emprunter un produit dans une armoire mais il n'est plus disponible
    * L'armoire contact les autres armoires dans l'ordre choisis par l'admin
    * L'armoire réserve le produit pour l'utilisateur

4. Un utilisateur souhaite retirer un produit réservé
    * Il présente sa carte à l'armoire concernée
    * Une pop'up l'informe qu'il doit retiré le produit réservé avant toute autre action
    * Il valide
        * Le casier concerné s'ouvre
    * Il ne valide pas
        * Retour au menu sélection de categorie
        * L'objet n'est plus réservé

5. L'administrateur ajoute un nouveau produit  (**disponible uniquement sur armoire maitre**)
    * Transmission de l'information à toutes les armoires


Liste des Routes (Api disponibles)
----------------------------------

* Un utilisateur réserve un produit (autre armoire ?)
http://192.168.144.1/api/book/<id_cat>/<id_user>

* Ajout/Modif/Supp utilisateur
http://192.168.144.1/api/add_user/<id_user>/<nom>/<prenom>
http://192.168.144.1/api/update_user/<id_user>/<nom>/<prenom>
http://192.168.144.1/api/delete_user/<id_user>

* Affiche toutes les catégories existantes
http://192.168.144.1/api/get_categories/

* Assigne un produit à un utilisateur dans une certaine armoire
http://192.168.144.1/api/user_get_product/<id_user>/<id_cat>/<id_armoire>

* Un produit est rendu dans une armoire
http://192.168.144.1/api/user_give_back_product/<id_produit>/<id_armoire>
