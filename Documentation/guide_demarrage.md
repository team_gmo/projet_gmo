Guide démarrage
===============

Initialisation
--------------

Synchronisation inter lignes
----------------------------

1. Fermer tous les casiers en commencant par celui le plus en bas à gauche
2. Fermer les autres casiers de la ligne dans l'ordre (démarrage à gauche)
3. Répeter l'opération ligne par ligne en remontant
4. NE PAS PLACER L'INTERFACE DE CONTROLE SUR UN CASIER EN BOUT DE LIGNE !

Synchronisation inter armoire
-----------------------------

1. Définir une armoire maitre **ou** définir comme armoire maitre
2. Syncronisation des bdd avec l'armoire maitre
