Package Dashboard
=================

Module accueil
----------------

.. automodule:: blueprints.dashboard.accueil
    :members:
    :undoc-members:
    :show-inheritance:

Module armoire
----------------

.. automodule:: blueprints.dashboard.armoire
    :members:
    :undoc-members:
    :show-inheritance:

Module categorie
----------------

.. automodule:: blueprints.dashboard.categorie
    :members:
    :undoc-members:
    :show-inheritance:

Module main
-----------

.. automodule:: blueprints.dashboard.main
    :members:
    :undoc-members:
    :show-inheritance:

Module produit
--------------

.. automodule:: blueprints.dashboard.produit
    :members:
    :undoc-members:
    :show-inheritance:

Module statistique
------------------

.. automodule:: blueprints.dashboard.statistique
    :members:
    :undoc-members:
    :show-inheritance:

Module timeline
----------------

.. automodule:: blueprints.dashboard.timeline
    :members:
    :undoc-members:
    :show-inheritance:

Module user
-----------

.. automodule:: blueprints.dashboard.user
    :members:
    :undoc-members:
    :show-inheritance:
