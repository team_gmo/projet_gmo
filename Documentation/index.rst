.. GMO documentation master file, created by
   sphinx-quickstart on Wed Nov 22 17:25:03 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation du projet GMO
==============================================

Bienvenue dans la documentation du projet GMO.

Vous trouverez dans ces différentes pages toute la documentation nécessaire
afin de reprendre le projet.

Afin de faciliter au maximum la compréhension et de faciliter les futures
recherches, voici une brève explication de l'architecture du projet.

Pour commencer
--------------

La documentation est découpée en trois grandes parties :

  * Partie mécanique
  * Partie software
  * Partie électronique

Mécanique
+++++++++

Dans cette partie sont détaillées les différentes étapes pour réaliser une armoire
de la même façon que nous l'avons fait.

.. toctree::
   :maxdepth: 3

   mecanique


Software
++++++++

Dans cette partie est documenté tout le code developpé, on y retrouve le
package GMO et les sites web utilisateur et administrateur.

.. toctree::
   :maxdepth: 2

   software


Électronique
++++++++++++

Dans cette partie sont décrits l'électronique utilisé et les PCB développés


.. toctree::
   :maxdepth: 3

   routage

Aide
----




Si vous ne trouvez pas l'information que vous cherchez, regardez sur l'index
ou essayez de la trouver avec la foncion de recherche.

* :ref:`genindex`
* :ref:`search`
