Mécanique
=========

Voici un petit pdf qui explique les outils utilisés et comment les utiliser
pour produit les mêmes pièces que nous (bois / plastique / impression 3d)

Le voici : :download:`tuto_meca.pdf`
