Notes sur le fonctionnement du serveur python
=============================================


Démarrage du serveur web


Réception requete navigateur **index.html** :

* génération du tableau de categorie
* envoi de la page **index.html**


réception d'un requête socket **ready** :

* attente carte étudiante
* envoi d'une réponse socket **carte_etudiant**


Réception requete navigateur **retrait_produit.html**

* récupération de la donnée GET sur le choix du produit
    * [https://stackoverflow.com/questions/22947905/flask-example-with-post]
* génération du visuel armoire
* envoi de la page **retrait_produit.html**
