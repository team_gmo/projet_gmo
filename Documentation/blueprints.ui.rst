Package UI
==========

Module admin\_menu
------------------

.. automodule:: blueprints.ui.admin_menu
    :members:
    :undoc-members:
    :show-inheritance:

Module index
------------

.. automodule:: blueprints.ui.index
    :members:
    :undoc-members:
    :show-inheritance:

Module product\_issue
---------------------

.. automodule:: blueprints.ui.product_issue
    :members:
    :undoc-members:
    :show-inheritance:

Module product
--------------

.. automodule:: blueprints.ui.product
    :members:
    :undoc-members:
    :show-inheritance:
