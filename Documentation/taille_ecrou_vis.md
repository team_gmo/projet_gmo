Taille Ecrou hexagonal + taille percage visserie
======================

Schéma
------

![Schéma d'un boulon](hex-nut.png)

Ecrou + vis de petite taille (en mm)
------------------------------------

|                      |  M1  | M1.6 |  M2  | M2.5 |  M3 |  M4  |  M5  |   M6  |
|:--------------------:|:----:|:----:|:----:|:----:|:---:|:----:|:----:|:-----:|
|         **p**        | 0,25 | 0,35 | 0,40 | 0,45 | 0,5 | 0,7  | 0,8  | 1,0   |
|         **h**        | 0,8  | 1,3  | 1,6  |   2  | 2,4 | 3,2  | 4,7  | 5,2   |
|         **k**        | 2,5  | 3,2  | 4    |   5  | 5,5 |  7   |  8   | 10    |
|         **e**        | 2,88 | 3,69 | 4,6  | 5,77 | 6,3 | 8,08 | 9,23 | 11,54 |
| **Percage visserie** | 0,75 | 1,25 | 1,6  | 2,05 | 2,5 | 3,3  | 4,2  | 5     |


[Source Ecrou](https://fr.wikipedia.org/wiki/%C3%89crou_hexagonal)   
[Source perçage visserie](https://fr.wikipedia.org/wiki/Filetage_m%C3%A9trique)
