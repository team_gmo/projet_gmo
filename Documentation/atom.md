Atom et python
==============

Installer les packages :

+ Script
+ linter
+ linter-flake8
+ Si atom vous dit qu'il a besoin d'autre chose, accepter ;)

Ajouter dans vos path système de windows :
`C:\Users\pierre-emmanuel\AppData\Local\Programs\Python\Python36\Scripts`
**(à adapter à votre pc, hein ...)**

ouvrir une console windows et installer ça :

+ pip install flake8
+ pip install flake8-docstrings

Et normalement vous devez avoir un bon assistant pour avoir un code propre.
En bonus vous pouvez maintenant executer du python depuis Atom avec
la commande `Shift+Ctrl+B` ou `⌘ + I` sur mac.

**Have Fun !**
