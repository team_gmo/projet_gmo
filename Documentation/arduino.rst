Arduino
=======

Comme vu dans la partie :doc:`routage` la partie de contrôle des portes des
casiers des armoires est basé sur une carte Arduino Mini Pro et donc sur le
microcontrolleur ATMEGA 328P de chez Atmel.

Afin de béfinicier au plus vite d'un prototype fonctionnel nous nous sommes
tournée vers la plate-forme de développement fournit par Arduino.

Nous gérons au sein du code 2 type de périphériques :

 - Des actionneurs : ici des loquets actionné par une source 12v.
   L'électrionique de contrôle adossé à ses loquets nous permet de les piloter
   avec une simple broche configurée en sortie (non PWM).
 - Des récepteurs : nous nous appuyons sur de petit intérupteur à micro-switch
   qui à l'aide d'un montage pull-up nous renvoit 5v si ce dernier est actionné.

Une autre grande partie de ce Q
