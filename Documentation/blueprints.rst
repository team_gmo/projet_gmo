Package blueprints
==================

.. toctree::

    blueprints.common
    blueprints.dashboard
    blueprints.ui
