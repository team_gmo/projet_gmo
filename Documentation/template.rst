Template
========

Le projet GMO utilise comme interface utilisateur des sites web. Ceux-ci sont
généré par python associé à la librairie flask. Le moteur de template est
jinja2.

Il existe 3 types : :ref:`dashboard`, :ref:`configuration` et :ref:`ui`.

.. _dashboard:

Dashboard
---------

Le Dashboard est utilisé pour l'administration des armoires. Il doit permettre
de :

* se connecter et se déconnecter (:any:`blueprints.dashboard.main`)
* ajouter ou modifier des profils utilisateurs (:any:`blueprints.dashboard.user`)
* ajouter ou modifier des catégories (:any:`blueprints.dashboard.categorie`)
* ajouter ou modifier des produits (:any:`blueprints.dashboard.produit`)
* visualiser l'historique d'un produit (:any:`blueprints.dashboard.timeline`)
* visualiser les emprunts d'un utilisateur (:any:`blueprints.dashboard.user`)
* emprunter / rendre avec ou sans problème un produit (:any:`blueprints.dashboard.produit`)
* résoudre un problème sur un produit (:any:`blueprints.dashboard.timeline`)
* rechercher un utilisateur / categorie / produit
* trier et subdiviser en page les résultats de cette recherche

Le schéma ci-dessous résume les intérations entre les pages web du
:any:`blueprints.dashboard`

.. graphviz::

   digraph {

    "main.connexion" -> "main.check_connexion";
    "main.deconnexion" -> "main.connexion";
    "main.check_connexion" -> "main.connexion";
    "main.check_connexion" -> "statistique.index";
    "main.check_connexion" -> "produit.index";
    "main.check_connexion" -> "user.index";
    "main.check_connexion" -> "categorie.index";
    "main.emprunt" -> "produit.index";
    "main.solve" -> "produit.index";
    "main.give_back" -> "produit.index";

    "statistique.index" -> "main.deconnexion";

    "categorie.index" -> "main.deconnexion";
    "categorie.index" -> "categorie.add";
    "categorie.index" -> "categorie.edit";
    "categorie.index" -> "categorie.trie";
    "categorie.index" -> "categorie.search";
    "categorie.index" -> "categorie.page";

    "categorie.add" -> "categorie.index";
    "categorie.edit" -> "categorie.index";
    "categorie.trie" -> "categorie.index";
    "categorie.search" -> "categorie.index";
    "categorie.page" -> "categorie.index";

    "produit.index" -> "main.deconnexion";
    "produit.index" -> "produit.add";
    "produit.index" -> "produit.edit";
    "produit.index" -> "produit.trie";
    "produit.index" -> "produit.search";
    "produit.index" -> "produit.page";
    "produit.index" -> "timeline.index";
    "produit.index" -> "main.emprunt";
    "produit.index" -> "main.give_back";

    "produit.add" -> "produit.index";
    "produit.edit" -> "produit.index";
    "produit.trie" -> "produit.index";
    "produit.search" -> "produit.index";
    "produit.page" -> "produit.index";

    "timeline.index" -> "produit.page";
    "timeline.index" -> "main.solve";

    "user.index" -> "main.deconnexion";
    "user.index" -> "user.add";
    "user.index" -> "user.edit";
    "user.index" -> "user.trie";
    "user.index" -> "user.search";
    "user.index" -> "user.page";

    "user.add" -> "user.index";
    "user.edit" -> "user.index";
    "user.trie" -> "user.index";
    "user.search" -> "user.index";
    "user.page" -> "user.index";
   }


.. _ui:

UI
--

L':any:`UI <blueprints.ui>` génère l'interface visible pour le grand publique.
Elle permet également à l'administrateur d'accéder à un premier menu de
configuration de l'armoire.
Elle doit permettre de :

* Voir les catégories de produits disponibles (:any:`blueprints.ui.index.index_page`)
* Emprunter un produit (:any:`blueprints.ui.product.get`)
* Rendre un produit (:any:`blueprints.ui.product.give_back`)
* Déclarer un problème lors de l'emprunt (:any:`blueprints.ui.product_issue`)
* Déclarer un problème lors du rendu (:any:`blueprints.ui.product_issue`)
* Passer en mode administrateur (:any:`blueprints.ui.index.index_page`)
* Verrouiller un casier
* Résoudre un problème

.. graphviz::

  digraph {

     "index" -> "product.get" [label="user_select_cat"];
     "index" -> "admin_menu" [label="user_click_superuser"];
     "product.get" -> "index" [label="casier_fermé"];
     "product.get" -> "product_issue.get" [label="prbl_déclaré"];
     "product_issue.get" -> "index" [label="casier_fermé"];

     "product.give_back" -> "product_issue.give_back" [label="rendu_avec_prbl"];
     "product.give_back" -> "index" [label="rendu_sans_prbl"];
     "product_issue.give_back" -> "index" [label="casier_fermé"];

     "admin_menu" -> "lock_casier"
     "admin_menu" -> "solve_issue"
  }


.. _configuration:

Configuration
-------------

La partie :ref:`configuration` n'est vouée à être utilisée qu'une seul fois : à
l'installation de l'armoire.
Elle offre la possibilité de :

* Déclarer une armoire entant que mettre ou esclave
* Détecter le nombre de casier
* Créer un modèle de vérification de carte
* Définir la position de la ligne de PI
* Indiquer la position de l'armoire dans un batiment
* Enregistrer un premier administrateur

.. graphviz::

    digraph {

       "index" -> "slave";
       "slave" -> "create_admin" [label="no"];
       "slave" -> "register_casier" [label="yes"];
       "create_admin" -> "id_card";
       "register_casier" -> "put_pi_line";
       "put_pi_line" -> "location";
       "location" -> "conf_ended";
       "id_card" -> "check_card";
       "check_card" -> "register_casier";
    }
