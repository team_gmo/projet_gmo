Protocole Comunication GMO
==========================

0 - bit start : 'S'
1 - bit d'adresse emetteur : '1'
2 - bit d'adresse destinataire : '1'
3 - bit d'ordre : 'A'
4 - bit de casier : '1'
5 - bit d'etat : '1'
6 - bit de fin : '\n'


|    Ordre    | byte |
|-------------|------|
| Alarme      | 'A'  |
| Ouverture   | 'O'  |
| Etat        | 'E'  |


|    Etat     | byte |
|-------------|------|
| Fermé       | '0'  |
| Ouvert      | '1'  |

|      Casier       | byte   |
|-------------------|--------|
| Tous les casiers  | '0'    |
| Casier numero X   | '1..X' |

Test ouverture ligne 1 casier 1 : S01O11
Test lecture état ligne 1 casier 1 : S01E10
Test alarme ligne 1 casier 1 : S01A10
S10E10
