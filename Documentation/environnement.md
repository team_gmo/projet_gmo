Installation Environnement
==========================

installer:

  + python3 : apt-get install python3
  + pip3 : apt-get install python3-pip
  + flask : pip3 install flask
  + socketio : pip3 install flask-socketio

lancement serveur:
la commande suivante lance le serveur en 127.0.0.1:5000/

$python3 serveur.py
