/*
GiveMeOne - Une armoire connectée
Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
                   Corentin Vretman, Maxence Dehaut

Developped in French at ESEO, engineering school (IoT Major)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <EEPROM.h>

//#define DEBUG
//#define RESET

#define FERME LOW
#define OUVERT HIGH

#define BIT_START     serialString[0]
#define BIT_ADDR_EMET serialString[1]
#define BIT_ADDR_DEST serialString[2]
#define BIT_ORDRE     serialString[3]
#define BIT_CASIER    serialString[4]
#define BIT_ETAT      serialString[5]
#define BIT_END       serialString[6]


int lockPin[10] = {2, 3, 4, 5, 6, 7, 8, 9, A7, A6};
int captPin[10] = {A5, A3, A4, A2, A1, A0, 13, 12, 11, 10};


int addrID = 0;
int addrNbCasier = 1;
byte myID = 1;
int nbCasier = 10;


char serialBuffer[7];
int byteCount = 0;

void setup()
{
  Serial.begin(9600);
  // Ouverture de la communication serie.
  
  // put your setup code here, to run once:
  for(int i = 0; i < 10; i++)
  { 
    pinMode(captPin[i], INPUT);
    pinMode(lockPin[i], OUTPUT);
  }
  myID = EEPROM.read(addrID);
  nbCasier = EEPROM.read(addrNbCasier);
  if (myID==254)
    myID = 1;
  #ifdef RESET   
  myID = 1;
  #endif
  EEPROM.write(addrID,myID);
  // Récuperation en ROM de l'addresse de l'arduino.

  #ifdef DEBUG   
  Serial.println(myID);
  #endif

}

void loop()
{
  byteCount = Serial.readBytes(serialBuffer, 7);
  if(byteCount > 0)
    serialStringAnalysis(serialBuffer);
  // Si un message est recu ce dernier est analyser par la fonction serialStringAnalysis.  
  
  memset(serialBuffer, 0, sizeof(serialBuffer));

  //Serial.flush();
}

/*reset*/

void reset()
{
  /* 
   * Réinitialisation de l'arduino a l'id 1 et au nombre de casier 10 
   */
  myID = 1;
  nbCasier = 10;
  EEPROM.write(addrID,myID);
  EEPROM.write(addrNbCasier,nbCasier);  
}


/* Doors stuff */

void openDoor(int doorNumber)
{
  /*
   * Demmande l'ouverture de la Porte pendant 1 seconde selon son numero (de 1 à 10) et demande une alarme à la fermeture de la porte.
   */
  
  //Serial.print("OPENDOOR pin : ");
  //Serial.print(lockPin[doorNumber-1]);
  //Serial.print(" ; id : ");
  //Serial.println(doorNumber);
  digitalWrite(lockPin[doorNumber-1], HIGH);
  delay(1000);
  digitalWrite(lockPin[doorNumber-1],LOW);

  ringWhenThisDoorIsClosed(doorNumber);
}

bool doorStatus(int doorNumber)
{
  /*
   * Retourne l'état (ouvert/fermé) de la porte demandée.
   */
  
  return !digitalRead(captPin[doorNumber-1]);
}

void statusOfAllDoors(int* tableOfDoorStatus)
{
  /*
   * Retourne l'état (ouvert/fermé) de toutes les portes de la ligne.
   */  
  
  for(int i = 0; i < nbCasier; i++)
  {
    tableOfDoorStatus[i] = doorStatus(i+1);
  }
  
}

void ringWhenADoorIsClosed()
{
  /*
   * Envois un message contenant le numéro du casier au PI dès le changement d'état d'un des casiers de la ligne. 
   */
  int i = 0;
  int oldStateOfDoors[nbCasier];
  int stateOfDoors[nbCasier];
  
  statusOfAllDoors(oldStateOfDoors);
  statusOfAllDoors(stateOfDoors);
  
  while(stateOfDoors[i] == oldStateOfDoors[i])
  {
    i++;
    if(i >= nbCasier )
    {
      statusOfAllDoors(stateOfDoors);
      i = 0;   
    }
  }
  sendCasierStateToPi((byte)i+1, FERME);
}

void ringWhenThisDoorIsClosed(int casierId)
{
  /*
   * Envois un message contenant le numéro du casier au PI dès le changement d'état du casier casierID.
   */
  
  //Serial.print("ringThisDoor : ");
  //Serial.println(casierId);
  while(doorStatus(casierId) == OUVERT)
  {
    delay(10); 
  }

  sendCasierStateToPi((byte)casierId, FERME);
  
  return;
}

void howManyDoors()
{
  /*
   * Détecte le premier casier fermé en partant de la fin de la ligne.
   */
  int i = 10;
  Serial.print("nbCasier avant = ");
  Serial.println(nbCasier);
  Serial.print("i = ");
  Serial.println(i);
   while ((i>=0)&&(doorStatus(i)==1))
   {
    i--;
    Serial.print("i = ");
    Serial.println(i);
   }
   nbCasier = i;
   Serial.print("nbCasier apres = ");
   Serial.println(nbCasier);
   sendNbCasier(i);
   EEPROM.write(addrNbCasier, nbCasier);
}

/* Serial stuff */

void serialStringAnalysis(char* serialString)
{
  /*
   * Analyse le message recu par l'arduino
   * Chaque message est composé de 7byte
   * S pour le Start_byte
   * Le byte d'éméteur, contenant l'adresse de l'emetteur du message
   * Le byte destinataire, contenant l'dresse du destinataire
   * Le byte d'ordre, contenant l'ordre : O pour overture E pour etat et A pour alarme
   * Le byte de casier, contenant l'identifiant du casier, de 1 à 10
   * Le byte d'Etat contenant l'etat d'ouverture ou de fermeture de la porte du dit casier
   * Le byte de fin, a savoir un retour à la ligne.
   */
  if(BIT_START != 'S')
  {
    #ifdef DEBUG
    Serial.println("Erreur pas de S");
    #endif
    return;
  }

  if(BIT_END != '\n')
  {
    #ifdef DEBUG
    Serial.println("Erreur pas de retour a la ligne");
    #endif
    return;
  }

  if(BIT_ADDR_DEST == 255)
  {
    #ifdef DEBUG
    Serial.println("Broadcast");
    #endif
    broadcastReceived(serialString);
  }
  else if(BIT_ADDR_DEST == myID+48)
  {
    #ifdef DEBUG
    Serial.println("ME");
    #endif
    messageForMe(serialString);
  } 
  else if((BIT_ADDR_EMET == '0') && (BIT_ADDR_DEST != myID+48))
  {
    #ifdef DEBUG
    Serial.print("string refusée");
    #endif
    return;
  } 
  else 
  {
    #ifdef DEBUG
    Serial.println("Somebody");
    #endif
    messageForSomebody(serialString);
  }
}

void broadcastReceived(char* serialString)
/*
 * Reaction a la réception d'un message de broadcast :
 * 2 cas possibles soit un casier est défini, dans ce cas on envois une alarme à la fermeture de celui-ci,
 * soit il n'y a pas de casier défini et on envoie une alarme à la fermeture du premier casier.
 */
{
  int id_casier = BIT_CASIER - '0';
  
  if(BIT_ORDRE == 'A')
  {
    if(BIT_CASIER == 0)
      ringWhenADoorIsClosed();
    else
      ringWhenThisDoorIsClosed(id_casier);
  }
}

void messageForMe(char* serialString)
{
  /*
   * L'arduino à identifier le message comme etant pour lui :
   * On analyse donc le bit d'ordre pour déterminer l'action à réaliser
   */
  int id_casier = BIT_CASIER - '0';
  #ifdef DEBUG
  Serial.println(BIT_ORDRE);
  #endif
  if(BIT_ORDRE == 'E')
  {
    sendCasierStateToPi(id_casier, doorStatus(id_casier));
  }

  if(BIT_ORDRE == 'O')
  {
    openDoor(id_casier);
  }

  if(BIT_ORDRE == 'A')
  {
    if(id_casier == 0)
      ringWhenADoorIsClosed();
    else
      ringWhenThisDoorIsClosed(id_casier);
  }
  if(BIT_ORDRE == 'R')
  {
    reset();
    #ifdef DEBUG
    Serial.print("Je suis l arduino ");
    Serial.print(myID);
    Serial.print(" et j ai ");
    Serial.print(nbCasier);
    Serial.println(" casier(s).");
    #endif
  }
  if(BIT_ORDRE == 'I')
  {
    howManyDoors();
    #ifdef DEBUG
    Serial.print("Je possede ");
    Serial.print(nbCasier);
    Serial.println(" casiers");
    #endif
  }
}

void messageForSomebody(char* serialString)
{
  /*
   * L'arduino a identifié le message comme n'étant pas pour lui, il le transmet donc à l'arduino suivant dans la ligne série.
   * Si l'adresse emetteur est supérieur ou égale à son adresse il incrémente également son adresse propre.
   */
  int addr_emet = BIT_ADDR_EMET - '0';
  if(myID <= addr_emet)
  {
    myID = addr_emet + 1;
    EEPROM.write(addrID,myID);
  }

  followMessage(BIT_ADDR_EMET, BIT_ADDR_DEST, BIT_ORDRE, BIT_CASIER, BIT_ETAT);
}

/* Transmition série */

void sendCasierStateToPi(byte casier, byte state)
{
  Serial.write('S');
  Serial.write(myID + '0');
  Serial.write('0');
  Serial.write('E');
  Serial.write(casier + '0');
  Serial.write(state + '0');
  Serial.write('\n');
}

void followMessage(byte emet, byte desti, byte ordre, byte casier, byte state)
{
  Serial.write('S');
  Serial.write(emet);
  Serial.write(desti);
  Serial.write(ordre);
  Serial.write(casier);
  Serial.write(state);
  Serial.write('\n');
}

void sendNbCasier(byte nbCasier)
{
  Serial.write('S');
  Serial.write(myID+'0');
  Serial.write('0');
  Serial.write('I');
  Serial.write(nbCasier + '0');
  Serial.write('0');
  Serial.write('\n');
}

