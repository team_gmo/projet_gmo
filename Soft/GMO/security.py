# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
Le module :py:func:`GMO.security` du projet GMO.

Il regroupe toute les fonctions permettant de certifier l'information provenant
d'une armoire.
Ce projet utilise un système à base de clés privées / publiques.
La clé générée est une clée de 2048 bits.
"""

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend

from GMO import config, armoire

import jwt


def generate_key():
    """
    Génère un couple de clé privé/public.

    Le système génère une clé de 2048 bits suivant l'algorythme RSA.

    :return: La clé privé et la clé publique.
    :rtype: list

    :Exemple:

    >>> from GMO import security
    >>> keys = security.generate_key()
    >>> print("Clé privée : ", keys[0])
    >>> print("Clé publique : ", keys[1])
    """
    key = rsa.generate_private_key(
        backend=default_backend(),
        public_exponent=65537,
        key_size=2048
    )

    # get public key in OpenSSH format
    public_key = key.public_key().public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )

    # get private key in PEM container format
    pem = key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )

    # decode to printable strings
    private_key_str = pem.decode('utf-8')
    public_key_str = public_key.decode('utf-8')

    return [private_key_str, public_key_str]


def get_my_private_key():
    """
    Retourne notre clé privée.

    Cette clé est stockée dans notre fichier de configuration (voir
    :py:func:`GMO.config.readParam()`).

    :return: La clé privé au format adapté pour jwt.
    :rtype: cryptography.hazmat.backends.openssl.rsa._RSAPrivateKey

    :Exemple:

    >>> from GMO import security
    >>> private_key = security.get_my_private_key()
    >>> type(private_key)
    <class 'cryptography.hazmat.backends.openssl.rsa._RSAPrivateKey'>
    """
    private_key_str = config.readParam("me.private_key")

    private_key = serialization.load_pem_private_key(
        private_key_str.encode('utf-8'),
        password=None,
        backend=default_backend()
    )

    return private_key


def get_public_key(id_armoire):
    """
    Retourne la clé publique d'une armoire donnée.

    Cette clé est stockée dans notre base de donnée, dans la table armoire
    (voir :py:func:`GMO.armoire.get_public_key`).

    :param id_armoire: ID de l'armoire dont on souhaite la clé publique.
    :type id_armoire: int
    :return: La clé publique au format adapté pour jwt.
    :rtype: cryptography.hazmat.backends.openssl.rsa._RSAPublicKey

    :Exemple:

    >>> from GMO import security
    >>> public_key = security.get_public_key(1)
    >>> type(public_key)
    <class 'cryptography.hazmat.backends.openssl.rsa._RSAPublicKey'>
    """
    public_key_str = armoire.get_public_key(id_armoire)

    public_key = serialization.load_pem_public_key(
        public_key_str.encode('utf-8'),
        backend=default_backend()
    )

    return public_key


def jwt2json(id_armoire, signed_jwt):
    """
    Convertie notre jwt en json.

    Le décodage a lieu uniquement si notre source est authentifiée.

    :param id_armoire: ID de l'armoire dont on souhaite la clé publique.
    :type id_armoire: int
    :param signed_jwt: Tableau binaire contenant l'information signée avec jwt.
    :type signed_jwt: bytes
    :return: La clé publique au format adapté pour jwt.
    :rtype: string

    :Exemple:

    >>> from GMO import security
    >>> import json
    >>> json_args = json.dumps([foo,bar])
    >>> data = security.json2jwt(json_args)
    >>> json_args_from_jwt = security.jwt2json(1, data)
    >>> json_args == json_args_from_jwt
    True
    """
    public_key = get_public_key(id_armoire)
    json_args = jwt.decode(
        signed_jwt,
        public_key,
        algorithms='RS256'
    )
    json_args = json_args['data']
    return json_args


def json2jwt(json_data):
    """
    Convertie notre json en paquet jwt.

    Le jwt est signé avec notre clé privée
    (voir :py:func:`GMO.security.get_my_private_key()`).

    :param json_data: String représentant les données à transmettre au format
                      json.
    :type json_data: str
    :return: Le jwt signé avec notre clé privée.
    :rtype: bytes

    :Exemple:

    >>> from GMO import security
    >>> import json
    >>> json_args = json.dumps([1,2,3])
    >>> data = security.json2jwt(json_args)
    >>> type(data)
    <class 'bytes'>
    """
    private_key = get_my_private_key()
    signed_jwt = jwt.encode(
        {'data': json_data},
        private_key,
        algorithm='RS256'
    )
    return signed_jwt
