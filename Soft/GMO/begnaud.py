# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Le module :py:func:`GMO.begnaud` du projet GMO.

**Vous devez l'utiliser dans les cas suivants :**

    * Enregistrement auprès de l'armoire begnaud du réseau.
    * Premier démarrage de l'armoire begnaud du réseau
    * Envoi d'une requête à l'armoire begnaud
"""

import requests
from flask import url_for
import json
from GMO import config, security, armoire
from threading import Lock

request_lock = Lock()


def register():
    """
    Enregistre une armoire esclave sur un réseau existant.

    **Cette fonction :**

        * Génère le couple de clé Privé/Public nécessaire.
          (voir :py:func:`GMO.security.generate_key`)
        * L'écrit dans le fichier de configuration
          (voir :py:func:`GMO.config.writeParam`)
        * Réalise la requête d'inscription au près du begnaud
          (voir :py:func:`blueprints.common.api.register`)
        * Sauvegarde la clé publique du maitre dans notre base locale
          (voir :py:func:`GMO.armoire.set`)
        * Sauvegarde de notre id d'armoire dans le fichier de configuration
          (voir :py:func:`GMO.config.writeParam`)

    :Exemple:

    >>> from GMO import begnaud
    >>> begnaud.register()

    Elle n'est utilisée que lors de l'installation d'une armoire esclave.
    """
    keys = security.generate_key()

    private_key_str = keys[0]
    public_key_str = keys[1]

    config.writeParam("me.private_key", private_key_str)

    description = "Armoire Arboux"

    json_args = json.dumps(
        {
            "public_key": public_key_str,
            "description": description
        }
    )

    url = config.readParam("master.address")
    url += "/api/register/"
    print(url)

    with request_lock:
        r = requests.post(url, data={'json_args': json_args})

    print(r)
    data = r.json()
    my_id = data["id_armoire"]
    master_public_key_str = data["public_key"]

    armoire.set(master_public_key_str, "")
    config.writeParam("me.id", my_id)


def register_master():
    """
    Enregistre une armoire esclave sur un réseau existant.

    **Cette fonction :**

        * Génère le couple de clé Privé/Public nécessaire.
          (voir :py:func:`GMO.security.generate_key`)
        * Ecrit la clé privée dans le fichier de configuration.
          (voir :py:func:`GMO.config.writeParam`)
        * Sauvegarde la clé publique dans notre base locale.
          (voir :py:func:`GMO.armoire.set`)
        * Donne l'ID 1 à l'armoire maitre et on enregistre dans le
          fichier de configuration. (voir :py:func:`GMO.config.writeParam`)

    :Exemple:

    >>> from GMO import begnaud
    >>> begnaud.register_master()

    Elle n'est utilisée que lors de l'installation d'une armoire begnaud.
    """
    keys = security.generate_key()

    private_key_str = keys[0]
    public_key_str = keys[1]

    config.writeParam("me.private_key", private_key_str)

    description = "Armoire de la salle Marconi"

    armoire.set(public_key_str, description)

    config.writeParam("me.id", 1)


def send(module, function, args):
    """
    Envoi une requête http à l'amoire begnaud.

    **Cette fonction :**

        * Construit l'URL adaptée à la requête.
          (voir :py:mod:`blueprints.common.api`)
        * Formate les paramètres ``args`` en json avec Json Web Token
          (voir :py:mod:`blueprints.common.api`)
        * Signe notre requête à l'aide notre clé publique
          (voir :py:func:`GMO.security.json2jwt`)
        * Récupère le résultat de l'opération
          (voir :py:func:`requests.post`)
        * Vérifie l'authenticité de la réponse.
          (voir :py:func:`GMO.security.jwt2json`)

    :Exemple:

    >>> from GMO import begnaud
    >>> assigned_casier = begnaud.send("casier", "get_by_id_produit", [id_prd])

    Cet exemple montre comment appeler la fonction
    :py:func:`GMO.casier.get_by_id_produit` à distance. Cette fonction demande
    comme paramètre l'id du produit. On transmet donc l'id du produit. Puis
    l'information de retournée par la fonction est stocké dans la variable
    assigned_casier.
    """
    id_armoire = config.readParam("me.id")

    url = config.readParam("master.address")
    url += "/api/" + module + "/" + str(id_armoire) + "/" + function + "/"

    json_args = json.dumps(args)
    signed_jwt = security.json2jwt(json_args)
    with request_lock:
        r = requests.post(url, data={'jwt': signed_jwt})

    try:
        verified_jwt = security.jwt2json(1, r.text)
        data = json.loads(verified_jwt)
    except:
        print("ERREUR : " + str(r.text))
        data = None
    return data
