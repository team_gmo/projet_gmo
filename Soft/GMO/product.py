# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Ce module implémente les différentes fonctions nécessaires pour l'utilisation
de la table produit définie en BDD.
La table produit enregistre trois informations dans les champs suivants :

    * `id_cat` définie la catégorie du produit.
    * `id_user` associe un produit à un utilisateur.
    * `tag_id` définie le numéro du tag du produit.
"""

from GMO import bdd, big_brother, casier, user


def convert_id(tag_id):
    """
    Convertit l'id du tag attribué au produit en rowid du produit.

    :param id_prd: Le numéro du tag du produit
    :type id_prd: str
    :return: Le tableau contenant le rowid du produit correspondant au tag
    """
    conn = bdd.open()
    data = (tag_id,)
    res = bdd.get_one(
        conn,
        "SELECT produit.rowid \
        FROM produit \
        WHERE produit.tag_id = ?",
        data
    )
    bdd.close(conn)
    return res


def set_product(tag_id, id_cat):
    """
    Ajoute un produit dans la table produit.

    :param tag_id: Le numéro du tag du produit
    :type tag_id: str
    :param id_cat: rowid attribué par sqlite correspondant à la catégorie \
    du produit
    :return: Le nombre de ligne affectés par la modification
    """
    conn = bdd.open()
    data = (id_cat, tag_id)
    res = bdd.execute(conn, 'INSERT INTO produit VALUES (?,0,?)', data)
    id_pdt = convert_id(id_prd) # Tag_id devient rowid
    big_brother.set(0, id_pdt[0], 0, 1, "", 0)  # Création dans big_brother
    big_brother.set(0, id_pdt[0], 0, 2, "", 0)  # Création dans big_brother
    bdd.close(conn)
    return res


# def update_state(id_prd, state):
#     """Update user information."""
#     conn = bdd.open()
#     data = (state, id_prd,)
#     res = bdd.execute(conn,'UPDATE produit SET dispo = ? WHERE id = ?', data)
#     bdd.close(conn)
#     return res


def update_cat(id_prd, id_cat):
    """
    Modifie l'enregistrement d'un produit dans la table produit.

    :param tag_id: Le numéro du tag du produit
    :type tag_id: str
    :param id_cat: rowid attribué par sqlite correspondant à la catégorie \
    du produit
    :return: Le nombre de ligne affectés par la modification
    """
    conn = bdd.open()
    data = (id_cat, id_prd,)
    res = bdd.execute(conn, 'UPDATE produit SET id_cat = ? WHERE rowid = ?', data)
    bdd.close(conn)
    return res


def update(id_prod, id_user):
    """Update full of produit."""
    conn = bdd.open()
    data = (id_user, id_prod,)
    res = bdd.execute(conn, 'UPDATE produit \
                      SET id_user = ? \
                      WHERE rowid = ?',
                      data
                      )
    bdd.close(conn)
    return res


def emprunt(cardID, id_cat, id_armoire):
    """
    Enregistre l'emprunt d'un produit par un utilisateur
    """
    userData = user.get_by_card(cardID)

    if userData is None:
        return None

    id_user = userData[2]
    id_prd = link_to_user_from_categorie(id_cat, id_user, id_armoire)

    if id_prd is not None:
        data_casier = casier.get_by_id_produit(id_prd)
        ligne = data_casier[0]
        colonne = data_casier[1]
        id_casier = data_casier[2]
        unlink_from_casier(id_prd)
        big_brother.set(id_user, id_prd, id_casier, 4, "", None)
        print("emprunt", [id_prd, ligne, colonne, id_casier])
        return [id_prd, ligne, colonne, id_casier]

    return None


def get_one(id_prd):
    """Get a product."""
    conn = bdd.open()
    data = (id_prd,)
    res = bdd.get_one(conn, "SELECT * FROM produit WHERE rowid = ?", data)
    bdd.close(conn)
    return res


def get_one_tag(id_prd):
    """Get a product."""
    conn = bdd.open()
    data = (id_prd,)
    res = bdd.get_one(conn, "SELECT * FROM produit WHERE tag_id = ?", data)
    bdd.close(conn)
    return res


def get_one_by_tag(tag_prd):
    """Get a product."""
    conn = bdd.open()
    data = (tag_prd,)
    res = bdd.get_one(
        conn,
        "SELECT produit.rowid, \
                produit.id_cat, \
                produit.id_user \
         FROM produit \
         WHERE tag_id = ?", data)
    bdd.close(conn)
    return res


def get_cat(id_prd):
    """Get a product."""
    conn = bdd.open()
    data = (id_prd, )
    res = bdd.get_one(
        conn,
        "SELECT categorie.* \
        FROM produit \
        LEFT JOIN categorie ON produit.id_cat = categorie.id \
        WHERE produit.rowid = ?",
        data
    )
    bdd.close(conn)
    return res


def link_to_user(id_user, id_prod):
    """Un utilisateur emprunte un produit."""
    if not get_one_tag(id_prod)[1]:
        update(id_prod, id_user)
    return None


def give_back_reserve(id_prod, prbl):
    """Le produit est rendu à l'administrateur qui le place en réserve"""
    etat = 2
    update(id_prod, 0)
    big_brother.set(0, id_prod, 0, etat, "", None)
    return None


def declare_prbl(id_user, id_prod, prbl):
    """Un utilisateur déclare un problème lors du rendu d'un produit.
    Le produit passe automatiquement au statut en réparation."""
    etat = 6
    id_casier = 0
    rowid_link = 0
    big_brother.set(id_user, id_prod, id_casier, etat, prbl, rowid_link)
    return ""


def declare_prbl_user(id_user, id_prod, id_casier, prbl):
    """Un utilisateur déclare un problème lors du rendu d'un produit."""
    etat = 5
    rowid_link = 0
    big_brother.set(id_user, id_prod, id_casier, etat, prbl, rowid_link)
    return ""


def solve(id_prod, description):
    """Résolution problème sur produit."""
    print("commentaire :")
    print(description)
    update(id_prod, 0)
    big_brother.set(0, id_prod, 0, 2, description, None)
    return None


def give_back(tag_produit, is_admin):
    """Un utilisateur rend un produit."""
    details_pdt = get_one_by_tag(tag_produit)
    if details_pdt is None:
        return "Ce produit n'existe pas."
    id_user = details_pdt[2]
    id_produit = details_pdt[0]

    if not id_user and not is_admin:
        return "Ce produit ne peut être déposé dans une armoire."

    res = link_to_casier(id_produit)
    if not res:
        return "Plus de casier adapté disponible pour ce produit."

    id_casier = casier.get_by_id_produit(id_produit)
    if id_casier is None:
        return "Ce produit n'a pas pu être lié à un casier."
    id_casier = id_casier[2]

    id_emprunt = [0]
    if id_user:
        id_emprunt = big_brother.get_last_user_emprunt_for_an_object(
            id_user,
            id_produit
        )
        if id_emprunt is None:
            return "Ce produit n'a pas été emprunté."

    rowid = big_brother.set(
        id_user, id_produit, id_casier, 3, "", id_emprunt[0]
    )

    if id_user:
        big_brother.link_emprunt(id_user, id_produit, rowid)
        unlink_from_user(id_produit)
    return ""


def book(id_user, id_cat):
    """Un utilisateur réserve un produit."""
    id_produit = link_to_user_from_categorie(id_cat, id_user)
    return id_produit


def link_to_user_from_categorie(id_cat, id_user, id_arm):
    """Assign a user to a produit. He is now in charge of the produit."""
    conn = bdd.open()
    data = (id_cat, id_arm)

    product = bdd.get_one(conn, "SELECT casier.id_produit \
                    FROM produit \
                    INNER JOIN casier ON produit.rowid = casier.id_produit \
                    WHERE produit.id_cat = ? AND casier.id_armoire = ?\
                    ORDER BY casier.hauteur DESC, \
                    casier.largeur DESC, \
                    casier.profondeur DESC \
                    LIMIT 1", data)

    if product is not None:
        data = (id_user, product[0])
        bdd.execute(conn, "UPDATE produit SET id_user = ? \
                    WHERE rowid = ?", data)
        bdd.close(conn)
        return product[0]

    bdd.close(conn)
    return None


def unlink_from_user(id_produit):
    """Remove the link between a user and a produit."""
    conn = bdd.open()
    data = (id_produit,)
    res = bdd.execute(conn,
                      "UPDATE produit SET id_user = 0 \
                      WHERE rowid = ?",
                      data)
    bdd.close(conn)
    return res


def unlink_from_casier(id_produit):
    """Remove the link between a casier and a produit."""
    conn = bdd.open()
    data = (id_produit,)
    res = bdd.execute(
        conn,
        "UPDATE casier SET id_produit = 0 WHERE id_produit = ?",
        data)
    bdd.close(conn)
    return res


def link_to_casier(id_produit):
    """Assign a casier to a produit."""
    conn = bdd.open()
    print("ID PRODUIT  : ", id_produit)
    produit = get_cat(id_produit)
    if produit is None:
        print("erreur", id_produit)
        return 0
    hauteur = produit[1]
    largeur = produit[2]
    profondeur = produit[3]
    data = (id_produit, hauteur, largeur, profondeur, id_produit,)
    res = bdd.execute(
        conn,
        "UPDATE casier \
        SET id_produit = ? \
        WHERE id = ( \
            SELECT casier.id \
            FROM casier \
            WHERE casier.id_produit = 0 \
            AND casier.hauteur > ? \
            AND casier.largeur > ? \
            AND casier.profondeur > ? \
            AND casier.verouille = 0 \
            ORDER BY  casier.hauteur ASC, \
            casier.largeur ASC, \
            casier.profondeur ASC \
            LIMIT 1 \
        ) \
        AND NOT EXISTS ( \
            SELECT casier.id \
            FROM casier \
            WHERE casier.id_produit = ? \
        )", data)
    bdd.close(conn)
    return res


def get_number():
    """Return le nombre d'user enregistré en base."""
    conn = bdd.open()
    nb_user = bdd.execute(conn, 'SELECT COUNT(*) FROM user', ())
    bdd.close(conn)
    return nb_user


def timeline(id_prod, date_deb, date_fin):
    """Read big brother and generate the time line."""
    conn = bdd.open()
    data = (id_prod, date_deb, date_fin)
    timeline = bdd.get_all(
        conn,
        "SELECT big_brother.date, \
                user.nom, \
                user.prenom, \
                casier.id_armoire, \
                casier.ligne, \
                casier.colonne, \
                big_brother.etat, \
                big_brother.commentaire, \
                categorie.nom, \
                big_brother.id_produit, \
                user.rowid \
          FROM big_brother \
          LEFT JOIN user ON user.rowid = big_brother.id_user \
          LEFT JOIN casier ON casier.id = big_brother.id_casier \
          LEFT JOIN produit ON produit.rowid = big_brother.id_produit \
          LEFT JOIN categorie ON categorie.id = produit.id_cat \
          WHERE big_brother.id_produit = ? \
          AND big_brother.date > ? AND big_brother.date < ? \
          ORDER BY  big_brother.rowid DESC \
          LIMIT 50",
        data)

    bdd.close(conn)
    return timeline


def search(requete, col, sens, limit, demarre, num):
    """Return le resultat de la requête du materiels."""
    conn = bdd.open()

    if num == 3:
        data = ("", "", requete, "", "", limit, demarre)
    else:
        requete = '%' + requete + '%'
        data = (requete, requete, requete, requete, requete, limit, demarre)
    resultat = bdd.get_all(
        conn,
        "SELECT produit.tag_id, \
                categorie.nom, \
                user.nom, \
                user.prenom, \
                casier.id_armoire, \
                casier.ligne, \
                casier.colonne, \
                ( \
                    SELECT bb.etat \
                    FROM big_brother \
                    AS bb WHERE bb.id_produit = produit.rowid \
                    ORDER BY bb.rowid DESC \
                    LIMIT 1 \
                ) AS etat, \
                user.nom || ' ' || user.prenom AS nom_prenom, \
                user.prenom || ' ' || user.nom AS prenom_nom, \
                produit.rowid \
        FROM produit \
        LEFT JOIN user ON user.rowid = produit.id_user \
        LEFT JOIN categorie ON categorie.id = produit.id_cat \
        LEFT JOIN casier ON casier.id_produit = produit.rowid \
        WHERE produit.tag_id LIKE ? \
        OR categorie.nom LIKE ? \
        OR etat LIKE ? \
        OR nom_prenom LIKE ? \
        OR prenom_nom LIKE ? \
        ORDER BY " + col + " " + sens + " \
        LIMIT ?, ?",
        data
    )
    bdd.close(conn)
    return resultat


def search_size(requete):
    """Return le nb de resultat de la requête du materiels."""
    conn = bdd.open()
    requete = '%' + requete + '%'
    data = (requete, requete, requete, requete, requete,)
    resultat = bdd.get_one(
        conn,
        "SELECT COUNT(*), \
                (SELECT bb.etat \
                FROM big_brother \
                AS bb WHERE bb.id_produit = produit.rowid \
                ORDER BY bb.date DESC \
                LIMIT 1) AS etat, \
                user.nom || ' ' || user.prenom AS nom_prenom, \
                user.prenom || ' ' || user.nom AS prenom_nom \
        FROM produit \
        LEFT JOIN user ON user.rowid = produit.id_user \
        LEFT JOIN categorie ON categorie.id = produit.id_cat \
        LEFT JOIN casier ON casier.id_produit = produit.rowid \
        WHERE produit.tag_id LIKE ? \
        OR categorie.nom LIKE ? \
        OR etat LIKE ? \
        OR nom_prenom LIKE ? \
        OR prenom_nom LIKE ?",
        data)
    bdd.close(conn)
    return resultat


def get_quantity():
    """Return le nombre de produits enregistrés en base."""
    conn = bdd.open()
    nb_prod = bdd.get_one(conn, 'SELECT COUNT(*) FROM produit', ())
    bdd.close(conn)
    return nb_prod


def get_all_ids():
    """
    Retourne une liste des id_produit enregistrés dans big_brother et retourne.
    """
    conn = bdd.open()

    res = bdd.get_all(
        conn,
        'SELECT big_brother.id_produit \
        FROM big_brother \
        ORDER BY big_brother.id_produit ASC',
        ())

    my_list_res = []
    for i in range(len(res)):
        my_list_res.append(res[i][0])
    my_list_res = list(set(my_list_res))

    # print("res all ids : ", my_list_res)

    bdd.close(conn)
    return my_list_res


def get_quantity_state(etat):
    """
    Retourne le nombre de produits actuellement dans l'état mis en parametre.
    """
    conn = bdd.open()
    inc = 0
    ids = get_all_ids()
    for id in ids:
        data = (id, )
        res = bdd.get_one(
            conn,
            'SELECT big_brother.etat \
            FROM big_brother \
            WHERE big_brother.id_produit = ? \
            ORDER BY date DESC \
            LIMIT 1',
            data)
        # print("res qs : ", res)
        if res[0] == etat:
            inc += 1

    # print("QUANTITY STATE : ", inc)
    bdd.close(conn)
    return inc


def get_signaled_issues(id_armoire):
    """
    Retourn la liste des casiers verrouillés selon la base dû à un
    soulevement d'erreur.
    """
    conn = bdd.open()
    data = (id_armoire, )

    res = bdd.get_all(
        conn,
        "SELECT DISTINCT ligne, colonne FROM casier \
         INNER JOIN big_brother as bb ON casier.id = bb.id_casier \
         WHERE casier.id_armoire = ? AND \
               casier.id_produit != 0 AND \
               casier.id_produit is not NULL AND ( \
                    SELECT bb.etat \
                    FROM big_brother as bb \
                    WHERE bb.id_casier = casier.id \
                    ORDER BY date DESC \
                    LIMIT 1 \
                    ) = 5", data)

    print("SIGNALED ISSUES : ", res)
    bdd.close(conn)
    return res


def get_com(id_armoire, tuples):
    """
    Return la liste des commentaires pour les casiers contenant une erreur.
    """
    # Recuperation de l'id de casier concerné
    print(" VARS : ", id_armoire, tuples)
    the_casier = casier.get_by_id_armoire(id_armoire, tuples[0], tuples[1])
    print(" the_casier: ", the_casier)

    conn = bdd.open()
    data = (the_casier[0], )

    res = bdd.get_one(
        conn,
        'SELECT big_brother.commentaire FROM big_brother \
        WHERE big_brother.id_casier = ? \
        AND big_brother.etat = 5 \
        ORDER BY date DESC \
        LIMIT 1',
        data)

    print("LES COM : ", res)
    bdd.close(conn)
    return res
