# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Le module :py:mod:`GMO.casier` du projet GMO.

Ce module implémente les différentes fonctions nécessaires pour l'utilisation
de la table casier définie en BDD.
La table casier enregistre dix informations dans les champs suivantes :

    * `id` permet de sélectionner le casier à utiliser.
    * `id_user` permet de lier un casier à un utilisateur.
      (non utilisé actuellement) (voir :py:mod:`GMO.user`)
    * `id_produit` permet de lier un casier à un produit.
      (voir :py:mod:`GMO.produit`)
    * `verrouille` permet de verouiller un casier en cas de problème mécanique.
    * `hauteur` définie la hauteur d'un casier.
    * `largeur` définie la largeur d'un casier.
    * `profondeur` définie la profondeur d'un casier.
    * `id_armoire` associe un casier à une armoire.
    * `ligne` associe un casier à la ligne d'une armoire.
      (voir :py:mod:`GMO.actionneur`)
    * `colonne` associe un casier à la colonne d'une ligne.
      (voir :py:mod:`GMO.actionneur`)

:todo:

Supprimer le champ `id` et utiliser le rowid fournit par sqlite.

"""

from GMO import bdd, actionneur


def set(hauteur, largeur, profondeur, id_armoire):
    """
    Ajoute un casier dans la table casier.

    :param hauteur: La hauteur d'un casier.
    :type hauteur: int
    :param largeur: La largeur d'un casier.
    :type largeur: int
    :param profondeur: La profondeur d'un casier.
    :type profondeur: int
    :param id_armoire: rowid attribué par sqlite correspondant à l'armoire.
    :type id_armoire: int
    :return: Le nombre de lignes affectés par la modification.
    """
    conn = bdd.open()
    data = (hauteur, largeur, profondeur, id_armoire,)
    res = bdd.execute(
        conn,
        "INSERT INTO casier ( \
            id_user, \
            id_produit, \
            verouille, \
            hauteur, \
            largeur, \
            profondeur, \
            id_armoire) \
        VALUES (0, 0, 0, ?, ?, ?, ?)", data)
    bdd.close(conn)
    return res


def get_by_id_produit(id_prd):
    """
    Affiche l'emplacement du casier dans lequel est un produit.

    :param id_prd: rowid attribué par sqlite du produit recherché.
    :type id_prd: int
    :return:
    """

    conn = bdd.open()
    data = (id_prd, )
    casier = bdd.get_one(
        conn,
        'SELECT casier.ligne, casier.colonne, casier.id \
        FROM casier WHERE casier.id_produit=?', data)
    bdd.close(conn)
    return casier


def get_by_id_armoire(id_arm, id_ligne, id_col):
    """Return the id_produit contained in the casier, using row and col num."""
    conn = bdd.open()

    data = (id_arm, id_ligne, id_col)
    produit = bdd.get_one(
        conn,
        'SELECT casier.id_produit \
        FROM casier \
        WHERE casier.id_armoire = ? \
        AND casier.ligne = ? AND casier.colonne = ?',
        data
    )
    bdd.close(conn)
    print('id_produit contained: ', produit)
    return produit


def get_id_by_l_c(id_arm, id_ligne, id_col):
    """Return the ligne and colonne of the casier."""
    conn = bdd.open()

    data = (id_arm, id_ligne, id_col)
    casier = bdd.get_one(
        conn,
        'SELECT casier.id \
        FROM casier \
        WHERE casier.id_armoire = ? \
        AND casier.ligne = ? AND casier.colonne = ?',
        data
    )
    bdd.close(conn)
    return casier


def get_number_of_floor(id_armoire):
    """Return the number of floor contained in the armoire."""
    conn = bdd.open()

    data = (id_armoire,)
    nb_floors = bdd.get_one(
        conn,
        'SELECT COUNT(*) \
        FROM casier \
        WHERE casier.id_armoire = ? \
        AND casier.colonne = 1',
        data
    )
    bdd.close(conn)
    return nb_floors


def get_number_of_col(ligne, id_armoire):
    """Return the number of col contained in a floor."""
    conn = bdd.open()

    data = (ligne, id_armoire,)
    nb_cols = bdd.get_one(
        conn,
        'SELECT COUNT(*) \
        FROM casier \
        WHERE casier.ligne = ? AND casier.id_armoire = ?',
        data
    )
    bdd.close(conn)
    return nb_cols


def get_by_id(id_casier):
    """Return the ligne and colonne of the casier."""
    conn = bdd.open()

    data = (id_casier,)
    casier = bdd.get_one(
        conn,
        'SELECT casier.ligne, casier.colonne \
        FROM casier \
        WHERE casier.id=?',
        data
    )
    bdd.close(conn)
    return casier


def open(id_casier):
    """Ouvre un casier."""
    locker = get_by_id(id_casier)
    actionneur.send(str(locker[0]), str(locker[1]), "O")
    reponse = actionneur.read()
    print(reponse)
    return


def getStatus(id_casier):
    """Return the status of the specified locker."""
    locker = get_by_id(id_casier)
    actionneur.send(str(locker[0]), str(locker[1]), "E")
    reponse = actionneur.read()
    print(reponse)
    return


def get_locked(id_armoire):
    """Return la liste des casiers verrouillés selon la base."""
    conn = bdd.open()
    data = (id_armoire, )

    res = bdd.get_all(
        conn,
        'SELECT casier.ligne, casier.colonne FROM casier \
        WHERE casier.id_armoire = ? \
        AND casier.verouille = 1',
        data
    )

    bdd.close(conn)
    return res


def set_lock(state, id_armoire, l, c):
    """Procède au lock / unlock d'un casier en base"""
    conn = bdd.open()
    data = (state, id_armoire, l, c)

    res = bdd.get_one(
        conn,
        'UPDATE casier SET verouille = ? \
        WHERE id_armoire = ? \
        AND ligne = ? \
        AND colonne = ?',
        data
    )

    bdd.close(conn)
    return res
