# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Ce module implémente les différentes fonctions nécessaires pour l'utilisation
de la table big_brother définie en BDD.
La table big_brother enregistre chaque évènement de la vie de chaque produit
à travers sept informations dans les champs suivantes :

    * `id_user` associe l'événement à un utilisateur.
    * `id_produit` associe l'événement à un produit.
    * `date` défini la date et l'heure de l'événement.
    * `id_casier` permet de lier l'événement à un casier.
    * `etat` définie l'état que prend un produit à un événement.
    * `commentaire` associe une éventuelle description d'un état du produit à \
    un événement.
    * `rowid_link` associe un événement à un autre si besoin.

Le champ 'etat' peut prendre les différentes valeurs suivantes :

    * `0` le produit est considéré comme hors-système (assimilé à un produit \
    supprimmé).
    * `1` le produit a été créé (c'est un état passagé).
    * `2` le produit se situe en réserve (à savoir dans dans le magasin) et \
    est disponible à l'emprunt auprès d'un administrateur.
    * `3` le produit est disponible à l'emprunt dans une armoire.
    * `4` le produit est emprunté par un utilisateur.
    * `5` le produit a eu un problème de défini, se trouve dans une armoire \
    et nécessite une action d'un administrateur.
    * `6` le produit est en réparation et se trouve dans la réserve.

Pour plus de détails concernant les états : consulter la partie ..............
"""

from GMO import bdd
import time


def set(id_user, id_produit, id_casier, etat, com, rowid_link):
    """
    Enregistre un événement dans la table big_brother.

    :param id_user: rowid attribué par sqlite correspondant à l'utilisateur.
    :type id_user: int
    :param id_produit: rowid attribué par sqlite correspondant au produit.
    :type id_produit: int
    :param id_casier: L'id attribué correspondant au casier.
    :type id_casier: int
    :param etat: L'état correspondant au produit.
    :type etat: int
    :param com: Le commentaire attribué à l'état du id_produit.
    :type com: str
    :param rowid_link: rowid attribué par sqlite de l'événement à lié au \
    nouvel événement.
    :type rowid_link: int
    :return: Le nombre de lignes affectés par la modification.
    """
    conn = bdd.open()
    if rowid_link is None:
        rowid_link = 0

    data = (
        id_user,
        id_produit,
        int(time.time()),
        id_casier,
        etat,
        com,
        rowid_link
    )

    res = bdd.execute_lrid(
        conn,
        'INSERT INTO big_brother VALUES (?, ?, ?, ?, ?, ?, ?)',
        data
    )
    bdd.close(conn)
    return res


def get_last_user_emprunt_for_an_object(id_user, id_produit):
    """
    Récupère l'événement, dans la table big_brother, décrivant l'emprunt \
    d'un produit par un utilisateur enregistré dans la table 'big_brother'.

    :param id_user: rowid attribué par sqlite de l'utilisateur recherché
    :type id_user: int
    :param id_produit: rowid attribué par sqlite du produit recherché
    :type id_user: int
    :return: Le tableau contenant le rowid de l'événement recherché
    """
    conn = bdd.open()
    data = (id_user, id_produit)
    res = bdd.get_one(
        conn,
        'SELECT big_brother.rowid \
         FROM big_brother \
         WHERE big_brother.id_user = ? AND big_brother.id_produit = ? \
         ORDER BY big_brother.date DESC \
         LIMIT 1',
        data
    )
    bdd.close(conn)
    return res


def link_emprunt(id_user, id_produit, rowid):
    """
    Relie deux événements de la table 'big_brother' entre eux à partir de leur
    rowid (utilisé lors d'un rendu de produit)

    :param id_user: rowid attribué par sqlite de l'utilisateur rendant le \
    produit
    :type id_user: int
    :param id_produit: rowid attribué par sqlite du produit rendu
    :type id_produit: int
    :param rowid: rowid attribué par sqlite correspondant à l'événement à lier
    :return: Le nombre de lignes modifiées
    """
    conn = bdd.open()
    data = (rowid, id_user, id_produit)
    res = bdd.execute(
        conn,
        'UPDATE big_brother \
        SET rowid_link = ? \
        WHERE id_user = ? \
        AND id_produit = ? \
        AND rowid_link = 0',
        data
    )
    bdd.close(conn)
    return res


def get_last_emprunteur_for_an_object(id_produit):
    """
    Retrouve quel est le dernier utilisateur d'un produit.

    :param id_produit: rowid atttribué par sqlite du produit emprunté
    :type id_produit: int
    :return: Le tableau contenant le rowid de l'utilisateur
    """
    conn = bdd.open()
    data = (int(id_produit),)
    res = bdd.get_one(
        conn,
        'SELECT big_brother.id_user \
         FROM big_brother \
         WHERE big_brother.id_produit = ? AND big_brother.etat = 3 \
         ORDER BY big_brother.date DESC \
         LIMIT 1',
        data
    )
    bdd.close(conn)

    print("get_last_emprunteur_for_an_object", id_produit, res)
    return res
