"""Fonctionnalités développées pour l'appareil photo et la lecture de carte."""

try:
    import picamera
    rasp_pi = True
except ImportError:
    rasp_pi = False

import pytesseract
import string
import colorsys
import io
import time
from GMO import config, led, global_var
from PIL import Image
from threading import Lock

getCardPicture_lock = Lock()


def getCardPicture():
    """Prend une photo de la carte.

    :Exemple:

    >>> from GMO import camera
    >>> getCardPicture()

    :return: Image capturée
    :rtype: PIL.Image.Image
    """
    with getCardPicture_lock:
        if not rasp_pi:
            cardPicture = Image.open('./carte-etu.png')
            return cardPicture

        # Prise de la photo.
        camera = picamera.PiCamera()
        camera.resolution = (2592, 1944)

        led.on()
        time.sleep(1)
        cardPicture = io.BytesIO()
        camera.capture(cardPicture, 'jpeg', use_video_port=True)
        while not checkPixelValidityHSL(cardPicture, 2100, 900, lValid=40, sValid=10):
            cardPicture = io.BytesIO()
            camera.capture(cardPicture, 'jpeg', use_video_port=True)
            if global_var.cardID == "0":
                camera.close()
                led.off()
                return None

        camera.close()
        led.off()

        return cardPicture


def cropPicture(image, x0, y0, largeurX, longueurY):
    """Rogne une image selon une taille passée en paramètre.

    :Exemple:

    >>> from GMO import camera
    >>> picture = getCardPicture()
    >>> getCardPicture(picture, 0, 0, 200, 300)

    :param image: L'image à rogner
    :type image: PIL.Image.Image
    :param x0: L'abscisse de l'origine
    :type y0: int
    :param y0: L'ordonnée de l'origine
    :type x0: int
    :param largeurX: La taille sur l'axe des abscisses
    :type largeurX: int
    :param longueurY: La taille sur l'axe des ordonnées
    :type longueurY: int
    :return: L'image rognée
    :rtype: PIL.Image.Image
    """
    # Rognage de l'image.
    box = (x0, y0, x0 + largeurX, y0 + longueurY)
    imCropped = image.crop(box)

    return imCropped


def getUserPicture(cardPicture):
    """Isole la photo présente sur la photo de la carte.

    :Exemple:

    >>> from GMO import camera
    >>> picture = getCardPicture()
    >>> getUserPicture(picture)

    :param cardPicture: L'image de la carte à traiter
    :type cardPicture: PIL.Image.Image
    :return: La photo de l'utilisateur
    :rtype: PIL.Image.Image
    """
    userPicturePIL = Image.open(cardPicture)
    # Recupération des paramètres de recadrage
    param = config.readParam('camera.userPicture.taille')

    x, y = param['origineX'], param['origineY']
    largeur, longueur = param['tailleX'], param['tailleY']

    # Recadrage sur la photo de l'étudiant.
    userFace = cropPicture(userPicturePIL, x, y, largeur, longueur)

    facePicture = io.BytesIO()
    userFace.save(facePicture, 'png')
    return facePicture


def textFromImageOcr(image, x0, y0, largeurX, longueurY):
    """Lis les caractères présents sur une image par OCR.

    :Exemple:

    >>> from GMO import camera
    >>> picture = getCardPicture()
    >>> textFromImageOcr(picture, 800, 300, 400, 500)

    :param image: L'image à rogner
    :type image: PIL.Image.Image
    :param x0: L'abscisse de l'origine
    :type y0: int
    :param y0: L'ordonnée de l'origine
    :type x0: int
    :param largeurX: La taille sur l'axe des abscisses
    :type largeurX: int
    :param longueurY: La taille sur l'axe des ordonnées
    :type longueurY: int
    :return: La liste des lignes de caractères identifiés
    :rtype: list
    """
    # Recadrage sur les caractères à lire.
    user2ocrPIL = Image.open(image)
    imToOcr = cropPicture(user2ocrPIL, x0, y0, largeurX, longueurY)
    # imToOcr.show()

    # Application de l'ocr à l'image traitée.
    text = pytesseract.image_to_string(imToOcr)
    # Renvoie la lecture sous forme de tableau.
    lignes = []
    info = ''

    for i in text:
        if i != '\n':
            info += i
        else:
            lignes.append(info)
            info = ''
    lignes.append(info)
    lignes = list(filter(None, lignes))
    return lignes


def getInfosUser(cardPicture):
    """Récupère les informations du l'utilisateur présents sur la carte.

    :Exemple:

    >>> from GMO import camera
    >>> picture = getCardPicture()
    >>> getInfosUser(picture)

    :param cardPicture: L'image de la carte à traiter
    :type cardPicture: PIL.Image.Image
    :return: Les données de l'utilisateur
    :rtype: list
    """
    # Recupération des paramètres de recadrage
    param = config.readParam('camera.texteToOcr.taille')

    x, y = param['origineX'], param['origineY']
    largeur, longueur = param['tailleX'], param['tailleY']

    return textFromImageOcr(cardPicture, x, y, largeur, longueur)


def getUserName(infosOcr):
    """Recupère le nom de l'utilisateur après l'OCR.

    :Exemple:

    >>> from GMO import camera
    >>> picture = getCardPicture()
    >>> ocr = getInfosUser(picture)
    >>> getUserName(ocr)

    :param infosOcr: Les informations lues sur la carte
    :type infosOcr: list
    :return: Le nom de l'utilisateur
    :rtype: str
    """
    userName = infosOcr[0]
    return userName


def getUserFirstName(infosOcr):
    """Recupère le prénom de l'utilisateur après l'OCR.

    :Exemple:

    >>> from GMO import camera
    >>> picture = getCardPicture()
    >>> ocr = getInfosUser(picture)
    >>> getUserFirstName(ocr)

    :param infosOcr: Les informations lues sur la carte
    :type infosOcr: list
    :return: Le prénom de l'utilisateur
    :rtype: str
    """
    userFirstName = infosOcr[1]
    return userFirstName


def validLetters(mot):
    """Valide que le nom ou prénom ne contient pas de chiffre.

    :Exemple:

    >>> from GMO import camera
    >>> picture = getCardPicture()
    >>> ocr = getInfosUser(picture)
    >>> name = getUserName(ocr)
    >>> validLetters(name)

    :param mot: Le mot à vérifier
    :type mot: str
    :return: Mot validé ou non
    :rtype: boolean
    """
    return mot not in string.digits


def checkPixelValidity(cardPicture, x, y, rValid, gValid, bValid):
    """Validation de la carte selon la couleur de différents pixels à tester.

    :Exemple:

    >>> from GMO import camera
    >>> picture = getCardPicture()
    >>> checkCardValidity(picture)

    :param cardPicture: La carte à vérifier
    :type cardPicture: PIL.Image.Image
    :param x: L'abscisse du pixel à valider
    :type x: int
    :param y: L'ordonnée du pixel à valider
    :type y: int
    :param rValid: la valeur 
    :return: Carte validée ou non
    :rtype: boolean
    """
    pixelValid = False

    picturePIL = Image.open(cardPicture)
    imToTest = picturePIL.convert('RGB')
    # imToTest.show()

    # Récupère les pixels à vérifier

    param = config.readParam('camera.validerCarte')

    # pixelValid = len(param['pixels']) * [False]

    # Tolerance pour la validation
    tolerH = param['tolerance']['h']
    tolerL = param['tolerance']['l']
    tolerS = param['tolerance']['s']

    # for i in range(len(param['pixels'])):

    # x = param['pixels'][str(i)]['coordonnees']['coordX']
    # y = param['pixels'][str(i)]['coordonnees']['coordY']

    rTry, gTry, bTry = imToTest.getpixel((x, y))
    hTry, lTry, sTry = colorsys.rgb_to_hls(
        rTry / 256,
        gTry / 256,
        bTry / 256
    )

    # rValid = param['pixels'][str(i)]['couleur']['r'] / 256
    # gValid = param['pixels'][str(i)]['couleur']['g'] / 256
    # bValid = param['pixels'][str(i)]['couleur']['b'] / 256
    lValid, hValid, sValid = colorsys.rgb_to_hls(rValid, gValid, bValid)
    print(hValid, lValid, sValid)

    # Condition de validation
    hInf = (hValid * 360 - tolerH) % 360
    hSup = (hValid * 360 + tolerH) % 360
    print(hInf, round(hTry * 360), hSup)

    lInf = 0 if (lValid - tolerL < 0) else round((lValid - tolerL) * 100)
    lSup = 100 if (lValid + tolerL > 1) else round((lValid + tolerL) * 100)
    print(lInf, round(lTry * 100), lSup)

    sInf = 0 if (sValid - tolerS < 0) else round((sValid - tolerS) * 100)
    sSup = 100 if (sValid + tolerS > 1) else round((sValid + tolerS) * 100)
    print(sInf, round(sTry * 100), sSup)

    if(hInf < hTry * 360 < hSup and
            lInf < lTry * 100 < lSup and sInf < sTry * 100 < sSup):
        pixelValid = True

    return pixelValid


def checkPixelValidityHSL(cardPicture, x, y, hValid=-1, lValid=-1, sValid=-1):
    """Validation de la carte selon la couleur de différents pixels à tester.

    :Exemple:

    >>> from GMO import camera
    >>> picture = getCardPicture()
    >>> checkPixelValidityHSL(picture)

    :param cardPicture: La carte à vérifier
    :type cardPicture: PIL.Image.Image
    :return: Carte validée ou non
    :rtype: boolean
    """
    pixelValid = True

    picturePIL = Image.open(cardPicture)
    imToTest = picturePIL.convert('RGB')

    param = config.readParam('camera.validerCarte')

    tolerH = param['tolerance']['h']
    tolerL = param['tolerance']['l']
    tolerS = param['tolerance']['s']

    rTry, gTry, bTry = imToTest.getpixel((x, y))
    hTry, lTry, sTry = colorsys.rgb_to_hls(
        rTry / 256,
        gTry / 256,
        bTry / 256
    )

    if hValid >= 0:
        hInf = (hValid * 360 - tolerH) % 360
        hSup = (hValid * 360 + tolerH) % 360
        print(hInf, round(hTry * 360), hSup)
        if not (hInf < hTry * 360 < hSup):
            pixelValid = False

    if lValid >= 0:
        lInf = 0 if (lValid - tolerL < 0) else (lValid - tolerL)
        lSup = 100 if (lValid + tolerL > 100) else (lValid + tolerL)
        if not(lInf < lTry * 100 < lSup):
            pixelValid = False

    if sValid >= 0:
        sInf = 0 if (sValid - tolerS < 0) else (sValid - tolerS)
        sSup = 100 if (sValid + tolerS > 100) else (sValid + tolerS)
        # print(sInf, round(sTry * 100), sSup, lInf, round(lTry * 100), lSup)
        if not (sInf < sTry * 100 < sSup):
            pixelValid = False

    return pixelValid
