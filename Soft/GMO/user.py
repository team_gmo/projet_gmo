# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Everything we need for a user."""
# cd / && run-parts --report /etv/cron.hourly

from GMO import bdd, camera
import time
from PIL import Image


def create(cardID, cardPicture):
    """Read the picture and write it to the database."""

    if cardPicture is not None:
        cardPicturePIL = Image.open(cardPicture)
        cardPicturePIL.save(cardPicture, 'png')

        userPicture = camera.getUserPicture(cardPicture)

        infosUser = camera.getInfosUser(cardPicture)
        if infosUser == []:
            return False
        userName = camera.getUserName(infosUser)
        userFirstName = camera.getUserFirstName(infosUser)

        if userName == "" or userFirstName == "":
            return False

        print(userName, userFirstName)

        set(
            cardPicture.getvalue(),
            userName, userFirstName,
            userPicture.getvalue(),
            cardID,
            0
        )

        return True

    return False


def get(user_id):
    """Return a user of the database."""
    conn = bdd.open()
    data = (user_id,)
    user = bdd.get_one(
        conn,
        'SELECT user.nom, user.prenom, user.rowid FROM user WHERE user.rowid=?',
        data
    )
    bdd.close(conn)

    return user


def get_pictures(user_id):
    """Return a user of the database."""
    conn = bdd.open()
    data = (user_id,)
    user = bdd.get_one(
        conn,
        'SELECT user.photo, user.visage FROM user WHERE user.rowid=?',
        data
    )
    bdd.close(conn)

    return user


def get_by_card(card_id):
    """Return a user of the database."""
    conn = bdd.open()
    data = (card_id,)
    user = bdd.get_one(
        conn,
        'SELECT user.nom, user.prenom, user.rowid, user.admin \
         FROM user \
         WHERE user.card=?',
        data
    )
    bdd.close(conn)

    return user


def set(photo, nom, prenom, visage, card, admin):
    """Add a user in the database."""
    conn = bdd.open()
    data = (photo, nom, prenom, visage, int(time.time()), card, admin)
    res = bdd.execute(
        conn,
        'INSERT INTO user VALUES (?, ?, ?, ?, ?, ?, ?)',
        data
    )
    bdd.close(conn)
    return res


def update(user_id, nom, prenom, card, admin):
    """Update user information."""
    conn = bdd.open()
    data = (nom, prenom, card, admin, user_id,)
    res = bdd.execute(
        conn,
        'UPDATE user \
        SET nom = ?, prenom = ?, card  = ?, admin = ? \
        WHERE user.rowid = ?',
        data
    )
    bdd.close(conn)
    return res


def get_categories(id_user):
    """Return the list of product of a user."""
    conn = bdd.open()
    data = (id_user,)
    categories = bdd.get_all(
        conn,
        "SELECT categorie.nom \
        FROM categorie \
        INNER JOIN produit ON produit.id_cat = categorie.id \
        WHERE produit.id_user = ?",
        data
    )
    bdd.close(conn)
    return categories


def get_user_emprunt_for_long_time(temps, col, sens):
    "Retourne les utilisateurs ayant empuntés un produit depuis longtemps"
    conn = bdd.open()
    data = (int(time.time()), temps,)
    resultat = bdd.get_all(
        conn,
        "SELECT user.nom,\
                user.prenom,\
                categorie.nom, \
                bb.date\
        FROM big_brother AS bb \
        LEFT JOIN user ON bb.id_user=user.rowid \
        LEFT JOIN produit ON bb.id_produit=produit.rowid \
        LEFT JOIN categorie ON produit.id_cat=categorie.id \
        WHERE bb.etat=4 AND bb.rowid_link=0 \
        AND (?-bb.date)> ?",
        data
    )
    bdd.close(conn)
    return resultat


def search(requete, col, sens, limit, demarre, num):
    """Return le resultat de la requête du user."""
    conn = bdd.open()

    if num == 1:
        data = (requete, "", "", "", limit, demarre)
    else:
        requete = '%' + requete + '%'
        data = (requete, requete, requete, requete, limit, demarre)
    resultat = bdd.get_all(
        conn,
        "SELECT user.rowid, \
                user.nom, \
                user.prenom, \
                user.inscription, \
                user.nom || ' ' || user.prenom AS nom_prenom, \
                user.prenom || ' ' || user.nom AS prenom_nom, \
                ( \
                    SELECT COUNT(*) \
                    FROM produit \
                    WHERE produit.id_user = user.rowid \
                ) AS nb_emprunt, \
                user.card, \
                user.admin \
        FROM user \
        WHERE user.rowid LIKE ? \
        OR user.card LIKE ? \
        OR nom_prenom LIKE ? \
        OR prenom_nom LIKE ? \
        ORDER BY " + col + " " + sens + "\
        LIMIT ?, ?",
        data
    )
    bdd.close(conn)
    return resultat


def search_size(requete):
    """Retourne le resultat de la requête du user."""
    conn = bdd.open()
    requete = '%' + requete + '%'
    data = (requete, requete, requete)
    resultat = bdd.get_one(
        conn,
        "SELECT COUNT(*), \
        user.nom || ' ' || user.prenom AS nom_prenom, \
        user.prenom || ' ' || user.nom AS prenom_nom \
        FROM user \
        WHERE user.rowid LIKE ? \
        OR nom_prenom LIKE ? \
        OR prenom_nom LIKE ?",
        data
    )
    bdd.close(conn)
    return resultat


def get_emprunt(user_id):
    """Retourne tous les emprunts d'un utilisateur."""
    conn = bdd.open()
    data = (user_id,)
    resultat = bdd.get_all(
        conn,
        "SELECT produit.tag_id, \
                categorie.nom, \
                bb.date, \
                bbb.date, \
                bb.id_user \
        FROM big_brother AS bb \
        LEFT JOIN big_brother AS bbb ON bb.rowid_link=bbb.rowid \
        LEFT JOIN produit ON bb.id_produit=produit.rowid \
        LEFT JOIN categorie ON produit.id_cat=categorie.id \
        WHERE bb.id_user=? \
        AND bb.etat=4",
        data
    )
    bdd.close(conn)
    return resultat


def get_pending_emprunt(user_id):
    """Return all the pending emprunt for a given user."""
    conn = bdd.open()
    data = (user_id,)
    resultat = bdd.get_all(
        conn,
        "SELECT produit.rowid, categorie.nom, bb.date \
        FROM big_brother AS bb \
        LEFT JOIN produit ON bb.id_produit=produit.rowid \
        LEFT JOIN categorie ON produit.id_cat=categorie.id \
        WHERE bb.rowid_link=0 AND bb.etat=1 AND bb.id_user = ?",
        data
    )
    bdd.close(conn)
    return resultat


def get_quantity():
    """Return le nombre d'utilisateurs enregistrés en base."""
    conn = bdd.open()
    nb_prod = bdd.get_one(conn, 'SELECT COUNT(*) FROM user', ())
    bdd.close(conn)
    return nb_prod
