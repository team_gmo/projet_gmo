# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Ce module implémente les différentes fonctions nécessaires à l'utilisation de
la table armoire définie en BDD.
La table armoire enregistre trois informations dans les champs suivants :

    * La clée publique associée à une armoire
      (voir :py:mod:`GMO.security`)
    * Un texte précisent l'emplacement d'une armoire. Ce dernier permet de
      guider un utilisateur vers une armoire disposant d'un produit.
    * Un booléen Définissant l'état d'une armoire.

"""

from GMO import bdd


def set(public_key, description):
    """
    Ajoute une armoire dans la base de donnée.

    Par défaut, lors de sa création une armoire est initialisée à l'état 0 sauf
    si cette dernière est l'armoire maitre.

    :param public_key: La clé publique générée par :py:mod:`GMO.security`
    :type public_key: str
    :param description: Le texte informant de la localisation d'une armoire
    :type description: str
    :return: la valeur du rowid de l'armoire nouvellement créée.
    :rtype: int
    """
    conn = bdd.open()
    data = (public_key, description,)
    res = bdd.execute_lrid(conn, 'INSERT INTO armoire VALUES (?, ?, 0)', data)
    bdd.close(conn)
    return res


def update(id_armoire, description):
    """
    Modifie le texte informant de la localisation d'une armoire.

    :param id_armoire: rowid attribué par sqlite correspondant à l'armoire.
    :type id_armoire: int
    :param description: Le texte informant de la localisation d'une armoire.
    :type description: str
    :return: Le nombre de ligne affecté par la modification.
    :rtype: int
    """
    conn = bdd.open()
    data = (description, id_armoire,)
    res = bdd.execute(
        conn,
        'UPDATE armoire SET armoire.decription = ? \
        WHERE armoire.rowid = ?',
        data
    )
    bdd.close(conn)
    return res


def delete(id_armoire):
    """
    Supprime une armoire en fonction de son rowid.

    :param id_armoire: rowid attribué par sqlite correspondant à l'armoire.
    :type id_armoire: int
    :return: Le nombre de ligne affecté par la modification.
    :rtype: int
    """
    conn = bdd.open()
    data = (id_armoire,)
    bdd.execute(conn, 'DELETE FROM armoire WHERE armoire.rowid = ?', data)
    bdd.close(conn)
    return


def get_all():
    """
    Retourne un tableau des armoires du réseau.

    Permet de connaitre les informations suivante :

        * `rowid` d'une armoire.
        * `public_key` d'une armoire.
        * `emplacement` d'une armoire.
        * Si une armoire est `active`

    :return: Le tableau regroupant les informations décritent ci-dessus.
    :rtype: list
    """
    conn = bdd.open()
    data = ()
    res = bdd.get_all(
        conn,
        'SELECT armoire.rowid, \
                armoire.public_key, \
                armoire.emplacement, \
                armoire.active \
        FROM armoire',
        data
    )
    bdd.close(conn)
    return res


def get_public_key(id_armoire):
    """
    Retourne la clé publique d'une armoire.

    :param id_armoire: rowid attribué par sqlite correspondant à l'armoire.
    :type id_armoire: int
    :return: La clé publique de l'armoire.
    :rtype: str
    """
    conn = bdd.open()
    data = (int(id_armoire),)
    res = bdd.get_one(
        conn,
        'SELECT armoire.public_key FROM armoire WHERE armoire.rowid = ?',
        data
    )
    bdd.close(conn)
    return res[0]
