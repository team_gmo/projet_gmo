# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Le module :py:mod:`GMO.serie` du projet GMO.

Il regroupe toute les fonctions permettant de communiquer à travers un port
série.
Il est également de détecter son emplacement d'exécution. S'il n'est pas sur
un raspberry pi il n'envoie pas les données et n'essaye pas d'ouvrir le port.
A la place il se contente d'écrire les données sur la sortie standart.

Afin d'utiliser correctement ce module il est demandé de :

    * D'ouvrir le port :py:func:`GMO.serie.open`
    * Lire et/ou écrire de l'information :py:func:`GMO.serie.read`
      :py:func:`GMO.serie.write`
    * Fermer le port :py:func:`GMO.serie.close`

Pour information, sur ce projet, les ports suivant sont ou ont pu être
utilisé :

    * Port PI : /dev/ttyAMA0
    * Port scanner : /dev/ttyACM0

Les ports sont initialiés avec un baudrate de 9600bits/s.

:Exemple:

>>> from GMO import serie
>>> port = serie.open("/dev/ttyAMA0")
>>> recieved_message = serie.read(port)
>>> serie.close(port)
>>> type(recieved_message)
<class 'str'>
"""

try:
    import serial
    rasp_pi = True
except ImportError:
    rasp_pi = False


def open(port):
    """
    Ouvre un port série et revoie l'objet le représentant.

    :param port: L'adresse du port série.
    :type port: str
    :return: Objet représentant le port série.
    """
    if not rasp_pi:
        print("Port ouvert !")
        return port
    port = serial.Serial(port, baudrate=9600, timeout=2.0)
    return port


def close(port):
    """
    Ferme un port série.

    :param port: Objet représentant le port série.
    """
    if not rasp_pi:
        print("Port fermé !")
        return
    port.close()


def read(port):
    """
    Lit une ligne envoyé sur un port série.

    Cette fonction est bloquante et ne retourne que lorsqu'une ligne est reçue.
    Une ligne est considérée finie à la réception du caractère \\n.

    :param port: Objet représentant le port série.
    :return: Information reçue.
    :rtype: str
    """
    print("RASPI = " + str(rasp_pi))
    if not rasp_pi:
        return "NOPI"

    recieved_message = ""
    while(recieved_message == ""):
        recieved_message = port.readline()
        recieved_message = recieved_message.decode('utf8').replace('\n', '')
    return recieved_message


def write(port, message):
    """
    Ecrit une chaine de caractère sur un port série.

    Elle renvoit le nombre de caractère écrit une fois le message envoyé.

    :param port: Objet représentant le port série.
    :param message: Message à envoyer
    :type message: str
    :return: Nombre de caractère envoyé
    :rtype: int
    """
    if not rasp_pi:
        return 1

    message_encoded = message.encode()
    nb_of_bytes = port.write(message_encoded)
    print("WRIIITE", message_encoded, nb_of_bytes)
    return nb_of_bytes
