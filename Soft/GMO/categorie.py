# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Everything we need for a categorie."""

from GMO import bdd


def set(nom, hauteur, largeur, profondeur, photo):
    """Add a new categorie in database."""
    conn = bdd.open()
    data = (nom, hauteur, largeur, profondeur, photo,)
    bdd.execute(conn, 'INSERT INTO categorie(nom,  \
                                             hauteur, \
                                             largeur, \
                                             profondeur, \
                                             photo)  \
                       VALUES (?,?,?,?,?)', data)
    bdd.close(conn)
    return


def update(nom, hauteur, largeur, profondeur, photo, cat_id):
    """Update a categorie in database."""
    conn = bdd.open()
    data = (nom, hauteur, largeur, profondeur, photo, cat_id,)
    bdd.execute(
        conn,
        'UPDATE categorie \
        SET categorie.nom = ?, categorie.hauteur = ?, \
        categorie.largeur = ?, categorie.profondeur = ?, categorie.photo = ? \
        WHERE categorie.id = ?', data)
    bdd.close(conn)
    return


def update_without_files(nom, hauteur, largeur, profondeur, cat_id):
    """Update a categorie in database without the picture."""
    conn = bdd.open()
    data = (nom, hauteur, largeur, profondeur, cat_id,)
    bdd.execute(conn,
                'UPDATE categorie \
                SET categorie.nom = ?, categorie.hauteur = ?, \
                categorie.largeur = ?, categorie.profondeur = ? \
                WHERE categorie.id = ?', data)
    bdd.close(conn)
    return


def delete(cat_id):
    """Delete a user selected by his id."""
    conn = bdd.open()
    data = (cat_id,)
    bdd.execute(conn, 'DELETE FROM categorie WHERE categorie.id = ?', data)
    bdd.close(conn)
    return


def get_picture(id_cat):
    """Return a categorie as asked."""
    conn = bdd.open()
    data = (id_cat,)
    res = bdd.get_one(conn, 'SELECT categorie.photo FROM categorie \
                    WHERE categorie.id = ?', data)
    bdd.close(conn)
    return res


def get_all():
    """Retourne un tableau listant toute les categories enregistrée."""
    conn = bdd.open()
    data = ()
    res = bdd.get_all(conn, 'SELECT categorie.id, categorie.nom FROM categorie', data)
    bdd.close(conn)
    return res


def get_available(id_armoire):
    """Retourne les catégories ayant un produit disponible dans une armoire.

    :Exemple:

    >>> get_available(0)

    :param id_armoire: Numéro de l'armoire stockant les produits
    :type id_armoire: int
    :return: Tableau des catégories dont un objet est disponible
    """
    conn = bdd.open()
    data = (id_armoire,)
    res = bdd.get_all(
        conn,
        "SELECT categorie.nom, \
                categorie.id, \
                ( \
                SUM \
                    ( \
                        CASE WHEN casier.id_armoire = ? AND \
                        ( \
                            SELECT big_brother.etat \
                            FROM big_brother \
                            WHERE produit.rowid = big_brother.id_produit \
                            ORDER BY big_brother.date DESC \
                            LIMIT 1 \
                        ) = 3 \
                        THEN 1  \
                        ELSE 0 \
                        END \
                    ) \
                ) AS available, \
                armoire.emplacement \
         FROM categorie \
         LEFT JOIN produit ON categorie.id = produit.id_cat \
         LEFT JOIN casier ON produit.rowid = casier.id_produit \
         LEFT JOIN armoire ON casier.id_armoire = armoire.rowid \
         WHERE produit.id_user = 0 \
         GROUP BY categorie.id \
         ORDER BY available DESC", data)
    bdd.close(conn)
    return res


def get_available_by_six(start, id_armoire):
    """Retourne les catégories ayant un produit disponible dans une armoire.

    :Exemple:

    >>> get_available(0)

    :param id_armoire: Numéro de l'armoire stockant les produits
    :type id_armoire: int
    :return: Tableau des catégories dont un objet est disponible
    """
    conn = bdd.open()
    data = (id_armoire, start)
    res = bdd.get_all(
        conn,
        "SELECT categorie.nom, \
                categorie.id, \
                ( \
                SUM \
                    ( \
                        CASE WHEN casier.id_armoire = ? AND \
                        ( \
                            SELECT big_brother.etat \
                            FROM big_brother \
                            WHERE produit.rowid = big_brother.id_produit \
                            ORDER BY big_brother.date DESC \
                            LIMIT 1 \
                        ) = 3 \
                        THEN 1  \
                        ELSE 0 \
                        END \
                    ) \
                ) AS available, \
                armoire.emplacement \
         FROM categorie \
         LEFT JOIN produit ON categorie.id = produit.id_cat \
         LEFT JOIN casier ON produit.rowid = casier.id_produit \
         LEFT JOIN armoire ON casier.id_armoire = armoire.rowid \
         WHERE produit.id_user = 0 \
         GROUP BY categorie.id \
         ORDER BY available DESC \
         LIMIT 6 OFFSET ?", data)
    bdd.close(conn)
    return res


def search(requete, col, sens, limit, demarre):
    """Return le resultat de la requête du user."""
    conn = bdd.open()

    requete = '%' + requete + '%'
    data = (requete, requete, limit, demarre)
    resultat = bdd.get_all(
        conn,
        "SELECT categorie.id, \
                categorie.nom, \
                categorie.hauteur, \
                categorie.largeur, \
                categorie.profondeur, \
                ( \
                    SELECT COUNT(*) \
                    FROM produit \
                    WHERE produit.id_cat=categorie.id \
                ) AS nb_pdt, \
                ( \
                    SELECT COUNT(*) \
                    FROM produit \
                    LEFT JOIN big_brother \
                    ON produit.rowid = big_brother.id_produit \
                    WHERE big_brother.etat = 0  \
                    AND produit.id_cat=categorie.id \
                ) AS nb_pdt_hors_systeme, \
                ( \
                    SELECT COUNT(*) \
                    FROM produit \
                    WHERE produit.id_user !=0 \
                    AND produit.id_cat=categorie.id \
                ) AS nb_emprunts \
        FROM categorie \
        WHERE categorie.id LIKE ? \
        OR categorie.nom LIKE ? \
        ORDER BY " + col + " " + sens + "\
        LIMIT ?, ?",
        data
    )
    bdd.close(conn)
    return resultat


def search_size(requete):
    """Return le nb de resultat de la requête du user."""
    conn = bdd.open()
    requete = '%' + requete + '%'
    data = (requete, requete)
    resultat = bdd.get_one(
        conn,
        "SELECT COUNT(*) \
        FROM categorie \
        WHERE categorie.id LIKE ? \
        OR categorie.nom LIKE ?", data
    )
    bdd.close(conn)
    return resultat
