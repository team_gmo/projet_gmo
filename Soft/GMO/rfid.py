# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Le module :py:mod:`GMO.rfid` du projet GMO.

Ce module permet de lire l'identifiant d'une carte sur un lecteur donné.
Ce lecteur doit utilisé une puce PN532 et être cablé en sur un bus SPI.
Le module détecte automatiquement s'il est executé sur une Raspberry Pi.
Si ce n'est pas le cas il attend 1 seconde et revoie l'identifiant de carte
suivant : `04584c4a104f80`

:Exemple:

>>> from GMO import rfid
>>> cardId = rfid.getTagID(18, 24, 23, 25)
>>> print(cardId)
<class 'str'>
"""

import binascii

import time

try:
    import Adafruit_PN532 as PN532
    rasp_pi = True
except ImportError:
    rasp_pi = False


def getTagID(cs, mosi, miso, sclk):
    """
    Retourne l'ID d'une carte a porté de lecture d'un lecteur.

    :param cs: Le numéro de la pin où est connectée la broche CS.
    :type cs: int
    :param mosi: le numéro de la pin où est connectée la broche MOSI.
    :type mosi: int
    :param miso: Le numéro de la pin où est connectée la broche MISO.
    :type miso: int
    :param sclk: Le numéro de la pin où est connectée la broche SCLK.
    :type sclk: int
    :return: L'ID de la carte.
    :rtype: str
    """
    if not rasp_pi:
        time.sleep(1)
        # return b'0'.decode("utf-8")
        return b'04584c4a104f80'.decode("utf-8")

    # Create an instance of the PN532 class.
    pn532 = PN532.PN532(cs=cs, sclk=sclk, mosi=mosi, miso=miso)

    # Call begin to initialize communication with the PN532.  Must be done
    # before any other calls to the PN532!
    pn532.begin()

    # Configure PN532 to communicate with MiFare cards.
    pn532.SAM_configuration()

    # Main loop to detect cards and read a block.
    cardID = 0

    # Check if a card is available to read.
    try:
        uid = pn532.read_passive_target()
    except:
        uid = b'0'.decode("utf-8")

    if uid is None:
        return b'0'.decode("utf-8")

    cardID = binascii.hexlify(uid).decode("utf-8")

    return cardID
