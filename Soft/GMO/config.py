# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Accès en lecture et écriture au fichier de paramètres."""

import json

param_file_path = './config/param.json'


def readParam(route):
    """Obtient un paramètre en fonction de son chemin dans le fichier de config.

    :Exemple:

    >>> from GMO import config
    >>> config.readParam('rfid.portsPI.CS')

    :param route: Le chemin où trouver au paramètre
    :type route: str
    :return: Le paramètre recherché
    """
    with open(param_file_path) as param_file:
        param = json.load(param_file)

    paramToParse = route.split('.')

    for element in paramToParse:
        param = param[element]
    return param


def writeParam(route, value):
    """Ajoute un paramètre en fonction de son chemin dans le fichier de config.

    :Exemple:

    >>> from GMO import config
    >>> config.writeParam('Projet','GMO')

    :param route: Le chemin où ajouter le parametre
    :type route: str
    :param value: Le paramètre à ajouter
    """

    try:
        with open(param_file_path) as param_file:
            allParams = json.load(param_file)
    except json.decoder.JSONDecodeError:
        allParams = {}
    except FileNotFoundError:
        allParams = {}

    tabOfRoute = route.split('.')
    parametre = allParams
    i = -1

    for i in range(0, len(tabOfRoute) - 1):
        try:
            parametre = parametre[tabOfRoute[i]]
        except KeyError:
            parametre[tabOfRoute[i]] = {}
            parametre = parametre[tabOfRoute[i]]

    parametre[tabOfRoute[i + 1]] = value

    with open(param_file_path, 'w') as param_file:
        json.dump(allParams, param_file, indent=4, sort_keys=True)
    return


def setDefaultMaitre():
    """Inscrit une configuration par défaut au fichier param.json

    :Exemple:

    >>> from GMO import config
    >>> config.setDefault()
    """

    writeParam('admin.idt', 'valentin')
    writeParam('admin.mdp', 'cb24eaf945bb98b4b847659edc6a4cecc15ae432923ded1bbc6b0e479fa6f6e92a7da6f8ccc4970ff6c4291c59fc420253ba7daa07443d5b4ecea56406665f03') # arboux

    writeParam('camera.texteToOcr.taille.origineX', 676)
    writeParam('camera.texteToOcr.taille.origineY', 428)
    writeParam('camera.texteToOcr.taille.tailleX', 881)
    writeParam('camera.texteToOcr.taille.tailleY', 400)

    writeParam('camera.userPicture.taille.origineX', 300)
    writeParam('camera.userPicture.taille.origineY', 390)
    writeParam('camera.userPicture.taille.tailleX', 602)
    writeParam('camera.userPicture.taille.tailleY', 846)

    writeParam('camera.validerCarte.pixels.0.coordonnees.coordX', 282)
    writeParam('camera.validerCarte.pixels.0.coordonnees.coordY', 495)
    writeParam('camera.validerCarte.pixels.0.couleur.b', 25)
    writeParam('camera.validerCarte.pixels.0.couleur.g', 84)
    writeParam('camera.validerCarte.pixels.0.couleur.r', 188)

    writeParam('camera.validerCarte.pixels.1.coordonnees.coordX', 282)
    writeParam('camera.validerCarte.pixels.1.coordonnees.coordY', 495)
    writeParam('camera.validerCarte.pixels.1.couleur.b', 25)
    writeParam('camera.validerCarte.pixels.1.couleur.g', 84)
    writeParam('camera.validerCarte.pixels.1.couleur.r', 188)

    writeParam('camera.validerCarte.tolerance.h', 20)
    writeParam('camera.validerCarte.tolerance.l', 10)
    writeParam('camera.validerCarte.tolerance.s', 10)

    writeParam('dashboard.accueil.tempsEmprunts', 1209600)

    writeParam('floor_pi.0', 1)
    writeParam('floor_pi.1', 1)
    writeParam('floor_pi.2', 4)

    writeParam('master.address', "http://localhost:5000")

    writeParam("me.id", 1)

    writeParam("rfid_product.portsPI.CS", 27)
    writeParam("rfid_product.portsPI.MISO", 10)
    writeParam("rfid_product.portsPI.MOSI", 22)
    writeParam("rfid_product.portsPI.SCLK", 9)

    writeParam("rfid_user.portsPI.CS", 18)
    writeParam("rfid_user.portsPI.MISO", 24)
    writeParam("rfid_user.portsPI.MOSI", 23)
    writeParam("rfid_user.portsPI.SCLK", 25)

def setDefaultEsclave():
    """Inscrit une configuration par défaut au fichier param.json

    :Exemple:

    >>> from GMO import config
    >>> config.setDefault()
    """

    writeParam('admin.idt', 'valentin')
    writeParam('admin.mdp', 'cb24eaf945bb98b4b847659edc6a4cecc15ae432923ded1bbc6b0e479fa6f6e92a7da6f8ccc4970ff6c4291c59fc420253ba7daa07443d5b4ecea56406665f03') # arboux

    writeParam('camera.texteToOcr.taille.origineX', 800)
    writeParam('camera.texteToOcr.taille.origineY', 800)
    writeParam('camera.texteToOcr.taille.tailleX', 480)
    writeParam('camera.texteToOcr.taille.tailleY', 360)

    writeParam('camera.userPicture.taille.origineX', 500)
    writeParam('camera.userPicture.taille.origineY', 300)
    writeParam('camera.userPicture.taille.tailleX', 400)
    writeParam('camera.userPicture.taille.tailleY', 500)

    writeParam('camera.validerCarte.pixels.0.coordonnees.coordX', 282)
    writeParam('camera.validerCarte.pixels.0.coordonnees.coordY', 495)
    writeParam('camera.validerCarte.pixels.0.couleur.b', 25)
    writeParam('camera.validerCarte.pixels.0.couleur.g', 84)
    writeParam('camera.validerCarte.pixels.0.couleur.r', 188)

    writeParam('camera.validerCarte.pixels.1.coordonnees.coordX', 282)
    writeParam('camera.validerCarte.pixels.1.coordonnees.coordY', 495)
    writeParam('camera.validerCarte.pixels.1.couleur.b', 25)
    writeParam('camera.validerCarte.pixels.1.couleur.g', 84)
    writeParam('camera.validerCarte.pixels.1.couleur.r', 188)

    writeParam('camera.validerCarte.tolerance.h', 20)
    writeParam('camera.validerCarte.tolerance.l', 0.1)
    writeParam('camera.validerCarte.tolerance.s', 0.25)

    writeParam('dashboard.accueil.tempsEmprunts', 1209600)

    writeParam('floor_pi.0', 1)
    writeParam('floor_pi.1', 1)
    writeParam('floor_pi.2', 4)

    writeParam('master.address', "http://172.17.1.40:5000")

    writeParam("rfid_product.portsPI.CS", 27)
    writeParam("rfid_product.portsPI.MISO", 10)
    writeParam("rfid_product.portsPI.MOSI", 22)
    writeParam("rfid_product.portsPI.SCLK", 9)

    writeParam("rfid_user.portsPI.CS", 18)
    writeParam("rfid_user.portsPI.MISO", 24)
    writeParam("rfid_user.portsPI.MOSI", 23)
    writeParam("rfid_user.portsPI.SCLK", 25)
