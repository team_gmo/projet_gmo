# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Le module :py:mod:`GMO.barcode` du projet GMO.

Ce module permet d'envoyer via un port série des commandes à un lecteur de code
barres branché sur l'ordinateur.

Le port série est piloté en utilisant :py:mod:`GMO.serie`

:Exemple:

>>> from GMO import barcode
>>> reponse = barcode.read()
>>> print(reponse)
<class 'str'>

Cet exemple permet d'attendre la réception d'un code barre.
"""

from GMO import serie
import time


def read():
    """
    Retourne le message renvoyé par un lecteur de code barre.

    :return: Message renvoyé par le lecteur de code barre.
    :rtype: str
    """
    try:
        port = serie.open("/dev/ttyACM0")
        loopback = serie.read(port)
        serie.close(port)
        if loopback == "NOPI":
            time.sleep(120)
            print("NOOOOPI")
            return "0001"
        return loopback
    except IOError:
        print("Erreur sur " + port + "\n")
        return
