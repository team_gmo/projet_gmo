# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Le module :py:mod:`GMO.actionneur` du projet GMO.

Ce module permet d'envoyer via un port série des commandes aux actionneurs de
GMO. Physiquement un actionneur est un arduino mini pro. Chaque actionneur est
capable de piloter et lire l'état de 10 casiers.

Un actionneur est régie par 2 règles :

    * Il n'est jamais à l'origine d'une décision
    * Il répond toujours à un message reçue via un unique message

Le port série est piloté en utilisant :py:mod:`GMO.serie`

:Exemple:

>>> from GMO import actionneur
>>> actionneur.send(str(1), str(1), "O")
>>> reponse = actionneur.read()
>>> print(reponse)
<class 'str'>

Cet exemple permet d'ouvrir le casier **1** de l'actionneur **1**.
"""

from GMO import serie
import time


def send(bit_destinataire, bit_casier, bit_ordre):
    """
    Envoi un ordre à un actionneur GMO.

    :param bit_destinataire: Numéro de l'actionneur GMO affecté par l'ordre.
    :type bit_destinataire: int
    :param bit_casier: Numéro du casier affecté par l'ordre.
    :type bit_casier: int
    :param bit_ordre: Identifiant de l'ordre (voir doc protocole)
    :type bit_ordre: int
    """
    start_Byte = 'S'
    bit_envoyeur = "0"
    bit_etat = '0'
    bit_end = '\n'
    bit_destinataire = str(int(bit_destinataire))

    message_to_send = start_Byte
    message_to_send += bit_envoyeur
    message_to_send += bit_destinataire
    message_to_send += bit_ordre
    message_to_send += bit_casier
    message_to_send += bit_etat
    message_to_send += bit_end

    port = serie.open("/dev/ttyAMA0")
    serie.write(port, message_to_send)
    serie.close(port)
    return


def read():
    """
    Retourne le message renvoyé par un actionneur GMO.

    :return: Message renvoyé par l'actionneur GMO.
    :rtype: str
    """
    port = serie.open("/dev/ttyAMA0")

    recieved_message = serie.read(port)
    if recieved_message == "NOPI":
        time.sleep(30)
        return "S02E11"
    serie.close(port)
    return recieved_message
