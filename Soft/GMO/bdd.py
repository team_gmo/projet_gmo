# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Le module :py:func:`GMO.bdd` du projet GMO.

Vous devez l'utiliser si vous avez besion d'accéder à la BDD Sqlite.
L'exemple ci-dessous détail l'appel classique à la base.

**Il est obligatoire de respecter l'ordre d'appel suivant :**

    * Ouverture de la Base
    * Modification / Lecture de la base
    * Fermeture de la base

:Exemple:

>>> conn = bdd.open()
>>> data = (description, id_armoire,)
>>> sql = 'UPDATE armoire SET decription = ? WHERE rowid = ?'
>>> res = bdd.execute(conn, sql, data)
>>> bdd.close(conn)
"""

import sqlite3


def execute(connection, sql, data):
    """
    Execute une requête sql et retourne le nombre de ligne affectée.

    :param connection: La variable représentant la session de connexion.
    :type connection: sqlite3.Connection
    :param sql: Reqête sql préparée.
    :type sql: str
    :param data: Paramètre de la requête sql préparée.
    :type data: tuple
    :return: Le nombre de ligne modifiée.
    :rtype: int
    """
    cursor = connection.cursor()
    cursor.execute(sql, data)
    connection.commit()
    return cursor.rowcount


def execute_lrid(connection, sql, data):
    """
    Execute une requête sql, retourne le numéro de la dernière ligne affectée.

    :param connection: La variable représentant la session de connexion.
    :type connection: sqlite3.Connection
    :param sql: Reqête sql préparée.
    :type sql: str
    :param data: Paramètre de la requête sql préparée.
    :type data: tuple
    :return: Le numéro de la dernière ligne modifiée.
    :rtype: int
    """
    cursor = connection.cursor()
    cursor.execute(sql, data)
    connection.commit()
    return cursor.lastrowid


def get_one(connection, sql, data):
    """
    Retourne la première ligne d'une requête sql.

    :param connection: La variable représentant la session de connexion.
    :type connection: sqlite3.Connection
    :param sql: Reqête sql préparée.
    :type sql: str
    :param data: Paramètre de la requête sql préparée.
    :type data: tuple
    :return: Le tableau contenant le resultat.
    :rtype: tuple
    """
    cursor = connection.cursor()
    cursor.execute(sql, data)
    resultat = cursor.fetchone()
    return resultat


def get_all(connection, sql, data):
    """
    Retourne toute les lignes d'une requête sql.

    :param connection: La variable représentant la session de connexion.
    :type connection: sqlite3.Connection
    :param sql: Reqête sql préparée.
    :type sql: str
    :param data: Paramètre de la requête sql préparée.
    :type data: tuple
    :return: Le tableau contenant le resultat.
    :rtype: list
    """
    cursor = connection.cursor()
    cursor.execute(sql, data)
    resultat = cursor.fetchall()
    return resultat


def open():
    """
    Ouvre la connexion à la base de donnée.

    Le nom de la base est codé en dur. Cette dernière doit être placée à la
    raçine du projet et avoir pour nom ``gmo.db``.

    :return: La variable représentant la session de connexion.
    :rtype: sqlite3.Connection
    """
    connection = sqlite3.connect("gmo.db")
    return connection


def close(connection):
    """
    Commit puis ferme la connexion à la base de donnée.

    :param connection: La variable représentant la session de connexion.
    :type connection: sqlite3.Connection
    """
    connection.commit()
    connection.close()
    return


def initialise():
    """
    Initialise la bdd avec les tables adaptée.
    """
    connection = open()

    execute(
        connection,
        "CREATE TABLE `armoire` ( \
             `public_key`  INTEGER, \
             `emplacement` TEXT, \
             `active`	   INTEGER \
         )",
        ()
    )

    execute(
        connection,
        "CREATE TABLE `big_brother` ( \
             `id_user`	     INTEGER, \
             `id_produit`	 INTEGER, \
             `id_casier`	 INTEGER, \
             `etat`	         INTEGER, \
             `date`	         INTEGER, \
             `commentaire`	 TEXT, \
             `rowid_link`	 INTEGER \
         )",
        ()
    )

    execute(
        connection,
        "CREATE TABLE `casier` ( \
             `id`	      INTEGER PRIMARY KEY AUTOINCREMENT, \
             `id_user`	  INTEGER, \
             `id_produit` INTEGER, \
             `id_armoire` INTEGER, \
             `ligne`	  INTEGER, \
             `colonne`	  INTEGER \
             `verouille`  INTEGER, \
             `hauteur`	  INTEGER, \
             `largeur`	  INTEGER, \
             `profondeur` INTEGER, \
         )",
        ()
    )

    execute(
        connection,
        "CREATE TABLE `categorie` ( \
            `id`	     INTEGER PRIMARY KEY AUTOINCREMENT, \
            `nom`	     TEXT UNIQUE, \
            `photo`	     BLOB, \
            `hauteur`	 INTEGER, \
            `largeur`	 INTEGER, \
            `profondeur` INTEGER, \
         )",
        ()
    )

    execute(
        connection,
        "CREATE TABLE `produit` ( \
            `id_cat`	INTEGER, \
            `id_user`	INTEGER,  \
            `tag_id`	TEXT UNIQUE \
         )",
        ()
    )

    execute(
        connection,
        "CREATE TABLE `user` ( \
            `nom`	INTEGER, \
            `prenom`	INTEGER, \
            `card`	TEXT, \
            `admin`	INTEGER \
            `photo`	BLOB, \
            `visage`	BLOB, \
            `inscription`	INTEGER, \
        );",
        ()
    )
