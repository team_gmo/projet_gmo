"""
Le module :py:mod:`GMO.serie` du projet GMO.

Il regroupe toute les fonctions permettant de communiquer à travers un port
série.
Il est également de détecter son emplacement d'exécution. S'il n'est pas sur
un raspberry pi il n'envoie pas les données et n'essaye pas d'ouvrir le port.
A la place il se contente d'écrire les données sur la sortie standart.

Afin d'utiliser correctement ce module il est demandé de :

    * D'ouvrir le port :py:func:`GMO.serie.open`
    * Lire et/ou écrire de l'information :py:func:`GMO.serie.read`
      :py:func:`GMO.serie.write`
    * Fermer le port :py:func:`GMO.serie.close`

Pour information, sur ce projet, les ports suivant sont ou ont pu être
utilisé :

    * Port PI : /dev/ttyAMA0
    * Port scanner : /dev/ttyACM0

Les ports sont initialiés avec un baudrate de 9600bits/s.

:Exemple:

>>> from GMO import serie
>>> port = serie.open("/dev/ttyAMA0")
>>> recieved_message = serie.read(port)
>>> serie.close(port)
>>> type(recieved_message)
<class 'str'>
"""

try:
    import wiringpi
    rasp_pi = True
except ImportError:
    rasp_pi = False


def init():
    """
    Initialise la led du flash.
    """
    if not rasp_pi:
        print("Init led ok !")
        return
    wiringpi.wiringPiSetup()
    wiringpi.pinMode(24, 1)
    return


def on():
    """
    Allume la led du flash.
    """
    if not rasp_pi:
        print("Allumage de la led ok !")
        return
    wiringpi.digitalWrite(24, 1)
    return


def off():
    """
    Eteint la led du flash.
    """
    if not rasp_pi:
        print("Extinction la led ok !")
        return
    wiringpi.digitalWrite(24, 0)
    return
