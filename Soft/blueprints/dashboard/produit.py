# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""fichier regroupant les fonctions liée aux page d'admin."""

from flask import Blueprint, render_template, redirect, request, session
from flask import url_for
from GMO import product, categorie
import sqlite3

produit = Blueprint('produit',
                    __name__,
                    template_folder='../../templates/dashboard',
                    static_folder='../../static')


@produit.route('/')
def index():
    """Affiche la page de materiel."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    erreur, info = "", ""
    nb_result_max = 5

    if 'erreur' in session:
        erreur = session['erreur']
        session.pop('erreur', None)
    if 'info' in session:
        info = session['info']
        session.pop('info', None)

    if 'produit.search' not in session:
        session['produit.search'] = ""
    if 'produit.col.id' not in session:
        session['produit.col.id'] = 0
    if 'produit.sens.id' not in session:
        session['produit.sens.id'] = 0
    if 'produit.col.name' not in session:
        session['produit.col.name'] = "produit.tag_id"
    if 'produit.sens.name' not in session:
        session['produit.sens.name'] = "ASC"
    if 'produit.page' not in session:
        session['produit.page'] = 1
    if 'produit.col' not in session:
        session['produit.col'] = 0

    tab = product.search(
        session['produit.search'],
        session['produit.col.name'],
        session['produit.sens.name'],
        nb_result_max * (session['produit.page'] - 1),
        nb_result_max,
        session['produit.col']
    )

    tab_cat = categorie.get_all()

    nb_resultat = product.search_size(session['produit.search'])

    return render_template("produit_index.tpl",
                           resultat=tab,
                           tab_cat=tab_cat,
                           nb_resultat=nb_resultat[0],
                           nb_resultat_actu=len(tab),
                           search=session['produit.search'],
                           col=session['produit.col.id'],
                           sens=session['produit.sens.id'],
                           erreur=erreur,
                           info=info,
                           page=session['produit.page'],
                           nb_max_result=nb_result_max)


@produit.route('/add', methods=['GET', 'POST'])
def add():
    """Fonction qui analyse le form add produit."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    if request.method == 'POST':
        try:
            id_produit = request.form['id']
            id_cat = request.form['cat']
            product.set_product(id_produit, id_cat)
            session['info'] = "Produit ajouté"
        except sqlite3.IntegrityError:
            session['erreur'] = "L'id du produit, que vous essayez d'ajouter, \
                     existe déjà"
        except sqlite3.Error as e:
            session['erreur'] = e
        except:
            session['erreur'] = "Erreur inconnue (I was in an add mood)"
    return redirect(url_for("produit.index"))


@produit.route('/edit', methods=['GET', 'POST'])
def edit():
    """Fonction qui analyse le form add produit."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    if request.method == 'POST':
        print("id_ac")
        id_produit = request.form['id']
        print("id_ap")
        id_cat = request.form['cat']
        print("id_aa")
        try:

            produit.update_cat_produit(id_produit, id_cat)
            session['info'] = "Catégorie modifiée"
        except sqlite3.IntegrityError:
            session['erreur'] = "L'id du produit, que vous essayez d'ajouter, \
                     existe déjà"
        except sqlite3.Error as e:
            session['erreur'] = e
        except:
            session['erreur'] = "Erreur inconnue (I was in an edit mood)"
    return redirect(url_for("produit.index"))


@produit.route('/select/<rqst>/<int:col>', methods=['GET', 'POST'])
def select(rqst, col):
    """Fonction qui analyse le form de search."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))
    try:
        session['produit.search'] = rqst
        session['produit.page'] = 1
        session['produit.col'] = col
    except:
        session['erreur'] = 'Mauvais formatage de la recherche'
    return redirect(url_for("produit.index"))


@produit.route('/search', methods=['GET', 'POST'])
def search():
    """Fonction qui analyse le form de search."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))
    if request.method == 'POST':
        try:
            session['produit.search'] = request.form['search']
            session['produit.page'] = 1
            session['produit.col'] = 0
        except:
            session['erreur'] = 'Mauvais formatage de la recherche'
    return redirect(url_for("produit.index"))


@produit.route('/trie/<int:col>/<int:sens>')
def trie(col, sens):
    """Fonction qui indique le trie à faire."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    col_name = ["produit.tag_id",
                "categorie.nom",
                "user.nom",
                "etat"]

    sens_name = ["ASC", "DESC"]

    if col < 0 or col > len(col_name):
        col = 0
    if sens < 0 or sens > len(sens_name):
        sens = 0

    session['produit.col.id'] = col
    session['produit.sens.id'] = not(sens)
    session['produit.col.name'] = col_name[col]
    session['produit.sens.name'] = sens_name[sens]
    return redirect(url_for("produit.index"))


@produit.route('/page/<int:num_page>/')
def page(num_page):
    """Fonction qui indique le tri à faire."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    if num_page < 1:
        num_page = 1

    session['produit.page'] = num_page
    return redirect(url_for("produit.index"))
