# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""fichier regroupant les fonctions liée aux page d'admin."""

from flask import (
    Blueprint,
    render_template,
    redirect,
    session,
    url_for,
    request
)

from GMO import user as u
import sqlite3
import datetime
import io

user = Blueprint('user',
                 __name__,
                 template_folder='../../templates/dashboard',
                 static_folder='../../static')


@user.route('/')
def index():
    """Affiche la page de materiel."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    erreur, info = "", ""
    nb_result_max = 5

    if 'erreur' in session:
        erreur = session['erreur']
        session.pop('erreur', None)
    if 'info' in session:
        info = session['info']
        session.pop('info', None)

    if 'user.search' not in session:
        session['user.search'] = ""
    if 'user.col.id' not in session:
        session['user.col.id'] = 0
    if 'user.sens.id' not in session:
        session['user.sens.id'] = 0
    if 'user.col.name' not in session:
        session['user.col.name'] = "user.rowid"
    if 'user.sens.name' not in session:
        session['user.sens.name'] = "ASC"
    if 'user.page' not in session:
        session['user.page'] = 1
    if 'user.col' not in session:
        session['user.col'] = 0

    tab = u.search(
        session['user.search'],
        session['user.col.name'],
        session['user.sens.name'],
        nb_result_max*(session['user.page']-1),
        nb_result_max,
        session['user.col']
    )

    nb_resultat = u.search_size(session['user.search'])

    return render_template("user_index.tpl",
                           resultat=tab,
                           nb_resultat=nb_resultat[0],
                           nb_resultat_actu=len(tab),
                           search=session['user.search'],
                           col=session['user.col.id'],
                           sens=session['user.sens.id'],
                           emprunts=emprunts_user,
                           erreur=erreur,
                           info=info,
                           page=session['user.page'],
                           nb_max_result=nb_result_max,
                           timestamp2text=timestamp2text)


def timestamp2text(ts):
    """Convert timestamp to text."""
    if not ts:
        return "Emprunt en cours"
    str_ts = datetime.datetime.fromtimestamp(ts).strftime('%d-%m-%Y %H:%M:%S')
    str_ts = str(str_ts)
    return str_ts


def emprunts_user(id_user):
    """Convert id_user to a liste of emprunt."""
    emprunt = u.get_emprunt(id_user)
    return emprunt


@user.route('/select/<rqst>/<int:col>', methods=['GET', 'POST'])
def select(rqst, col):
    """Fonction qui analyse le form de search."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))
    try:
        session['user.search'] = rqst
        session['user.page'] = 1
        session['user.col'] = col
    except:
        session['erreur'] = 'Mauvais formatage de la recherche'
    return redirect(url_for("user.index"))


@user.route('/search', methods=['GET', 'POST'])
def search_by_post():
    """Fonction qui analyse le form de search."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    if request.method == 'POST':
        try:
            session['user.search'] = request.form['search']
            session['user.page'] = 1
            session['user.col'] = 0
        except:
            session['erreur'] = 'Mauvais formatage de la recherche'
    return redirect(url_for("user.index"))


@user.route('/trie/<int:col>/<int:sens>')
def trie(col, sens):
    """Fonction qui indique le trie à faire."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    col_name = ["user.rowid",
                "user.card",
                "user.nom",
                "user.prenom",
                "user.inscription",
                "nb_emprunt"]
    sens_name = ["ASC", "DESC"]

    if col < 0 or col > len(col_name):
        col = 0
    if sens < 0 or sens > len(sens_name):
        sens = 0

    session['user.col.id'] = col
    session['user.sens.id'] = not(sens)
    session['user.col.name'] = col_name[col]
    session['user.sens.name'] = sens_name[sens]
    return redirect(url_for("user.index"))


@user.route('/page/<int:num_page>/')
def page(num_page):
    """Fonction qui indique le trie à faire."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    if num_page < 1:
        num_page = 1

    session['user.page'] = num_page
    return redirect(url_for("user.index"))


@user.route('/add', methods=['GET', 'POST'])
def add():
    """Fonction qui analyse le form add produit."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    photo_d = io.BytesIO()
    if request.method == 'POST':
        try:
            card = request.form['cardid']
            nom = request.form['nom']
            prenom = request.form['prenom']
            admin = request.form['admin']
            photo = request.files['photo']
            photo.save(photo_d)
            u.set(None, nom, prenom, photo_d.getvalue(), card, admin)
            session['info'] = "Utilisateur ajouté"
        except sqlite3.IntegrityError:
            session['erreur'] = "L'id de l'utilisateur, que vous essayez d'ajouter, \
                     existe déjà"
        except sqlite3.Error as e:
            session['erreur'] = str(e)
        except:
            session['erreur'] = "Erreur inconnue (I was in an add mood)"
    return redirect(url_for("user.index"))


@user.route('/edit', methods=['GET', 'POST'])
def edit():
    """Fonction qui analyse le form edit user."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))
    if request.method == 'POST':
        try:
            id_user = request.form['id']
            card = request.form['cardid']
            nom = request.form['nom']
            prenom = request.form['prenom']
            admin = request.form['admin']
            u.update(id_user, nom, prenom, card, admin)
            session['info'] = "Utilisateur modifié"
        except sqlite3.IntegrityError:
            session['erreur'] = "Cette carte est déjà associée à un \
                utilisateur 😞"
        except sqlite3.Error as e:
            session['erreur'] = e
        except:
            session['erreur'] = "Erreur inconnue (I was in an edit mood)"
    return redirect(url_for("user.index"))
