# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""fichier regroupant les fonctions liée aux page d'admin."""

from flask import Blueprint, render_template, redirect
from flask import session, url_for
from GMO import product, user, config
import datetime

accueil = Blueprint('accueil',
                    __name__,
                    template_folder='../../templates/dashboard',
                    static_folder='../../static')


@accueil.route('/')
def index():
    """Affiche la page d'accueil."""
    if 'username' not in session:
        return redirect(url_for('.connexion'))

    erreur, info = "", ""
    nb_result_max = 5

    if 'erreur' in session:
        erreur = session['erreur']
        session.pop('erreur', None)
    if 'info' in session:
        info = session['info']
        session.pop('info', None)

    if 'accueil.col.id' not in session:
        session['accueil.col.id'] = 0
    if 'accueil.sens.id' not in session:
        session['accueil.sens.id'] = 0
    if 'accueil.col.name' not in session:
        session['accueil.col.name'] = "produit.tag_*id"
    if 'accueil.sens.name' not in session:
        session['accueil.sens.name'] = "ASC"
    if 'accueil.col' not in session:
        session['accueil.col'] = 0

    # date en timestamp
    date = config.readParam('dashboard.accueil.tempsEmprunts')
    tab = user.get_user_emprunt_for_long_time(
        date,
        session['accueil.col.name'],
        session['accueil.sens.name']
    )

    nb_user = user.get_quantity()
    nb_prod = product.get_quantity()
    nb_emprunt = product.get_quantity_state(4)
    nb_probleme = product.get_quantity_state(5)
    nb_reparation = product.get_quantity_state(6)

    return render_template("accueil_index.tpl",
                           nb_user=nb_user[0],
                           resultat=tab,
                           nb_resultat_actu=len(tab),
                           nb_prod=nb_prod[0],
                           nb_emprunt=nb_emprunt,
                           nb_probleme=nb_probleme,
                           nb_reparation=nb_reparation,
                           col=session['accueil.col.id'],
                           sens=session['accueil.sens.id'],
                           erreur=erreur,
                           info=info,
                           nb_max_result=nb_result_max,
                           timestamp2text=timestamp2text)


@accueil.route('/trie/<int:col>/<int:sens>')
def trie(col, sens):
    """Fonction qui indique le trie à faire."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    col_name = ["user.nom",
                "user.prenom",
                "categorie.nom",
                "bb.date"]
    sens_name = ["ASC", "DESC"]

    if col < 0 or col > len(col_name):
        col = 0
    if sens < 0 or sens > len(sens_name):
        sens = 0

    session['accueil.col.id'] = col
    session['accueil.sens.id'] = not(sens)
    session['accueil.col.name'] = col_name[col]
    session['accueil.sens.name'] = sens_name[sens]
    return redirect(url_for("accueil.index"))


def timestamp2text(ts):
    """Convert timestamp to text."""
    if not ts:
        return "Emprunt en cours"
    str_ts = datetime.datetime.fromtimestamp(ts).strftime('%d-%m-%Y %H:%M:%S')
    str_ts = str(str_ts)
    return str_ts
