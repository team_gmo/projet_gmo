# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""fichier regroupant les fonctions liée aux page d'admin."""

from flask import Blueprint, render_template, redirect
from flask import request, session, url_for
from GMO import product
import time
import datetime

timeline = Blueprint('timeline',
                     __name__,
                     template_folder='../../templates/dashboard',
                     static_folder='../../static')


@timeline.route('/<id_prod>', methods=['GET', 'POST'])
def index(id_prod):
    """Generate the webpage wich displayed an object's timeline."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    erreur, info = "", ""

    if 'erreur' in session:
        erreur = session['erreur']
        session.pop('erreur', None)
    if 'info' in session:
        info = session['info']
        session.pop('info', None)

    date_deb = 0
    date_fin = int(time.time())

    if request.method == 'POST':
        date_deb = time.mktime(
            datetime.datetime.strptime(
                request.form['date_deb'], "%Y-%m-%d").timetuple())
        date_fin = time.mktime(
            datetime.datetime.strptime(
                request.form['date_fin'], "%Y-%m-%d").timetuple())

    tab_timeline = product.timeline(id_prod, date_deb, date_fin)
    if not tab_timeline:
        date_deb = 0
        date_fin = int(time.time())
        tab_timeline = product.timeline(id_prod, date_deb, date_fin)

    if not tab_timeline:
        session['erreur'] = "Timeline vide"
        return redirect(url_for("produit.index"))

    return render_template("timeline_index.tpl",
                           tl=tab_timeline,
                           timestamp2text=timestamp2text,
                           erreur=erreur,
                           info=info)


def timestamp2text(ts):
    """Convert timestamp to text."""
    if not ts:
        return "Emprunt en cours"
    str_ts = datetime.datetime.fromtimestamp(ts).strftime('%d-%m-%Y %H:%M:%S')
    str_ts = str(str_ts)
    return str_ts
