# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""fichier regroupant les fonctions liée aux page d'admin."""

from flask import Blueprint, render_template, redirect
from flask import request, session, url_for
from GMO import config, product, big_brother, casier
import hashlib

main = Blueprint('main',
                 __name__,
                 template_folder='../../templates/dashboard',
                 static_folder='../../static')


@main.route('/')
def connexion():
    """Admin but still not a page."""
    return render_template("main_connexion.tpl")


@main.route('/connexion',  methods=['GET', 'POST'])
def check_connexion():
    """Enregistrement de la connexion d'un user."""
    if request.method == "POST":
        idt = request.form['idt']
        passwd = request.form['passwd']
        idt_saved = config.readParam("admin.idt")
        passwd_saved = config.readParam("admin.mdp")
        passwd = passwd.encode("utf-8")
        passwd = hashlib.sha512(passwd).hexdigest()
        if idt == idt_saved and passwd == passwd_saved:
            session['username'] = request.form['idt']
            return redirect(url_for("accueil.index"))
    return redirect(url_for("main.connexion"))


@main.route('/deconnexion')
def deconnexion():
    """Deconnexion du user de la page d'admin."""
    session.pop('username', None)
    return redirect(url_for('.connexion'))


@main.route('/emprunt', methods=['GET', 'POST'])
def emprunt():
    """Emprunt d'un produit par un user."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))
    if request.method == "POST":
        id_user = request.form['id_user']
        id_produit = request.form['id_produit']
        id_casier = casier.get_by_id_produit(id_produit)
        if id_casier is None:
            product.link_to_user(id_user, id_produit)
            big_brother.set(id_user, id_produit, 0, 4, "", None)
    return redirect(url_for('produit.index'))


@main.route('/giveBack', methods=['GET', 'POST'])
def give_back():
    """Retour d'un produit par un user."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))
    if request.method == "POST":
        prbl = request.form['prbl']
        id_produit = request.form['id_produit']
        product.give_back_reserve(id_produit)
        id_user = 6 #A changer
        if len(prbl) != 0:
            big_brother.set(id_user, id_produit, 0, 6, prbl, None)
            product.declare_prbl(id_user, id_produit, prbl)
    return redirect(url_for('produit.index'))


@main.route('/solve', methods=['GET', 'POST'])
def solve():
    """Résolution d'un probleme."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))
    if request.method == "POST":
        com = request.form['com']
        id_produit = request.form['id_produit']
        product.solve(id_produit, com)
    return redirect(url_for('produit.index'))
