# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""fichier regroupant les fonctions liée aux page d'admin."""

from flask import (
    Blueprint,
    redirect,
    request,
    session,
    url_for,
    render_template
)

from GMO import categorie as cat
import sqlite3
import io

categorie = Blueprint('categorie',
                      __name__,
                      template_folder='../../templates/dashboard',
                      static_folder='../../static')


@categorie.route('/')
def index():
    """Affiche la page de categorie."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    erreur, info = "", ""
    nb_result_max = 5

    if 'erreur' in session:
        erreur = session['erreur']
        session.pop('erreur', None)
    if 'info' in session:
        info = session['info']
        session.pop('info', None)

    if 'categorie.search' not in session:
        session['categorie.search'] = ""
    if 'categorie.col.id' not in session:
        session['categorie.col.id'] = 0
    if 'categorie.sens.id' not in session:
        session['categorie.sens.id'] = 0
    if 'categorie.col.name' not in session:
        session['categorie.col.name'] = "categorie.id"
    if 'categorie.sens.name' not in session:
        session['categorie.sens.name'] = "ASC"
    if 'categorie.page' not in session:
        session['categorie.page'] = 1

    tab = cat.search(
           session['categorie.search'],
           session['categorie.col.name'],
           session['categorie.sens.name'],
           nb_result_max*(session['categorie.page']-1),
           nb_result_max
    )

    nb_resultat = cat.search_size(session['categorie.search'])

    return render_template("categorie_index.tpl",
                           resultat=tab,
                           nb_resultat=nb_resultat[0],
                           nb_resultat_actu=len(tab),
                           search=session['categorie.search'],
                           col=session['categorie.col.id'],
                           sens=session['categorie.sens.id'],
                           erreur=erreur,
                           info=info,
                           page=session['categorie.page'],
                           nb_max_result=nb_result_max)


@categorie.route('/add', methods=['GET', 'POST'])
def add():
    """Fonction qui analyse le form add cat."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    photo_d = io.BytesIO()
    if request.method == 'POST':
        try:
            titre = request.form['titre']
            hauteur = request.form['hauteur']
            largeur = request.form['largeur']
            profondeur = request.form['profondeur']
            photo = request.files['photo']
            photo.save(photo_d)
            cat.set(
                titre, hauteur, largeur, profondeur, photo_d.getvalue())
        except sqlite3.IntegrityError as e:
            session['erreur'] = "Le nom de la catégorie, que vous essayez d'ajouter, \
                     existe déjà ("+str(e)+")"
        except sqlite3.OperationalError as e:
            session['erreur'] = str(e)
        except sqlite3.Error as e:
            session['erreur'] = str(e)
        except:
            session['erreur'] = "Erreur inconnue"
    print("FERMETURE BDD")
    return redirect(url_for("categorie.index"))


@categorie.route('/edit', methods=['GET', 'POST'])
def edit():
    """Fonction qui analyse le form add cat."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    if request.method == 'POST':
        try:
            id_cat = request.form['id']
            titre = request.form['titre']
            hauteur = request.form['hauteur']
            largeur = request.form['largeur']
            profondeur = request.form['profondeur']
            if 'photo' in request.files:
                photo_d = io.BytesIO()
                photo = request.files['photo']
                photo.save(photo_d)
                cat.update(
                    titre,
                    hauteur,
                    largeur,
                    profondeur,
                    photo_d.getvalue(),
                    id_cat
                )
            else:
                cat.update_without_files(
                    titre,
                    hauteur,
                    largeur,
                    profondeur,
                    id_cat
                )

        except sqlite3.IntegrityError as e:
            session['erreur'] = "Le nom de la catégorie, que vous essayez de \
                                modifier, existe déjà ("+str(e)+")"
        except sqlite3.OperationalError as e:
            session['erreur'] = str(e)
        except sqlite3.Error as e:
            session['erreur'] = str(e)
        except:
            session['erreur'] = "Erreur inconnue"

    return redirect(url_for("categorie.index"))


@categorie.route('/trie/<int:col>/<int:sens>')
def trie(col, sens):
    """Fonction qui indique le trie à faire."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    col_name = ["categorie.id", "categorie.nom"]
    sens_name = ["ASC", "DESC"]

    if col < 0 or col > len(col_name):
        col = 0
    if sens < 0 or sens > len(sens_name):
        sens = 0

    session['categorie.col.id'] = col
    session['categorie.sens.id'] = not(sens)
    session['categorie.col.name'] = col_name[col]
    session['categorie.sens.name'] = sens_name[sens]
    return redirect(url_for("categorie.index"))


@categorie.route('/search', methods=['GET', 'POST'])
def search():
    """Fonction qui analyse le form de search."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))
    if request.method == 'POST':
        try:
            session['categorie.search'] = request.form['search']
            session['categorie.page'] = 1
        except:
            session['erreur'] = 'Mauvais formatage de la recherche'
    return redirect(url_for("categorie.index"))


@categorie.route('/page/<int:num_page>/')
def page(num_page):
    """Fonction qui indique le trie à faire."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    if num_page < 1:
        num_page = 1

    session['categorie.page'] = num_page
    return redirect(url_for("categorie.index"))
