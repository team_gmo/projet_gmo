# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""fichier regroupant les fonctions liée aux page d'admin."""

from flask import Blueprint, render_template, redirect
from flask import session, url_for
from GMO import product, user

statistique = Blueprint('statistique',
                        __name__,
                        template_folder='../../templates/dashboard',
                        static_folder='../../static')


@statistique.route('/')
def index():
    """Affiche la page de statistiques."""
    if 'username' not in session:
        return redirect(url_for('.connexion'))

    erreur, info = "", ""

    if 'erreur' in session:
        erreur = session['erreur']
        session.pop('erreur', None)
    if 'info' in session:
        info = session['info']
        session.pop('info', None)

    nb_user = user.get_quantity()
    nb_prod = product.get_quantity()
    nb_emprunt = product.get_quantity_state(1)

    return render_template("statistique_index.tpl",
                           nb_user=nb_user[0],
                           nb_prod=nb_prod[0],
                           nb_emprunt=nb_emprunt,
                           erreur=erreur,
                           info=info)
