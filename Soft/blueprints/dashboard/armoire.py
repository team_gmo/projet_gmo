# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Fichier regroupant les fonctions liée aux page d'admin."""

from flask import Blueprint, render_template, redirect, session
from flask import url_for
from GMO import armoire as a, casier, config

armoire = Blueprint('armoire',
                    __name__,
                    template_folder='../../templates/dashboard',
                    static_folder='../../static')


@armoire.route('/')
def index():
    """Affiche la page armoire."""
    if 'username' not in session:
        return redirect(url_for('main.connexion'))

    erreur, info = "", ""

    if 'erreur' in session:
        erreur = session['erreur']
        session.pop('erreur', None)
    if 'info' in session:
        info = session['info']
        session.pop('info', None)

    tab = a.get_all()

    nb_floors = []
    nb_cols = []

    for id_armoire in range(0, len(tab)):
        # récupération du nombre de ligne de l'armoire
        nb_floors.append(casier.get_number_of_floor(id_armoire + 1)[0])

        # set de nb_cols en tableau
        nb_cols.append([])
        # récupération du nombre de casier/colonne pour chaque ligne de l'armoire
        for ligne in range(nb_floors[id_armoire]):
            nb_cols[id_armoire] += casier.get_number_of_col(ligne + 1, id_armoire + 1)

    # récupération de l'étage de la pi pour son insertion dans le schéma
    floor_pi = config.readParam("floor_pi." + str(id_armoire))

    print(nb_floors)
    print(nb_cols)

    return render_template("armoire_index.tpl",
                           resultat=tab,
                           erreur=erreur,
                           info=info,
                           floor_pi=floor_pi,
                           nb_floors=nb_floors,
                           nb_cols=nb_cols,
                           get_data_casier=casier.get_by_id_armoire)
