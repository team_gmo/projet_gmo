# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Fichier Python gérant la page index.html.

Génère la page index.
"""

from math import ceil
from flask import Blueprint, render_template, session
from GMO import categorie

index = Blueprint('index',
                  __name__,
                  template_folder='../../templates/ui',
                  static_folder='../../static')


@index.route('/<int:id_armoire>/', defaults={'error': ""})
@index.route('/<int:id_armoire>/<error>')
def index_page(id_armoire, error):
    """
    Fonction générant la page index du système.


    **Cette fonction récupère:**
        * all_categories: liste des catégories disponible

    **Cette fonction calcul:**
        * nb_grid: nombre de "page" de produit à présenter
        * nb_rows_last_grid: nombre de ligne de produit à présenter dans la
            dernière grid de produit
        * nb_cols_last_grid: nombre de colonne de produit à présenter dans la
            dernière grid de produit

    **Et transmet au template:**
        * id_armoire: identifiant de l'armoire
        * nb_grid: nombre de "page" de produit
        * nb_rows: nombre de ligne par grid de produit
        * nb_cols: nombre de colonne par grid de produit
        * nb_cols_last_grid: nombre de colonne de la dernière grid
        * nb_rows_last_grid: nombre de ligne de la dernière grid
        * cat5: booleen sur le nombre de produit à présenter sur la dernière grid
        * categories_last_grid: liste des categories de la derniere grid
        * all_categories: liste des catégories
        * cat_matrix: matrix de positionnement des catégories
        * cat_matrix_last_grid: matrix de positionnement des catégories de la dernière grid
        * notif_msg: message transmis pour affichage dans l'alerte

    :Exemple:

    entrer l'url : 127.0.0.1:5000/1
    ou 127.0.0.1:5000/1/
    ou 127.0.0.1:5000/1/Ceci est un test de page avec message

    :param id_armoire: identifiant de l'armoire récupéré dans l'url
    :type id_armoire: int
    :param error: paramètre optionel, message d'erreur à afficher via l'alerte
    :type error: chaine de caractère

    """
    # recuperation de toutes les categories
    all_categories = categorie.get_available(id_armoire)

    # Calcul nombre de grid necessaire sur base de 6 categories par grid
    nb_grid = ceil(len(all_categories) / 6)

    # declaration nb_cols et nb_rows (toujours egaux a 3 et 2)
    nb_cols = 3
    nb_rows = 2

    # Recuperation des categories de la derniere grid, peut importe leur nombre
    cat_last_grid = categorie.get_available_by_six((nb_grid - 1) * 6, id_armoire)

    # Calcul nombre de rows necessaire sur base de 3 categories par rows
    nb_rows_last_grid = ceil(len(cat_last_grid) / 3)

    # Calcul nombre de cols necessaire pour le last grid sur base de 3 par rows
    # cat5 booleen pour cas de 5 categories à afficher, 3 sur la premiere
    # ligne, 2 sur la deuxieme
    cat5 = 0
    if len(cat_last_grid) % 6 == 1:
        nb_cols_last_grid = 1
    elif len(cat_last_grid) % 6 == 2 or len(cat_last_grid) % 6 == 4:
        nb_cols_last_grid = 2
    elif len(cat_last_grid) % 6 == 5:
        nb_cols_last_grid = 3
        cat5 = 1
    else:
        nb_cols_last_grid = 3

    # Declaration de la matrix de placement des catégories
    cat_matrix = [[0, 1, 2], [3, 4, 5]]
    if nb_cols_last_grid == 2:
        cat_matrix_last_grid = [[0, 1], [2, 3]]
    else:
        cat_matrix_last_grid = cat_matrix

    notif_msg = error
    # Vérification si une panne a été déclarée
    if 'notif_msg' in session:
        notif_msg = session['notif_msg']
        session.pop('notif_msg', None)

    return render_template('index.tpl',
                           id_armoire=id_armoire,
                           nb_grid=nb_grid,
                           nb_rows=nb_rows,
                           nb_cols=nb_cols,
                           nb_cols_last_grid=nb_cols_last_grid,
                           nb_rows_last_grid=nb_rows_last_grid,
                           cat5=cat5,
                           categories_last_grid=cat_last_grid,
                           all_categories=all_categories,
                           cat_matrix=cat_matrix,
                           cat_matrix_last_grid=cat_matrix_last_grid,
                           notif_msg=notif_msg,
                           )
