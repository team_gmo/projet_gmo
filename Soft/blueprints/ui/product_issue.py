# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Fichier Python gérant la page product_issue.html.

Génère la page product_issue.
"""
from flask import Blueprint, render_template

product_issue = Blueprint('product_issue',
                          __name__,
                          template_folder='../../templates/ui',
                          static_folder='../../static')

notif_msg = ""


@product_issue.route('/<int:id_arm>/product_issue/<id_prd>/')
def index(id_arm, id_prd):
    """
    Fonction générant la page index du système.

    **Et transmet au template:**
        * id_armoire: identifiant de l'armoire
        * notif_msg: message transmis pour affichage dans l'alerte
        * id_produit: identifiant du produit

    :Exemple:

    entrer l'url : 127.0.0.1:5000/1/product_issue/2

    :param id_arm: identifiant de l'armoire récupéré dans l'url
    :type id_arm: int
    :param id_prd: identifiant du produit récupéré dans l'url
    :type id_prd: int

    """

    return render_template('product_issue.tpl',
                           id_armoire=id_arm,
                           notif_msg=notif_msg,
                           id_produit=id_prd)
