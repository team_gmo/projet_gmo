# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Fichier Python gérant la page admin_menu.tpl.

Génère la page admin_menu.
"""
from GMO import product, casier, config, big_brother
from flask import url_for, redirect, render_template, Blueprint

admin_menu = Blueprint('admin_menu',
                       __name__,
                       template_folder='../../templates/ui',
                       static_folder='../../static')


# admin_menu.admin_menu Fonction principale
@admin_menu.route('/<int:id_armoire>/admin_menu')
def index(id_armoire):
    """
    Fonction générant la page admin_menu.

    **Cette fonction récupère:**
        * nb_locked_locker: nombre de casier condamnés dans cette armoire
        * issue_to_solve: nombre d'erreur répertoriée par le système
        * not_available: nombre de produit considéré comme indisponible

    **Et transmet au template:**
        * id_armoire: identifiant de l'armoire
        * nb_locked_locker: nombre de casier condamnés dans cette armoire
        * issue_to_solve: nombre d'erreur répertoriée par le système
        * not_available: nombre de produit considéré comme indisponible
        * notif_msg: message transmis pour affichage dans l'alerte

    :Exemple:

    entrer l'url : 127.0.0.1:5000/1/admin_menu

    :param id_armoire: identifiant de l'armoire récupéré dans l'url
    :type id_armoire: int
    :return: le rendu de la page /admin_menu

    """
    nb_locked_locker = len(casier.get_locked(id_armoire))
    # récupération du nombre d'erreur soulevées (code 5)
    issue_to_solve = product.get_quantity_state(5)
    # récupération du nombre de produits indiqués comme indisponible (code 0)
    not_available = product.get_quantity_state(0)

    return render_template('admin_menu.tpl',
                           id_armoire=id_armoire,
                           nb_locked_locker=nb_locked_locker,
                           issue_to_solve=issue_to_solve,
                           not_available=not_available,
                           notif_msg=""
                           )


# admin_menu.lock_casier Fonction de lockdown d'un casier
@admin_menu.route('/<int:id_armoire>/admin_menu/lock_casier/',
                  defaults={'l': None, 'c': None})
@admin_menu.route('/<int:id_armoire>/admin_menu/lock_casier/<int:l>/<int:c>')
def lock_casier(id_armoire, l, c):
    """
    Fonction générant la page admin_menu/lock_casier.

    **Cette fonction récupère:**
        * nb_floors: le nombre de ligne dont est composée l'armoire
        * nb_cols: le nombre de casier (colonne) de chaque ligne
        * floor_pi: la ligne au-dessus de laquelle se trouve l'interface user
        * locked_casier: la liste des casiers considéré condamnés

    **Et transmet au template:**
        * id_armoire: identifiant de l'armoire
        * nb_floors: le nombre de ligne dont est composée l'armoire
        * nb_cols: le nombre de casier (colonne) de chaque ligne
        * floor_pi: la ligne au-dessus de laquelle se trouve l'interface user
        * locked_casier: la liste des casiers considéré condamnés
        * chr: fonction python chr, convertissant les int en ascii
        * notif_msg: message transmis pour affichage dans l'alerte

    :Exemple:

    entrer l'url : 127.0.0.1:5000/1/admin_menu/lock_casier/
    ou 127.0.0.1:5000/1/admin_menu/lock_casier/2/3

    :param id_armoire: identifiant de l'armoire récupéré dans l'url
    :type id_armoire: int
    :param l: paramètre optionel, numéro de ligne du casier désigné
    :type l: int
    :param c: paramètre optionel, numéro de colonne du casier désigné
    :type c: int
    :return: le rendu de la page /admin_menu

    """
    # récupération du nombre de ligne de l'armoire
    nb_floors = casier.get_number_of_floor(id_armoire)[0]

    # set de nb_cols en tableau
    nb_cols = []
    # récupération du nombre de casier/colonne pour chaque ligne de l'armoire
    for ligne in range(nb_floors):
        nb_cols += casier.get_number_of_col(ligne + 1, id_armoire)

    # récupération de l'étage de la pi pour son insertion dans le schéma
    floor_pi = config.readParam("floor_pi." + str(id_armoire))

    # récupération des casier signalés condamnés
    locked_casier = casier.get_locked(id_armoire)
    if (l is not None) and (c is not None):
        # Si la liste des casiers est None alors c'est nécessairement un lock
        if locked_casier == []:
            casier.set_lock(1, id_armoire, l, c)
        else:
            # Si  l ET c not None => un casier est lock/unlock
            # On vérifie si c'est un lock ou un unlock et on agit en base.
            for tuples in locked_casier:
                # Si le casier est lock, on unlock
                if (l, c) == tuples:
                    casier.set_lock(0, id_armoire, l, c)
                    break
                # Sinon, on lock
                else:
                    casier.set_lock(1, id_armoire, l, c)

    # On reprend la liste des casiers pour la génération avec le changement
    locked_casier = casier.get_locked(id_armoire)
    return render_template('lock_casier.tpl',
                           id_armoire=id_armoire,
                           nb_floors=nb_floors,
                           nb_cols=nb_cols,
                           floor_pi=floor_pi,
                           locked_casier=locked_casier,
                           chr=chr,
                           notif_msg=""
                           )


# admin_menu.solve_issue Fonction de résolution d'un problème signalé
@admin_menu.route('/<int:id_armoire>/admin_menu/solve_issue/')
def solve_issue(id_armoire):
    """
    Fonction générant la page admin_menu/solve_issue

    **Cette fonction récupère:**
        * nb_floors: le nombre de ligne dont est composée l'armoire
        * nb_cols: le nombre de casier (colonne) de chaque ligne
        * floor_pi: la ligne au-dessus de laquelle se trouve l'interface user
        * issue_to_solve: nombre d'erreur répertoriée par le système
        * not_available: nombre de produit considéré comme indisponible
        * signaled_issues: liste des casier signalés en erreur
        * issues_com: liste des commentaires pour chaque erreur signalée

    **Et transmet au template:**
        * id_armoire: identifiant de l'armoire
        * floor_pi: la ligne au-dessus de laquelle se trouve l'interface user
        * nb_floors: le nombre de ligne dont est composée l'armoire
        * nb_cols: le nombre de casier (colonne) de chaque ligne
        * issue_to_solve: nombre d'erreur répertoriée par le système
        * not_available: nombre de produit considéré comme indisponible
        * signaled_issues: liste des casier signalés en erreur
        * issues_com: liste des commentaires pour chaque erreur signalée
        * chr: fonction python chr, convertissant les int en ascii
        * notif_msg: message transmis pour affichage dans l'alerte

    :Exemple:

    entrer l'url : 127.0.0.1:5000/1/admin_menu/solve_issue/

    :param id_armoire: identifiant de l'armoire récupéré dans l'url
    :type id_armoire: int
    :return: le rendu de la page /admin_menu

    """
    # récupération du nombre de ligne de l'armoire
    nb_floors = casier.get_number_of_floor(id_armoire)[0]
    # set de nb_cols en tableau
    nb_cols = []
    # récupération du nombre de casier/colonne pour chaque ligne de l'armoire
    for ligne in range(nb_floors):
        nb_cols += casier.get_number_of_col(ligne + 1, id_armoire)
    # récupération de l'étage de la pi pour son insertion dans le schéma
    floor_pi = config.readParam("floor_pi." + str(id_armoire))

    # récupération du nombre d'erreur soulevées (code 5)
    issue_to_solve = product.get_quantity_state(5)
    # récupération du nombre de produits indiqués comme indisponible (code 0)
    not_available = product.get_quantity_state(0)

    # Recuperation des casiers signalés en erreurs
    signaled_issues = product.get_signaled_issues(id_armoire)

    # Recuperation de l'erreur concernée

    issues_com = []
    print("SIGNALED ISSUE=", signaled_issues)
    if signaled_issues is not None:
        for tuples in signaled_issues:
            issues_com += product.get_com(id_armoire, tuples)

    return render_template('solve_issue.tpl',
                           id_armoire=id_armoire,
                           floor_pi=floor_pi,
                           nb_cols=nb_cols,
                           nb_floors=nb_floors,
                           issue_to_solve=issue_to_solve,
                           not_available=not_available,
                           signaled_issues=signaled_issues,
                           issues_com=issues_com,
                           chr=chr,
                           notif_msg=""
                           )


# admin_menu.solve Fonction de mise en base de la sorti d'un produit
@admin_menu.route('/<int:id_armoire>/admin_menu/solve/<int:l>/<int:c>')
def solve(id_armoire, l, c):
    """
    Fonction inscrivant en base la sortie d'un produit par l'admin
    lors d'une résolution de problème / mise en réparation.

    **Cette fonction récupère:**
        * locker: le casier sélectionné par l'administrateur
        * id_produit: l'idée du produit concerné par l'erreur

    **Et transmet au template:**
        * id_armoire: identifiant de l'armoire

    :Exemple:

    entrer l'url: 127.0.0.1:5000/1/admin_menu/solve/2/3

    :param id_armoire: identifiant de l'armoire récupéré dans l'url
    :type id_armoire: int
    :param l: numéro de ligne du casier désigné
    :type l: int
    :param c: numéro de colonne du casier désigné
    :type c: int
    :return: redirect vers la page admin_menu/solve_issue

    """
    # récupération de l'id du casier sélectionné
    locker = casier.get_id_by_l_c(id_armoire, l, c)[0]
    # récupération de l'id du produit contenu dans le casier
    id_produit = casier.get_by_id_armoire(id_armoire, l, c)[0]

    product.unlink_from_casier(id_produit)
    # Ouverture du casier
    casier.open(locker)
    # Inscription en base dans Big_Brother du retrait du produit
    big_brother.set(0, id_produit, 0, 6, "", None)

    return redirect(url_for('admin_menu.solve_issue',
                            id_armoire=id_armoire))
