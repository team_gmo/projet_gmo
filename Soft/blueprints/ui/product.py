# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Fichier Python gérant la page get_product.html.

Génère la page get et give_back.
"""

from GMO import (
    casier,
    config
)

from flask import (
    render_template,
    Blueprint,
    session
)

product = Blueprint('product',
                    __name__,
                    template_folder='../../templates/ui',
                    static_folder='../../static')


# Product.get Fonction #
@product.route('/<int:id_armoire>/get/<int:id_cat>')
def get(id_armoire, id_cat):
    """
    Fonction générant la page get_product.

    **Cette fonction récupère:**
    * nb_floors: le nombre de ligne dont est composée l'armoire
    * nb_cols: le nombre de casiers (colonne) de chaque ligne
    * floor_pi: la ligne au-dessus de laquelle se trouve l'interface user

    **Et transmet au template:**
    * id_armoire: identifiant de l'armoire
    * nb_floors: le nombre de ligne dont est composée l'armoire
    * nb_cols: le nombre de casier (colonne) de chaque ligne
    * floor_pi: la ligne au-dessus de laquelle se trouve l'interface user
    * notif_msg: message transmis pour affichage dans l'alerte
    * id_cat: identifiant de la catégorie souhaitée
    * chr: fonction python chr, convertissant les int en ascii

    :Exemple:

    entrer l'url : 127.0.0.1:5000/1/get/1

    :param id_armoire: identifiant de l'armoire récupéré dans l'url
    :type id_armoire: int
    :param id_cat: identifiant de la catégorie souhaitée récupéré dans l'url
    :type id_cat: int

    """
    # récupération du nombre de ligne de l'armoire
    nb_floors = casier.get_number_of_floor(id_armoire)[0]

    # set de nb_cols en tableau
    nb_cols = []
    # récupération du nombre de casier/colonne pour chaque ligne de l'armoire
    for ligne in range(nb_floors):
        nb_cols += casier.get_number_of_col(ligne + 1, id_armoire)

    # récupération de l'étage de la pi pour son insertion dans le schéma
    floor_pi = config.readParam("floor_pi." + str(id_armoire))

    notif_msg = ""
    if 'notif_msg' in session:
        notif_msg = session['notif_msg']
        session.pop('notif_msg', None)

    return render_template('get_product.tpl',
                           id_armoire=id_armoire,
                           nb_floors=nb_floors,
                           nb_cols=nb_cols,
                           floor_pi=floor_pi,
                           notif_msg=notif_msg,
                           id_cat=id_cat,
                           chr=chr)


# Product.give_back Fonction #
@product.route('/<int:id_armoire>/give_back/<id_produit>')
def give_back(id_armoire, id_produit):
    """
    Fonction générant la page give_back.


    Cette fonction récupère :
        * nb_floors: le nombre de ligne dont est composée l'armoire
        * nb_cols: le nombre de casier (colonne) de chaque ligne
        * floor_pi: la ligne au-dessus de laquelle se trouve l'interface user

    Et transmet au template:
        * id_armoire: identifiant de l'armoire
        * nb_floors: le nombre de ligne dont est composée l'armoire
        * nb_cols: le nombre de casier (colonne) de chaque ligne
        * floor_pi: la ligne au-dessus de laquelle se trouve l'interface user
        * notif_msg: message transmis pour affichage dans l'alerte
        * id_produit: identifiant du produit souhaité récupéré dans l'url
        * chr: fonction python chr, convertissant les int en ascii

    :Exemple:

    entrer l'url : 127.0.0.1:5000/1/give_back/2


    :param id_armoire: identifiant de l'armoire récupérée dans l'url
    :type id_armoire: int
    :param id_produit: identifiant du produit souhaité récupéré dans l'url
    :type id_produit: int

    :Exemple:

    Entrer l'url : `http://127.0.0.1:5000/1/give_back/2`
    """
    # récupération du nombre de ligne de l'armoire
    nb_floors = casier.get_number_of_floor(id_armoire)[0]

    # set de nb_cols en tableau
    nb_cols = []
    # récupération du nombre de casier/colonne pour chaque ligne de l'armoire
    for ligne in range(nb_floors):
        nb_cols += casier.get_number_of_col(ligne+1, id_armoire)

    # récupération de l'étage de la pi pour son insertion dans le schéma
    floor_pi = config.readParam("floor_pi." + str(id_armoire))

    notif_msg = ""
    if 'notif_msg' in session:
        notif_msg = session['notif_msg']
        session.pop('notif_msg', None)

    return render_template('give_back_product.tpl',
                           id_armoire=id_armoire,
                           nb_floors=nb_floors,
                           nb_cols=nb_cols,
                           floor_pi=floor_pi,
                           notif_msg=notif_msg,
                           id_produit=id_produit,
                           chr=chr)
