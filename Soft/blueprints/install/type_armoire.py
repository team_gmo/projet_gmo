# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""fichier regroupant les fonctions liée aux page d'admin."""

from flask import Blueprint, render_template, session, request

type_armoire = Blueprint(
    'type_armoire',
    __name__,
    template_folder='../../templates/install',
    static_folder='../../static'
)


@type_armoire.route('/type')
def pc():
    """Affiche la page choisissant si une armoire est begnaud."""
    erreur, info = "", ""

    if 'erreur' in session:
        erreur = session['erreur']
        session.pop('erreur', None)
    if 'info' in session:
        info = session['info']
        session.pop('info', None)

    return render_template("type_armoire_pc.tpl",
                           erreur=erreur,
                           info=info)


@type_armoire.route('/pi/type')
def pi():
    """Affiche la page choisissant si une armoire est begnaud."""
    erreur, info = "", ""

    if 'erreur' in session:
        erreur = session['erreur']
        session.pop('erreur', None)
    if 'info' in session:
        info = session['info']
        session.pop('info', None)

    return render_template("type_armoire_pi.tpl",
                           erreur=erreur,
                           info=info)


@type_armoire.route('/type/save', methods=["POST"])
def save():
    """Enregistre ce qu'a écrit l'installateur."""
    if request.method == "POST":
        return "PLOP"
