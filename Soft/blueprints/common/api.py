# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Fichier regroupant les différentes fonctions utilisé par l'API."""

import json
from GMO import (
    product as GMO_product,
    user as GMO_user,
    casier as GMO_casier,
    armoire as GMO_armoire,
    big_brother as GMO_big_brother,
    security
)

from flask import Blueprint, request, session


api = Blueprint('api', __name__)


@api.route('/user/<int:id_armoire>/<function_name>/', methods=['POST'])
def user(id_armoire, function_name):
    """Accès aux fonctions de user à distance."""
    signed_jwt = request.form['jwt']
    json_args = security.jwt2json(id_armoire, signed_jwt)
    args = json.loads(json_args)

    value = getattr(GMO_user, function_name)(*args)

    json_args = json.dumps(value)
    signed_jwt = security.json2jwt(json_args)

    return signed_jwt


@api.route('/product/<int:id_armoire>/<function_name>/', methods=['POST'])
def product(id_armoire, function_name):
    """Accès aux fonctions de product à distance."""
    signed_jwt = request.form['jwt']
    json_args = security.jwt2json(id_armoire, signed_jwt)
    args = json.loads(json_args)

    value = getattr(GMO_product, function_name)(*args)

    json_args = json.dumps(value)
    signed_jwt = security.json2jwt(json_args)

    return signed_jwt


@api.route('/casier/<int:id_armoire>/<function_name>/', methods=['POST'])
def casier(id_armoire, function_name):
    """Accès aux fonctions de casier à distance."""
    signed_jwt = request.form['jwt']
    json_args = security.jwt2json(id_armoire, signed_jwt)
    args = json.loads(json_args)

    value = getattr(GMO_casier, function_name)(*args)

    json_args = json.dumps(value)
    signed_jwt = security.json2jwt(json_args)

    return signed_jwt


@api.route('/big_brother/<int:id_armoire>/<function_name>/', methods=['POST'])
def big_brother(id_armoire, function_name):
    """Accès aux fonctions de casier à distance."""
    signed_jwt = request.form['jwt']
    json_args = security.jwt2json(id_armoire, signed_jwt)
    args = json.loads(json_args)

    value = getattr(GMO_big_brother, function_name)(*args)

    json_args = json.dumps(value)
    signed_jwt = security.json2jwt(json_args)

    return signed_jwt


@api.route('/register/', methods=['POST'])
def register():
    """Accès aux fonctions de casier à distance."""
    print("I HAVE A NEW FRIEND :)")
    if request.method == 'POST':
        json_args = request.form['json_args']
        args = json.loads(json_args)
        public_key = args['public_key']
        description = args['description']

        armoire_id = GMO_armoire.set(public_key, description)
        master_public_key = GMO_armoire.get_public_key(1)
        value = {
            "id_armoire": armoire_id,
            "public_key": master_public_key[0]
        }

        return json.dumps(value)


@api.route('/search_user/<search>')
def search_user(search):
    """Return an api de recherche d'utilisateur."""
    if 'username' not in session:
        return json.dumps({'error': "Not logged"})
    if not search:
        search = "%"
    result = GMO_user.search(search, "user.rowid", "ASC", 0, 5, 0)
    print(result)
    print(search)
    return json.dumps(result)
