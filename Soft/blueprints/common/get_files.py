# GiveMeOne - Une armoire connectée
# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
#                    Corentin Vretman, Maxence Dehaut
#
# Developped in French at ESEO, engineering school (IoT Major)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Ensemble de fonction récupérant des fichiers en base."""

from GMO import categorie, user
from flask import Blueprint, send_file, redirect
import io

get_files = Blueprint('get_files', __name__)


@get_files.route('/files/cat/<int:id_photo>.png')
def image_cat(id_photo):
    """Choppe les photos de catégorie dans la bdd."""
    try:
        cat = categorie.get_picture(id_photo)
    except:
        return redirect("/static/img/produit.png")
    if cat[0] == b'':
        return redirect("/static/img/produit.png")
    if cat is None:
        return redirect("/static/img/produit.png")
    if cat[0] is None:
        return redirect("/static/img/produit.png")
    return send_file(io.BytesIO(cat[0]), mimetype='image/png')


@get_files.route('/files/carte/<int:id_user>.png')
def carte_etu(id_user):
    """Choppe les cartes etudiantes dans la bdd."""
    try:
        user_data = user.get_pictures(id_user)
    except:
        return redirect("/static/img/carte.png")
    if user_data[0] == b'':
        return redirect("/static/img/carte.png")
    if user_data is None:
        return redirect("/static/img/carte.png")
    if user_data[0] is None:
        return redirect("/static/img/carte.png")
    return send_file(io.BytesIO(user_data[0]), mimetype='image/png')


@get_files.route('/files/visage/<int:id_user>.png')
def visage(id_user):
    """Choppe les visages dans la bdd."""
    try:
        user_data = user.get_pictures(id_user)
    except:
        return redirect("/static/img/anonyme.png")
    if user_data[1] == b'':
        return redirect("/static/img/anonyme.png")
    if user_data is None:
        return redirect("/static/img/anonyme.png")
    if user_data[1] is None:
        return redirect("/static/img/anonyme.png")
    return send_file(io.BytesIO(user_data[1]), mimetype='image/png')
