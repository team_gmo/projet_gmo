{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "base.tpl" %}

{% block page_title %}DECLARER UN PROBLEME{% endblock %}

{% block notif %}{% endblock notif %}

{% block main %}
<!-- CONTENT -->
<div class="row mx-auto mt-5">
    <div class="d-block mx-auto my-auto spaced">
        <div class="card" onclick="declarePrbl(this, 'Le produit est manquant.');">
            <div class="card-block">
                <img class="card-img-top" src="/static/img/missing_product_card_1.jpg" alt="">
                <h4 class="card-title">"Le produit est manquant."</h4>
            </div>
        </div>
    </div>
    <div class="d-block mx-auto my-auto spaced">
        <div class="card" onclick="declarePrbl(this, 'Le produit est abîmé.');">
            <div class="card-block">
                <img class="card-img-top" src="/static/img/missing_product_card_2.jpg" alt="">
                <h4 class="card-title">"Le produit est abîmé."</h4>
            </div>
        </div>
    </div>
    <div class="d-block mx-auto my-auto spaced">
        <div class="card" onclick="declarePrbl(this, 'Le casier est vide.');">
            <div class="card-block">
                <img class="card-img-top" src="/static/img/missing_product_card_3.jpg" alt="">
                <h4 class="card-title">"Le casier est vide."</h4>
            </div>
        </div>
    </div>
</div>
<!-- Message to the user -->
<div class="row text-center mt-4">
    <div class="col">
        Après avoir indiqué votre problème ci-dessus,<br /><br />
        merci de <strong>déposer votre produit dans le casier ouvert</strong>, et de <strong>bien refermer celui-ci</strong> pour terminer le rendu.<br />
    </div>
</div>
<!-- END OF CONTENT -->
{% endblock main %}

{% block script %}
<script>
    var element_choosen = 0;
    var door_closed = 0;

    function declarePrbl(element, description) {
        element_choosen=description;
        $(".card").removeClass("selected");
        $(element).addClass("selected");
        try2send();
    }

    function try2send() {
        if(element_choosen && door_closed)
        {
            socket.emit('product_issue', { 'id_produit': "{{ id_produit }}", "panne":  element_choosen});
        }
    }

    socket.on('casier_closed', function() {
        door_closed=1;
        try2send();
    })

    socket.on('error', function(error) {
        location.href='/{{ id_armoire }}/' + error.txt;
    });

    socket.on('prbl_saved', function() {
        location.href='/{{ id_armoire }}';
    });
</script>
{% endblock script %}
