{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "base.tpl" %}

{% block page_title %}LOCKDOWN CASIER{% endblock page_title %}

{% block adminAccess %}<i class="fa fa-lock fa-3x ml-2 mt-2" id="admin_button" aria-hidden="true" onclick="location.href='{{ url_for('index.index_page', id_armoire=id_armoire) }}'"></i>{% endblock adminAccess %}

{% block main %}
<div class="row" style="height: 1em"></div>
<div class="row" style="height: 1em"></div>
<div class="row" style="height: 1em"></div>
<div class="col"><a role="button" class="btn btn-primary" href="{{url_for('admin_menu.index', id_armoire=id_armoire )}}" style="font-size: 1.2em">Retour</a></div>
<div class="container" style="padding-top:5px">
  <div class="row">
    <div id="subcontainer_left" class="col align-self-center" style="margin:0px; margin-left:-10px;">

      {# Si le floor_pi est au dessus de l'armoire #}
      {% if floor_pi >= nb_floors %}
        <div class="row ligne" id="floor_pi">
          <div class="col casier logo bgcenter"></div>
          <div class="col casier"></div>
        </div>
      {% endif %}

      {# la boucle est inversée par soucis d'affichage des lignes #}
      {# de bas en haut pour un rendu correcte du réel : #}
      {# l'armoire est décrite par étage, donc la ligne 1 se trouve en bas #}
      {% for i in range(nb_floors-1,-1,-1) %}

      <div id="floor{{i+1}}" class="row ligne" style="height:{{300/nb_floors}}px;">

        {% for j in range(nb_cols[i]) %}
            <a id="fl{{chr(i+65)}}_{{j+1}}" class="col casier
            {# On compare la liste de tuples des casiers verrouillés aux incréments #}
            {# et assignons la classe locked le cas échéants #}
            {% for tuples in locked_casier %}
              {% if tuples == (i+1, j+1) %}
                locked
              {% endif %}
            {% endfor %}" role="button" href="{{url_for('admin_menu.lock_casier', id_armoire=id_armoire, l=i+1, c=j+1)}}" style="font-height:8px">
            <!-- chr(64+xxx) permet de convertir en Ascii et donc d'utiliser des lettres pour nommer les casiers -->
            <div></div>{{chr(64+i+1)}}{{j+1}}
          </a>
        {% endfor %}

      </div>

        {% if i == floor_pi %}
        <div class="row ligne" id="floor_pi">
          <div class="col casier logo bgcenter"></div>
          <div class="col casier"></div>
        </div>
        {% endif %}

      {% endfor %}
    </div>
    <div id="subcontainer_right" class="col align-self-center">
        <h5 class="text-center" style="color:white">
            <br />Sélectionnez le casier que vous souhaitez verrouiller ou bien déverrouiller<br /><br />
        </h5>
    </div>
  </div>
</div>
{% endblock main %}
