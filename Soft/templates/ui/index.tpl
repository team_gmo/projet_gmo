{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "base.tpl" %}

{% block page_title %}CHOISISSEZ VOTRE PRODUIT{% endblock %}


{% block main %}

<div id="gros_sac"></div>

<div id="enregistrement"></div>

<div id="unknown_card"></div>

<div class="row align-items-center mt-4" id="menu">
    <div class="col-1 div-btn" id="div-btn-prev">
        <div class="row align-items-center div-btn">
            <div class="col">
                <i class="fa fa-chevron-left fa-3x" id="btn-prev" aria-hidden="true"></i>
            </div>
        </div>
    </div>
    <div class="col-10">
        <!-- Grids -->
        {% for k in range(nb_grid) %}
        <div id="grid{{k+1}}" class="grid">

            {% set this_grid_categories = [] %}

            {% if k == nb_grid-1 %}
            {% set this_grid_categories = categories_last_grid %}
            {% set nb_rows = nb_rows_last_grid %}
            {% set nb_cols = nb_cols_last_grid %}
            {% set cat_matrix = cat_matrix_last_grid %}
            {% else %}
            {% for c in range(6) %}
            {% set this_grid_categories = this_grid_categories.append(all_categories[(k*6)+c]) %}
            {% endfor %}
            {% endif %}

            {% for i in range(nb_rows) %}

            <div class="row mb-2">

                {% if cat5 and i == 1 and k == nb_grid-1 %}

                {% for j in range(2) %}

                {% set categorie = this_grid_categories[cat_matrix[i][j]] %}
                <div class="col">
                    <img id="categorie{{categorie[1]}}" class="d-block mx-auto img-thumbnail img-fluid" src=
                        {% if categorie[2] == 0 %}
                            "http://localhost:5000/static/img/sold-out.png"
                        {% else %}
                            {{ url_for('get_files.image_cat', id_photo=categorie[1]) }}
                        {% endif %}
                        style="background-image:
                        {% if categorie[2] == 0 %}
                             url({{ url_for('get_files.image_cat', id_photo=categorie[1]) }})
                        {% endif %}
                        ;"/>
                </div>

                {% endfor %}

                {% else %}

                {% for j in range(nb_cols) %}

                {% set categorie = [] %}
                {% set categorie = this_grid_categories[cat_matrix[i][j]] %}
                <div class="col">
                    <img id="categorie{{categorie[1]}}" class="d-block mx-auto img-thumbnail img-fluid" src=
                        {% if categorie[2] == 0 %}
                            "http://localhost:5000/static/img/sold-out.png"
                        {% else %}
                            {{ url_for('get_files.image_cat', id_photo=categorie[1]) }}
                        {% endif %}
                        style="background-image:
                        {% if categorie[2] == 0 %}
                             url({{ url_for('get_files.image_cat', id_photo=categorie[1]) }})
                        {% endif %}
                        ;"/>
                </div>

                {% endfor %}

                {% endif %}
            </div>

            {% endfor %}

        </div>
        {% endfor %}
    </div>

    <div class="col-1 align-items-center div-btn"  id="div-btn-next">
        <div class="row align-items-center div-btn" {% if nb_grid==1 %}style="display:none;"{% endif %}>
            <div class="col">
                <i class="fa fa-chevron-right fa-3x" id="btn-next" aria-hidden="true"></i>
            </div>
        </div>
    </div>

</div>

<!-- END OF CONTAINER -->

{% endblock main %}

{% block modals %}
<!-- Modal -->
{% for categorie in all_categories %}
<div class="modal fade" id="modalCategorie{{categorie[1]}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                {% if categorie[2] == 0 %}
                    {% if categorie[3] %}
                        <h5 class="modal-title" id="exampleModalLabel">
                            Il n'y a plus de {{ categorie[0] }} dans cette armoire 😞.<br />
                            Mais tu pourras en trouver en allant vers l'armoire située au
                            <strong>{{categorie[3]}}</strong>.
                        </h5>
                    {% else %}
                        <h5 class="modal-title" id="exampleModalLabel">
                            Il n'y a plus de {{ categorie[0] }} dans cette armoire 😞.<br />
                            Et il n'y en a plus nulle part ailleurs ! <br />
                            C'est dommage... Try again...
                        </h5>
                    {% endif %}
                {% else %}
                    <h5 class="modal-title" id="exampleModalLabel">
                        Êtes-vous sûr de bien vouloir emprunter un(e) {{ categorie[0] }} ?
                    </h5>
                {% endif %}
            </div>
            <div class="modal-footer">
                {% if categorie[2] == 0 %}
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                {% else %}
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Non, j'annule</button>
                    <a role="button" class="btn btn-primary" href="{{ url_for('product.get', id_armoire=id_armoire, id_cat=categorie[1]) }}">Oui j'emprunte !</a>
                {% endif %}
            </div>
        </div>
    </div>
</div>
{% endfor %}

<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="helpModalTitle">Que faire si ...</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mb-4">
                    <div class="col">
                        <h5>Le code-barre de mon objet n'est plus lisible</h5>
                    </div>
                    <div class="col">
                        <h5><small class="text-muted">Le donner à Valentin.</small></h5>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col">
                        <h5>J'ai perdu l'objet emprunté</h5>
                    </div>
                    <div class="col">
                        <h5><small class="text-muted">Signaler la perte dans le tableau récapitulatif des emprunts.</small></h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

{% endblock modals %}

{% block script %}

<script src="/static/js/nav.js"></script>

<script>


$(document).ready(function() {
    {% for categorie in all_categories %}
        $('#categorie{{categorie[1]}}').click(function() {
            $("#modalCategorie{{categorie[1]}}").modal();
        });
    {% endfor %}
});

socket.emit('connect');

socket.on('barcode', function(barcode) {
    location.href='/{{ id_armoire }}/give_back/'+barcode.value;
});

</script>
{% endblock script %}
