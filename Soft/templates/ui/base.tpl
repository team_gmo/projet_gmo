<!--
GiveMeOne - Une armoire connectée
Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
                   Corentin Vretman, Maxence Dehaut

Developped in French at ESEO, engineering school (IoT Major)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version. #}

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/css/roboto.css">
    <link rel="stylesheet" href="/static/css/style.css">
    <link rel="stylesheet" href="/static/css/animate.css">
    <link rel="stylesheet" href="/static/css/flappy.css">

    <script src="/static/js/jquery.min.js"></script>
    <script src="/static/js/popper.min.js"></script>
    <script src="/static/js/tether.min.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>
    <script src="/static/js/socket.io.min.js"></script>
    <script src="/static/js/flappy.js"></script>


    <title>GMO</title>
</head>

<body>


    <!-- Notification in case of event to display -->
    <!-- Raise a Boostrap alert in case of product issue registred -->
    <div class="alert m-0 alert-dismissible fade show text-center animated slideInDown"  id="notif_msg" role="alert" {% if notif_msg == "" %}style="display:none"{% endif %}>
        <h4>
            <strong>Ah!</strong>
            <span id="txt_error">{{ notif_msg }}</span>
        </h4>
    </div>

    <div id="autre_gros_sac">
        <div class="row" id="chargement">
            <ul>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
        <!-- HEADER -->

        <div class="row header">
            <div class="col logo">
                <div class="col text-right p-0">
                    {% block adminAccess %}<i class="fa fa-unlock-alt fa-3x ml-2 mt-2" id="admin_button" aria-hidden="true" onclick="location.href='{{url_for('admin_menu.index', id_armoire=id_armoire)}}'"></i>{% endblock adminAccess %}
                </div>
            </div>
            <div class="col-8 text-center">
                <div class="centrage_title">
                    <div class="triangle_left" data-toggle="modal" data-target="#flappyModal"></div>
                    <h4 class="between_triangle">{% block page_title %}{% endblock page_title %}</h4>
                    <div class="triangle_right"></div>
                </div>
            </div>
            <div class="col text-right p-0">
                <i class="fa fa-id-card-o fa-3x mr-2 mt-2" aria-hidden="true" data-toggle="modal" data-target="#userModal"></i>
                <i class="fa fa-question-circle fa-3x mr-2 mt-2" aria-hidden="true" data-toggle="modal" data-target="#helpModal"></i>
            </div>
        </div>
        <!-- END OF HEADER -->

        {% block main %}{% endblock main %}
        {% block footer %}{% endblock footer %}
    </div>

    {% block modals %}{% endblock modals %}

    <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Emprunt en cours</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-2 text-center user">
                            <img class="rounded img-fluid mb-2" src="">
                            <h5 id="name"></h5>
                            <h5 id="firstname"></h5>
                        </div>
                        <div class="col">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Produit</th>
                                            <th>Categorie</th>
                                            <th>Date d'emprunt</th>
                                        </tr>
                                    </thead>
                                    <tbody id="liste_emprunt">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="flappyModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog mt-2 mb-1" role="document">
            <div class="modal-content">
                <div class="wrapper">
                    <div class="window">
                        <p class="score">0</p>
                        <div class="bird"></div>

                        <div class="pipe hidden"></div>
                        <div class="pipe hidden"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<script type="text/javascript" charset="utf-8">

$("#gros_sac").hide();

var socket = io.connect('http://127.0.0.1:5000');

var wait4something = 0;

$( window ).on('beforeunload ', function() {
    $("#autre_gros_sac").show();
    $("#gros_sac").hide();
});

socket.on('error', function(response) {
    $("#txt_error").html(response.txt);
    if(response.txt != "") {
        $("#notif_msg").show();
    }
});

socket.on('user_connected', function(response) {
    if (!wait4something) {
        $("#autre_gros_sac").hide();
    }
    $("#gros_sac").hide();
    $("#enregistrement").hide();
    $("#unknown_card").hide();

    if (response.admin)
    {
        $("#admin_button").css('display', 'inline-block');
    } else {
        $("#admin_button").hide();
    }

    $(".user > #name").html(response.name);
    $(".user > #firstname").html(response.firstname);
    $(".user > img").attr("src", "/files/visage/"+response.userID+".png");

    for (var emprunt in response.emprunts)
    {
        var date = new Date(response.emprunts[emprunt][2]*1000);
        $("#liste_emprunt").append("<tr> \
                                        <td>"+response.emprunts[emprunt][0]+"</td> \
                                        <td>"+response.emprunts[emprunt][1]+"</td> \
                                        <td>"+date.getDay()+"/"+date.getMonth()+"/"+date.getFullYear()+"</td> \
                                    </tr>");
    }

});

socket.on('new_card', function(response) {
    $("#autre_gros_sac").hide();
    $("#gros_sac").hide();
    $("#enregistrement").show();
    $("#unknown_card").hide();
    $("#admin_button").hide();

    $(".modal").modal('hide');

    $("#name").html("");
    $("#firstname").html("");
    $("#liste_emprunt").html("")
    $(".user > img").attr("src", "");
});

socket.on('unknown_card', function(response) {
    $("#autre_gros_sac").hide();
    $("#gros_sac").hide();
    $("#enregistrement").hide();
    $("#unknown_card").show();
    $("#admin_button").hide();

    $(".modal").modal('hide');

    $("#name").html("");
    $("#firstname").html("");
    $("#liste_emprunt").html("")
    $(".user > img").attr("src", "");
});

socket.on('no_card', function(response) {
    $("#autre_gros_sac").hide();
    $("#enregistrement").hide();
    $("#unknown_card").hide();
    $("#gros_sac").show();
    $("#admin_button").hide();

    $(".modal").modal('hide');

    $("#name").html("");
    $("#firstname").html("");
    $("#liste_emprunt").html("")
    $(".user > img").attr("src", "");
});

</script>

{% block script %}{% endblock script %}

</html>
