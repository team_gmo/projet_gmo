{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "base.tpl" %}

{% block page_title %}ADMINISTRATEUR{% endblock page_title %}

{% block adminAccess %}<i class="fa fa-lock fa-3x ml-2 mt-2" id="admin_button" aria-hidden="true" onclick="location.href='{{ url_for('index.index_page', id_armoire=id_armoire) }}'"></i>{% endblock adminAccess %}

{% block main %}


<div class="container-fluid" style="padding-top:50px">
  <div class="col">
    <div class="row text-center placeholders">
      <div class="col-4 placeholder">
        <span style="font-size: 2.5rem">{{ nb_locked_locker }}</span>
        <h5>Casiers</h5>
        <span class="text-muted">Condamnés</span>
      </div>
        <div class="col-4 placeholder">
            <span style="font-size: 2.5rem">{{ issue_to_solve }}</span>
            <h5>Erreurs</h5>
            <span class="text-muted">Non Traitées</span>
        </div>
        <div class="col-4 placeholder">
          <span style="font-size: 2.5rem">{{ not_available }}</span>
          <h5>Produits</h5>
          <span class="text-muted">Hors Systèmes</span>
        </div>
    </div>
    <div class="row" style="height: 1em"></div>
    <div class="row" style="height: 1em"></div>
    <div class="row" style="height: 1em"></div>
    <div class="row" style="height: 1em"></div>
    <div class="row" style="text-align:center;">
      <div class="col"><a role="button" class="btn btn-primary" href="{{ url_for('admin_menu.lock_casier', id_armoire=id_armoire) }}" style="font-size: 1.2em">Condamner un casier</a></div>
      <div class="col"></div>
      <div class="col"><a role="button" class="btn btn-primary" href="{{url_for('admin_menu.solve_issue',id_armoire=id_armoire)}}" style="font-size: 1.2em">Régler un problème</a></div>
    </div>
  </div>
</div>


{% endblock main %}
