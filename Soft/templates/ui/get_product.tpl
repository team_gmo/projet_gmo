{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "base.tpl" %}

{% block page_title %}RECUPEREZ VOTRE PRODUIT{% endblock page_title %}

{% block main %}

<div class="container" style="padding-top:50px">
    <div class="row">
        <div class="col-8 align-self-center">

          {# Si le floor_pi est au dessus de l'armoire #}
          {% if floor_pi >= nb_floors %}
          <div class="row ligne" id="floor_pi">
            <div class="col casier logo bgcenter"></div>
            <div class="col casier"></div>
          </div>
          {% endif %}

          {# la boucle est inversée par soucis d'affichage des lignes #}
          {# de bas en haut pour un rendu correcte du réel : #}
          {# l'armoire est décrite par étage, donc la ligne 1 se trouve en bas #}
          {% for i in range(nb_floors-1,-1,-1) %}

            <div id="floor{{i+1}}" class="row ligne" style="height:{{300/nb_floors}}px;">

            {% for j in range(nb_cols[i]) %}
            <!-- chr(64+xxx) permet de convertir en Ascii et donc d'utiliser des lettres pour nommer les casiers -->
                <div id="fl{{chr(64+i+1)}}_{{j+1}}" class="col casier">{{chr(64+i+1)}}{{j+1}}</div>
            {% endfor %}

            </div>

            {% if i == floor_pi %}
              <div class="row ligne">
                <div class="col casier logo bgcenter" id="floor_pi"></div>
                <div class="col casier" id="floor_pi"></div>
              </div>
            {% endif %}

          {% endfor %}
        </div>
        <div class="col-4 align-self-center">
            <h5 class="text-center">
                Veuillez retirer votre produit dans le casier  <br /><strong id="casier"></strong><br /><br /><br /><br />
                Refermez le casier, et récupérez votre carte.
            </h5>
        </div>
    </div>
</div>

{% endblock main %}

{% block script %}
<script>
    socket.emit('get_product', { 'id_cat': {{ id_cat }}, 'id_armoire':  {{ id_armoire }}});

    socket.on('error', function(error) {
        location.href='/{{ id_armoire }}/' + error.txt;
    });

    socket.on('casier_opened', function(casier) {
        $("#fl"+casier.ligne+"_"+casier.colonne).addClass("selected");
        $("#casier").html(casier.ligne+" "+casier.colonne);
    });

     socket.on('casier_closed', function() {
         location.href='/{{ id_armoire }}';
    });
</script>
{% endblock script %}
