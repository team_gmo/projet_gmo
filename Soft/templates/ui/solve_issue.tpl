{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "base.tpl" %}

{% block page_title %}SOLVE ISSUE{% endblock page_title %}

{% block adminAccess %}<i class="fa fa-lock fa-3x ml-2 mt-2" id="admin_button" aria-hidden="true" onclick="location.href='{{ url_for('index.index_page', id_armoire=id_armoire) }}'"></i>{% endblock adminAccess %}

{% block main %}
<div class="row" style="height: 1em"></div>
<div class="row" style="height: 1em"></div>
<div class="row" style="height: 1em"></div>
<div class="col"><a role="button" class="btn btn-primary" href="{{url_for('admin_menu.index', id_armoire=id_armoire )}}" style="font-size: 1.2em">Retour</a></div>
<div class="container" style="padding-top:5px">
  <div class="row">
    <div id="subcontainer_left" class="col align-self-center" style="margin:0px; margin-left:-10px;">

      {# Si le floor_pi est au dessus de l'armoire #}
      {% if floor_pi >= nb_floors %}
        <div class="row ligne" id="floor_pi">
          <div class="col casier logo bgcenter"></div>
          <div class="col casier"></div>
        </div>
      {% endif %}

      {# la boucle est inversée par soucis d'affichage des lignes #}
      {# de bas en haut pour un rendu correcte du réel : #}
      {# l'armoire est décrite par étage, donc la ligne 1 se trouve en bas #}
      {% for i in range(nb_floors-1,-1,-1) %}

      <div id="floor{{i+1}}" class="row ligne" style="height:{{300/nb_floors}}px; color: black;">

        {% for j in range(nb_cols[i]) %}
            <a id="fl{{chr(i+65)}}_{{j+1}}" class="col casier
            {# On compare la liste de tuples des casiers soulevant une erreur aux incréments i,j #}
            {# on assigne la classe locked le cas échéant et bind avec le modal#}
            {% for tuples in signaled_issues %}
              {% if tuples == (i+1, j+1) %}
                locked" aria-hidden="true" data-toggle="modal" data-target="#modalIssue_{{i+1}}_{{j+1}}
              {% endif %}
            {% endfor %}
            "
            role="button" href="#">
            <!-- chr(64+xxx) permet de convertir en Ascii et donc d'utiliser des lettres pour nommer les casiers -->
            <div>{{chr(64+i+1)}}{{j+1}}</div>
            </a>
        {% endfor %}
      </div>

        {% if i == floor_pi %}
        <div class="row ligne" id="floor_pi">
          <div class="col casier logo bgcenter"></div>
          <div class="col casier"></div>
        </div>
        {% endif %}

      {% endfor %}
    </div>
    <div id="subcontainer_right" class="col align-self-center">

      <h5 class="text-center" style="color:white">Résumé des signalements</h5>

      <div class="row text-center placeholders" style="color: white;">
        <div class="col placeholder">
          <span style="font-size: 2rem">{{ not_available }}</span>
          <h6>Produits Hors Système</h6>
        </div>
      </div>

      <div class="row text-center placeholders" style="color: white;">
        <div class="col placeholder">
          <span style="font-size: 2rem">{{ issue_to_solve }}</span>
          <h6>Signalement non traités</h6>
        </div>
      </div>

    </div>
  </div>
</div>
{% endblock main %}

{% block modals %}

{% for i in range(nb_floors-1,-1,-1) %}
  {% for j in range(nb_cols[i]) %}
    <div class="modal fade" id="modalIssue_{{i+1}}_{{j+1}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalIssueTitle">Erreur signalée :</h5>
                </div>
                <div class="modal-body">
                  {% for tuples in signaled_issues %}
                    {% if tuples == (i+1, j+1) %}
                      <h5 class="modal-title" id="modalIssueTitle">{{ issues_com[loop.index-1] }}</h5>
                    {% endif %}
                  {% endfor %}
                </div>
                <div class="modal-footer">
                  <div class="row">
                    <button type="button" class="col btn btn-primary" data-dismiss="modal"><div>Retour</div></button>
                    <div class="col"></div>
                    <a role="button" class="col btn btn-primary" href="{{url_for('admin_menu.solve', id_armoire=id_armoire, l=i+1, c=j+1 )}}"><div>Récupérer le produit</div></a>
                    <div class="col"></div>
                  </div>
                </div>
            </div>
        </div>
    </div>
  {% endfor %}
{% endfor %}

{% endblock modals %}
