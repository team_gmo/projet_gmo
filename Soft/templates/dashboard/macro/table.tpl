{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% macro th(titre, taille_col, link="", sens=0, col_num=-1, col_actu=0) -%}
<th {% if link != "" %} class="col-{{ taille_col }}" onclick="location.href='{{ url_for(link, col=col_num, sens=sens) }}'" {% endif %}>
        {{ titre }}
        <span class="badge badge-light">{% if col_num == col_actu %}{% if sens %}⋁{% else %}⋀{% endif %}{% endif %}</span>
</th>
{%- endmacro %}

{% macro table(width=12) -%}
<div class="row mb-1">
    <div class="col-{{ width }}">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                {{ caller() }}
            </table>
        </div>
    </div>
</div>
{%- endmacro %}

{% macro head() -%}
<thead>
    <tr>
        {{ caller() }}
    </tr>
</thead>
{%- endmacro %}

{% macro body(nb_resultat_actu, nb_col) -%}
<tbody>
    {{ caller() }}
    {% if not(nb_resultat_actu) %}
    <tr class="text-center">
        <td colspan="{{ nb_col }}" >Aucun résultat 💔😒</td>
    </tr>
    {% endif %}
</tbody>
{%- endmacro %}
