{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% macro search(link, search, place_holder="Rechercher ...") -%}
<form method="post" action="{{ url_for(link) }}">
    <div class="row mb-4">
        <div class="col-10">
            <input class="form-control" type="text" placeholder="{{ place_holder }}" aria-label="Search" name="search" value="{{ search }}">
        </div>
        <div class="col-2 text-right">
            <button class="btn btn-outline-success" type="submit">Rechercher</button>
        </div>
    </div>
</form>
{%- endmacro %}

{% macro input_text(titre, name, placeholder, value="", textAddon=0) -%}
<div class="form-group row">
    <label for="{{ name }}" class="col-sm-3 col-form-label">{{ titre }}</label>
    <div class="col-sm-9">
        <div class="input-group">
            <input type="text" class="form-control" id="{{ name }}" name="{{ name }}" placeholder="{{ placeholder }}" value="{{ value }}" {% if textAddon %} aria-describedby="addon_{{ name }}" {% endif %}>
            {% if textAddon %}<span class="input-group-addon" id="addon_{{ name }}">{{ textAddon }}</span>{% endif %}
        </div>
    </div>
</div>
{%- endmacro %}

{% macro input_duoradio(titre, name, help1, help2, value=0) -%}
<div class="form-group row">
    <div class="col-sm-3">{{ titre }}</div>
    <div class="col-sm-9">
        <label class="custom-control custom-radio">
            <input id="{{ name }}1" name="{{ name }}" value=1 type="radio" class="custom-control-input form-control" {% if value==1 %} checked {% endif %}>
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">{{ help1 }}</span>
        </label>
        <label class="custom-control custom-radio">
          <input id="{{ name }}2" name="{{ name }}" value=0 type="radio" class="custom-control-input form-control" {% if value==0 %} checked {% endif %}>
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">{{ help2 }}</span>
        </label>
    </div>
</div>
{%- endmacro %}


{% macro input_file(titre, name) -%}
<div class="form-group row">
    <label for="{{ name }}" class="col-sm-3 col-form-label">{{ titre }}</label>
    <div class="col-sm-9">
        <div class="input-group">
            <label class="custom-file" style="width:100%;">
                <input type="file" id="{{ name }}" name="{{ name }}" class="custom-file-input form-control">
                <span class="custom-file-control"></span>
            </label>
        </div>
    </div>
</div>
{%- endmacro %}
