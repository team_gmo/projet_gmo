{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% macro title(titre, addButton_text="", addButton_modal="") -%}
<div class="row mb-4 trair">
    <div class="col">
        <h1>{{ titre }}</h1>
    </div>
    {% if addButton_text != "" -%}
    <div class="col text-right">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#{{ addButton_modal }}">{{ addButton_text }}</button>
    </div>
    {%- endif %}
</div>
{%- endmacro %}

{% macro resultat(nb_resultat_actu, nb_resultat) -%}
<div class="row mb-1">
    <div class="col">
        <b>{{ nb_resultat_actu }}</b> résultats affichés sur <b>{{ nb_resultat }}</b>
    </div>
</div>
{%- endmacro %}

{% macro pagination(link, nb_resultat, nb_max_result, page) -%}
<div class="row mb-1">
    <div class="col-12">
        <nav aria-label="...">
            <ul class="pagination justify-content-center">
                <li class="page-item {% if page == 1 %}disabled{% endif %}">
                    <a class="page-link" href="{{ url_for(link, num_page=page-1) }}">Précédent</a>
                </li>
                {% for pages in range(1, ((nb_resultat-1)//nb_max_result)+2) %}
                <li class="page-item {% if pages == page %}active{% endif %}"><a class="page-link" href="{{ url_for(link, num_page=pages) }}">{{ pages }}</a></li>
                {% endfor %}
                {% if not(nb_resultat) %}
                <li class="page-item active"><a class="page-link" href="{{ url_for(link, num_page=1) }}">1</a></li>
                {% endif %}
                <li class="page-item {% if page == ((nb_resultat-1)//nb_max_result)+1 or not(nb_resultat) %}disabled{% endif %}">
                    <a class="page-link" href="{{ url_for(link, num_page=page+1) }}">Suivant</a>
                </li>
            </ul>
        </nav>
    </div>
</div>
{%- endmacro %}
