{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "layout.tpl" %}

{% block utilisateurs %}active{% endblock %}

{% block main %}

{{ Mpage.title("Utilisateurs", "Ajouter un utilisateur", "addUserModal") }}

{{ Mform.search("user.search_by_post", search, "Rechercher avec un nom, un id, ....") }}

{{ Mpage.resultat(nb_resultat_actu, nb_resultat) }}

{% call Mtable.table(10) %}
    {% call Mtable.head() %}
        {{ Mtable.th("id", 1, 'user.trie', sens, 0, col) }}
        {{ Mtable.th("Carte id", 2, 'user.trie', sens, 1, col) }}
        {{ Mtable.th("Nom", 2, 'user.trie', sens, 2, col) }}
        {{ Mtable.th("Prenom", 2, 'user.trie', sens, 3, col) }}
        {{ Mtable.th("Incription", 2, 'user.trie', sens, 4, col) }}
        {{ Mtable.th("Emprunt en cours", 3, 'user.trie', sens, 5, col) }}
    {% endcall %}
    {% call Mtable.body(nb_resultat_actu, 6) %}
        {% for user in resultat %}
        <tr data-container="body" data-toggle="popover" data-trigger="hover" data-placement="right" data-html="true"  data-animation="false" data-content='<img src="/files/visage/{{ user[0] }}.png" height="180">'>
            <td data-toggle="modal" data-target="#editUser{{ user[0] }}Modal">{{ user[0] }}</td>
            <td data-toggle="modal" data-target="#editUser{{ user[0] }}Modal">{{ user[7] }}</td>
            <td data-toggle="modal" data-target="#editUser{{ user[0] }}Modal">{{ user[1] }}</td>
            <td data-toggle="modal" data-target="#editUser{{ user[0] }}Modal">{{ user[2] }}</td>
            <td>{{ timestamp2text(user[3]) }}</td>
            <td data-toggle="modal" data-target="#dataUser{{ user[0] }}Modal">{{ user[6] }}</td>
        </tr>
        {% endfor %}
    {% endcall %}
{% endcall %}

{{ Mpage.pagination("user.page", nb_resultat, nb_max_result, page) }}

{% endblock %}

{% block modal %}
<!-- Modal add user-->
<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="addUserModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addUserModalLabel">Ajouter un utilisateur</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url_for('user.add') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ Mform.input_text("Card ID", "cardid", "1a2b3c4d5e6f7g") }}
                    {{ Mform.input_text("Nom", "nom", "Fliste") }}
                    {{ Mform.input_text("Prénom", "prenom", "Elliot") }}
                    {{ Mform.input_file("Photo", "photo") }}
                    {{ Mform.input_duoradio("Admin", "admin", "Oui", "Non", 0) }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary">Ajouter</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal edit user-->
{% for user in resultat %}
<div class="modal fade" id="editUser{{ user[0] }}Modal" tabindex="-1" role="dialog" aria-labelledby="editUser{{ user[0] }}ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editUser{{ user[0] }}ModalLabel">{{ user[2] }} {{ user[1] }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url_for('user.edit') }}">
                <div class="modal-body">
                    {{ Mform.input_text("Card ID", "cardid", "123456", user[7]) }}
                    {{ Mform.input_text("Nom", "nom", "Dehaut", user[1]) }}
                    {{ Mform.input_text("Prénom", "prenom", "Maxence", user[2]) }}
                    {{ Mform.input_file("Photo", "photo") }}
                    {{ Mform.input_duoradio("Admin", "admin", "Oui", "Non", user[8]) }}
                    <input type="hidden" value="{{ user[0] }}" name="id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
{% endfor %}

<!-- Modal emprunt -->
{% for user in resultat %}
<div class="modal fade" id="dataUser{{ user[0] }}Modal" tabindex="-1" role="dialog" aria-labelledby="dataUser{{ user[0] }}ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dataUser{{ user[0] }}ModalLabel">{{ user[1] }} {{ user[2] }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <h6>Liste d'emprunt</h6>
                    </div>
                </div>
                {% call Mtable.table() %}
                    {% call Mtable.head() %}
                        <th>Produit</th>
                        <th>Categorie</th>
                        <th>Date d'emprunt</th>
                        <th>Date retour</th>
                    {% endcall %}
                    {% call Mtable.body(emprunts(user[0]), 4) %}
                        {% for emprunt in emprunts(user[0]) %}
                        <tr>
                            <td>{{ emprunt[0] }}</td>
                            <td>{{ emprunt[1] }}</td>
                            <td>{{ timestamp2text(emprunt[2]) }}</td>
                            <td>{{ timestamp2text(emprunt[3]) }}</td>
                        </tr>
                        {% endfor %}
                    {% endcall %}
                {% endcall %}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Quitter</button>
            </div>
        </div>
    </div>
</div>
{% endfor %}
{% endblock %}
