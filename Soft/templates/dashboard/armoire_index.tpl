{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "layout.tpl" %}

{% block armoires %}active{% endblock %}

{% block main %}

{{ Mpage.title("Armoires") }}

<div class="card-columns">
    {% for armoire in resultat %}
    <div class="card text-center {% if not armoire[3] %} border-danger {% endif %}" data-toggle="modal" data-target="#editArmoire{{ armoire[0] }}Modal">
        <div class="card-body">
            <h4 class="card-title">Armoire {{ armoire[0] }}</h4>
            <p class="card-text"><small class="text-muted">{{ armoire[2] }}</small></p>

            {# Si le floor_pi est au dessus de l'armoire #}
            {% if floor_pi >= nb_floors[armoire[0]-1] %}
              <div class="row ligne" id="floor_pi">
                <div class="col casier logo bgcenter"></div>
                <div class="col casier"></div>
              </div>
            {% endif %}

            {% for i in range(nb_floors[armoire[0]-1]-1,-1,-1) %}

              <div id="floor{{i+1}}" class="row ligne" style="height:{{300/nb_floors[armoire[0]-1]}}px;">

              {% for j in range(nb_cols[armoire[0]-1][i]) %}
                  <div id="fl{{i+1}}_{{j+1}}" class="col casier">{{get_data_casier(armoire[0], i+1, j+1)[0]}}</div>
              {% endfor %}

              </div>

              {% if i == floor_pi %}
                <div class="row ligne" id="floor_pi">
                  <div class="col casier logo bgcenter"></div>
                  <div class="col casier"></div>
                </div>
              {% endif %}
            {% endfor %}
        </div>
    </div>
    {% endfor %}
</div>

{% endblock %}

{% block modal %}
{% for armoire in resultat %}
<div class="modal fade" id="editArmoire{{ armoire[0] }}Modal" tabindex="-1" role="dialog" aria-labelledby="editArmoire{{ armoire[0] }}ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editArmoire{{ armoire[0] }}ModalLabel">Armoire {{ armoire[0] }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url_for('user.edit') }}">
                <div class="modal-body">
                    {{ Mform.input_text("Description", "description", "Marconi A223", armoire[2]) }}
                    {{ Mform.input_duoradio("Etat", "active", "Actif", "Inactif", armoire[3]) }}
                    <input type="hidden" value="{{ armoire[0] }}" name="id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
{% endfor %}
{% endblock %}
