<!--
GiveMeOne - Une armoire connectée
Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
                   Corentin Vretman, Maxence Dehaut

Developped in French at ESEO, engineering school (IoT Major)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version. #}

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

{% import 'macro/form.tpl' as Mform %}
{% import 'macro/table.tpl' as Mtable %}
{% import 'macro/page.tpl' as Mpage %}

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>GiveMeOne Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/static/css/animate.css">

    <!-- Custom styles for this template -->
    <link href="/static/css/dashboard.css" rel="stylesheet">
    <link href="/static/css/timeline.css" rel="stylesheet">

    <script src="/static/js/Chart.min.js"></script>
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="{{ url_for('accueil.index') }}">
              <img src="static/img/logo.png" width="40">
              Dashboard
            </a>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item {% block accueil %}{% endblock %}">
                      <a class="nav-link" href="{{ url_for('accueil.index') }}">Accueil</a>
                  </li>
                    <!-- <li class="nav-item {% block statistiques %}{% endblock %}">
                        <a class="nav-link" href="{{ url_for('statistique.index') }}">Statistiques</a>
                    </li> -->
                    <li class="nav-item {% block utilisateurs %}{% endblock %}">
                        <a class="nav-link" href="{{ url_for('user.index') }}">Utilisateurs</a>
                    </li>
                    <li class="nav-item {% block materiels %}{% endblock %}">
                        <a class="nav-link" href="{{ url_for('produit.index') }}">Matériels</a>
                    </li>
                    <li class="nav-item {% block categories %}{% endblock %}">
                        <a class="nav-link" href="{{ url_for('categorie.index') }}">Catégories</a>
                    </li>
                    <li class="nav-item {% block amoires %}{% endblock %}">
                        <a class="nav-link" href="{{ url_for('armoire.index') }}">Armoire</a>
                    </li>
                </ul>
            </div>
            <span class="mt-2 mt-md-0">
                <a class="btn btn-outline-light" href="{{ url_for('main.deconnexion') }}">Déconnexion</a>
            </span>
        </nav>
    </header>

    <div class="container-fluid">
        <div class="row justify-content-center">

            <main role="main" class="col-sm-9 col-md-10 pt-3">
                {% if erreur != "" %}
                <div class="animated slideInRight align-items-center mt-3 alert_over">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Erreur!</strong> {{ erreur }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                {% endif %}

                {% if info != "" %}
                <div class="animated slideInRight align-items-center mt-3 alert_over">
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <strong>Information !</strong> {{ info }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                {% endif %}

                {% block main %}{% endblock %}
            </main>
        </div>
    </div>

    {% block modal %}{% endblock %}
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- jQuery library -->
    <!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>-->

    <!-- Popper -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>-->

    <!-- Latest compiled and minified Bootstrap JavaScript -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>-->

    <script src="/static/js/jquery.min.js"></script>
    <script src="/static/js/popper.min.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>

    <script>
    $(function () {
        $('[rel="tooltip"]').tooltip()
        $('[data-toggle="popover"]').popover()
    })
    </script>

    {% block js %}{% endblock %}
</body>
</html>
