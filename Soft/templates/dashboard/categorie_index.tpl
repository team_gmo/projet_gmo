{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "layout.tpl" %}

{% block categories %}active{% endblock %}

{% block main %}

{{ Mpage.title("Catégories", "Ajouter une catégorie", "addCatModal") }}

{{ Mform.search("categorie.search", search, "Rechercher avec un nom, un id, ....") }}

{{ Mpage.resultat(nb_resultat_actu, nb_resultat) }}

{% call Mtable.table(10) %}
    {% call Mtable.head() %}
        {{ Mtable.th("id", 1, 'categorie.trie', sens, 0, col) }}
        {{ Mtable.th("Nom", 2, 'categorie.trie', sens, 1, col) }}
        {{ Mtable.th("Dimension (hxlxp)", 3) }}
        {{ Mtable.th("Empruntés", 1) }}
        {{ Mtable.th("Dans armoires", 1) }}
        {{ Mtable.th("En réserve", 1) }}
        {{ Mtable.th("Enregistrés", 1) }}
    {% endcall %}
    {% call Mtable.body(nb_resultat_actu, 7) %}
        {% for cat in resultat %}
        <tr data-container="body" data-toggle="popover" data-trigger="hover" data-placement="right" data-html="true"  data-animation="false" data-content='<img src="/files/cat/{{ cat[0] }}.png " height="180">'>
            <td data-toggle="modal" data-target="#editCat{{ cat[0] }}Modal">{{ cat[0] }}</td>
            <td data-toggle="modal" data-target="#editCat{{ cat[0] }}Modal">{{ cat[1] }}</td>
            <td>{{ cat[2] }}x{{ cat[3] }}x{{ cat[4] }}</td>
            <td> {{ cat[7] }} </td>
            <td> for </td>
            <td> it </td>
            <td> {{ cat[5] - cat[6] }} </td>
        </tr>
        {% endfor %}
    {% endcall %}
{% endcall %}

{{ Mpage.pagination("categorie.page", nb_resultat, nb_max_result, page) }}

{% endblock %}

{% block modal %}

<!-- Modal add cat-->
<div class="modal fade" id="addCatModal" tabindex="-1" role="dialog" aria-labelledby="addCatModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addCatModalLabel">Ajouter une catégorie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url_for('categorie.add') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ Mform.input_text("Titre", "titre", "Multimètre")}}
                    {{ Mform.input_text("Hauteur", "hauteur", "8", textAddon="cm") }}
                    {{ Mform.input_text("Largeur", "largeur", "11", textAddon="cm") }}
                    {{ Mform.input_text("Profondeur", "profondeur", "17", textAddon="cm") }}
                    {{ Mform.input_file("Photo", "photo") }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary" name="type_form" value="addCat">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal edit cat-->
{% for cat in resultat %}
<div class="modal fade" id="editCat{{ cat[0] }}Modal" tabindex="-1" role="dialog" aria-labelledby="editCat{{ cat[0] }}ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editCat{{ cat[0] }}ModalLabel">{{ cat[1] }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url_for('categorie.edit') }}" enctype="multipart/form-data">
                <input type="hidden" name="id" value="{{ cat[0] }}">
                <div class="modal-body">
                    {{ Mform.input_text("Titre", "titre", "Multimètre", value=cat[1]) }}
                    {{ Mform.input_text("Hauteur", "hauteur", "8", value=cat[2], textAddon="cm") }}
                    {{ Mform.input_text("Largeur", "largeur", "11", value=cat[3], textAddon="cm") }}
                    {{ Mform.input_text("Profondeur", "profondeur", "17", value=cat[4], textAddon="cm") }}
                    {{ Mform.input_file("Photo", "photo") }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary" name="type_form" value="addCat">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
{% endfor %}

{% endblock %}
