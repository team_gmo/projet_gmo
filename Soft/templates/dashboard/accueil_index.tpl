{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "layout.tpl" %}

{% block accueil %}active{% endblock %}

{% block main %}

{{ Mpage.title("Accueil") }}

<section class="row text-center placeholders">
    <div class="col-3 placeholder">
      <span class="cliquable big_number" onclick="location.href='{{ url_for('armoire.index') }}'">{{ nb_probleme }}</span>
      <h4>Produits</h4>
      <span class="text-muted">Nécessitant une maintenance</span>
    </div>
    <div class="col-3 placeholder">
      <span class="cliquable big_number" onclick="location.href='{{ url_for('produit.select', rqst = 4, col = 3) }}'">{{ nb_emprunt }}</span>
      <h4>Emprunts</h4>
      <span class="text-muted">En cours</span>
    </div>
    <div class="col-3 placeholder">
      <span class="cliquable big_number" onclick="location.href='{{ url_for('user.select', rqst = "%", col=0) }}'">{{ nb_user }}</span>
      <h4>Utilisateurs</h4>
      <span class="text-muted">Enregistrés</span>
    </div>
    <div class="col-3 placeholder">
        <span class="cliquable big_number" onclick="location.href='{{ url_for('produit.select', rqst = "%", col = 0) }}'">{{ nb_prod }}</span>
        <h4>Objets</h4>
        <span class="text-muted">Enregistrés</span>
    </div>
</section>

{% call Mtable.table() %}
    {% call Mtable.head() %}
        {{ Mtable.th("Nom", 3, 'accueil.trie', sens, 0, col) }}
        {{ Mtable.th("Prénom", 3, 'accueil.trie', sens, 1, col) }}
        {{ Mtable.th("Catégorie", 3, 'accueil.trie', sens, 2, col) }}
        {{ Mtable.th("Date", 3, 'accueil.trie', sens, 3, col) }}
    {% endcall %}
    {% call Mtable.body(nb_resultat_actu, 4) %}
        {% for ligne in resultat %}
        <tr>
            <td>{{ ligne[0] }}</td>
            <td>{{ ligne[1] }}</td>
            <td>{{ ligne[2] }}</td>
            <td>{{ timestamp2text(ligne[3]) }}</td>
        </tr>
        {% endfor %}
    {% endcall %}
{% endcall %}

{% endblock %}
