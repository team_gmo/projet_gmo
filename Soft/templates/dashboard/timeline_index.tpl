{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "layout.tpl" %}

{% block materiels %}active{% endblock %}

{% set etat = ["hors système",
               "créé(e)",
               "en réserve",
               "dans l'armoire",
               "emprunté(e)",
               "problème défini",
               "en réparation"]
%}

{% set badge_class = ["dark",
                      "",
                      "info",
                      "success",
                      "primary",
                      "danger",
                      "warning"]
%}

{% set badge_logo = ["fa-times",
                     "fa-plus",
                     "fa-archive",
                     "fa-sign-in",
                     "fa-sign-out",
                     "fa-exclamation-triangle",
                     "fa-wrench"]
%}

{% block main %}

<h1>Timeline de {{ tl[0][8] }}, tag {{ tl[0][9] }}</h1>
<div class="row mb-4">
    <div class="col-6">
        <a class="btn btn-outline-dark" href="{{ url_for('produit.index') }}">Retour produit</a>
    </div>
    <div class="col-6 text-right">
        <form class="form-inline mt-2 mt-md-0 justify-content-end" method="post" action="{{ url_for('timeline.index', id_prod=tl[0][9]) }}">
            <input type="date" class="form-control mr-sm-2" name="date_deb" placeholder="Date début">
            <input type="date" class="form-control mr-sm-2" name="date_fin" placeholder="Date fin">
            <button type="btn btn-primary my-2 my-sm-0" class="form-control">Filtrer</button>
        </form>
    </div>
</div>
<div class="container">
    <ul class="timeline">
        {% for tl_line in tl %}
        <li {% if tl_line[6] == 6 %} data-toggle="modal" data-target="#solveModal" {% endif %}>
            <div class="timeline-badge {{ badge_class[tl_line[6]] }}"><i class="fa {{ badge_logo[tl_line[6]] }}"></i></div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                    <h5 class="timeline-titl_linee">{{ tl_line[8] }} {{ etat[tl_line[6]] }}{% if tl_line[2] != None %} par <a class="cliquable" href="{{ url_for('user.select', rqst=tl_line[10], col=1) }}">{{ tl_line[2] }} {{ tl_line[1] }}</a>{% endif %}{% if tl_line[3] != None %} à l'emplacement A{{ tl_line[3] }} L{{ tl_line[4] }} C{{ tl_line[5] }}{% endif %}</h5>
                    <p><small class="text-muted"><i class="fa fa-clock-o"></i> {{ timestamp2text(tl_line[0]) }}</small></p>
                </div>
                <div class="timeline-body">
                    <p>
                        {% if tl_line[7] != None %}{{ tl_line[7] }} <br \>{% endif %}
                    </p>
                </div>
            </div>
        </li>
        {% endfor %}
    </ul>
</div>

{% endblock %}

{% block modal %}
<div class="modal fade" id="solveModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{ url_for('main.solve') }}">
                <div class="modal-body">
                    Ce produit est-il réparé <br/><br/>
                    <input type="hidden" name="id_produit" value="{{ tl[0][9] }}">
                    <div class="form-group">
                        <label for="solutionTA" class="sr-only">Solution</label>
                        <textarea class="form-control" id="solutionTA" name="com" rows="3" placeholder="Description de la réparation."></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
{% endblock %}
