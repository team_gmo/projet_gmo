{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "layout.tpl" %}

{% block materiels %}active{% endblock %}

{% set etat =["hors système",
              "créé",
              "en réserve",
              "dans l'armoire",
              "emprunté",
              "problème défini",
              "en réparation"
              ]
%}

{% set table_class = ["table-dark",
                      "",
                      "table-info",
                      "table-success",
                      "table-primary",
                      "table-danger",
                      "table-warning"]
%}

{% block main %}

{{ Mpage.title("Matériels", "Ajouter un produit", "addProduitModal") }}

{{ Mform.search("produit.search", search, "Rechercher avec un id, une catégorie, une date") }}

{{ Mpage.resultat(nb_resultat_actu, nb_resultat) }}

{% call Mtable.table() %}
    {% call Mtable.head() %}
        {{ Mtable.th("Tag", 1, 'produit.trie', sens, 0, col) }}
        {{ Mtable.th("Catégorie", 3, 'produit.trie', sens, 1, col) }}
        {{ Mtable.th("Emprunteur", 3, 'produit.trie', sens, 2, col) }}
        {{ Mtable.th("Casier associé", 3) }}
        {{ Mtable.th("Etat", 2, 'produit.trie', sens, 3, col) }}
    {% endcall %}
    {% call Mtable.body(nb_resultat_actu, 5) %}
        {% for ligne in resultat %}
        <tr class="{{ table_class[ligne[7]] }}">
            <td data-toggle="modal" data-target="#editProduit{{ligne[0]}}Modal">{{ligne[0]}}</td>
            <td data-toggle="modal" data-target="#editProduit{{ligne[0]}}Modal">{{ligne[1]}}</td>
            <td id="emprunt{{ ligne[0] }}" class="emprunt"
            {% if (ligne[3] == None and ligne[2] == None and ligne[4] == None and ligne[5] == None and ligne[6] == None) %}data-idProduit="{{ ligne[0] }}" contenteditable="true" data-container="body" data-toggle="popover" data-placement="left" data-html="true"  data-animation="false" data-content=''
            {% else %} data-toggle="modal" data-target="#giveBack{{ligne[0]}}Modal"
            {% endif %}>
                {% if (ligne[3] != None or ligne[2] != None) %}{{ligne[3]}} {{ligne[2]}}{% endif %}
            </td>
            <td>{% if (ligne[4] != None or ligne[5] != None or ligne[6] != None) %}A{{ligne[4]}} L{{ligne[5]}} C{{ligne[6]}}{% endif %}</td>
            <td class="cliquable" onclick="location.href='{{ url_for('timeline.index', id_prod=ligne[10]) }}'">{{ etat[ligne[7]] }}</td>
        </tr>
        {% endfor %}
    {% endcall %}
{% endcall %}

{{ Mpage.pagination("produit.page", nb_resultat, nb_max_result, page) }}

{% endblock %}

{% block modal %}
<!-- Modal add produit-->
<div class="modal fade" id="addProduitModal" tabindex="-1" role="dialog" aria-labelledby="addProduitModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addProduitModalLabel">Ajouter un produit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url_for('produit.add') }}">
                <div class="modal-body">
                    {{ Mform.input_text("ID", "id", "123456") }}
                    <div class="form-group row">
                        <label for="inputCat" class="col-sm-3 col-form-label">Catégorie</label>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <select class="form-control" id="inputCat" name="cat">
                                    {% for options in tab_cat %}
                                    <option value="{{ options[0] }}">{{ options[1] }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuler</button>
                    <button type="submit" class="btn btn-primary" name="type_form" value="addProduit">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal edit produit-->
{% for ligne in resultat %}
<div class="modal fade" id="editProduit{{ ligne[0] }}Modal" tabindex="-1" role="dialog" aria-labelledby="editProduit{{ ligne[0] }}ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editProduit{{ ligne[0] }}ModalLabel">{{ ligne[0] }} - {{ ligne[1] }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url_for('produit.edit') }}">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="inputIdEditProduit1" class="col-sm-2 col-form-label">ID</label>
                        <div class="col-sm-10">
                            <input type="text" name="id" class="form-control" id="inputIdEditProduit1" value="{{ ligne[0] }}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputCatEditProduit{{ ligne[0] }}" class="col-sm-2 col-form-label">Catégorie</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <select class="form-control" id="inputCatEditProduit{{ ligne[0] }}" name="cat" autocomplete="off">
                                    {% for options in tab_cat %}
                                    <option value="{{ options[0] }}" {% if options[0] == ligne[1] %}selected="selected"{% endif %}>{{ options[1] }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuler</button>
                    <button type="submit" class="btn btn-primary">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
{% endfor %}

<!-- Modal give back produit-->
{% for ligne in resultat %}
<div class="modal fade" id="giveBack{{ ligne[0] }}Modal" tabindex="-1" role="dialog" aria-labelledby="editProduit{{ ligne[0] }}ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editProduit{{ ligne[0] }}ModalLabel">{{ ligne[0] }} - {{ ligne[1] }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url_for('main.give_back') }}">
                <div class="modal-body">
                    Rendre le produit ?
                    <input type="hidden" name="id_produit" value="{{ ligne[0] }}">
                    <input type="hidden" name="id_user" value="{{ ligne[0] }}">
                    <div class="form-group">
                        <label for="problemeTA{{ ligne[0] }}" class="sr-only">Rendre</label>
                        <textarea class="form-control" id="problemeTA{{ ligne[0] }}" name="prbl" rows="3" placeholder="Décrire le problème du produit"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary">Valider</button>
                </div>
            </form>
        </div>
    </div>
</div>
{% endfor %}

{% endblock %}

{% block js %}

<script>
//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 10;  //time in ms, 5 second for example
var $input = $('.emprunt');

//on keyup, start the countdown
$input.on('keyup', function () {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping.bind(null, this), doneTypingInterval);
});

//on keydown, clear the countdown
$input.on('keydown', function () {
    clearTimeout(typingTimer);
});

//user is "finished typing," do something
function doneTyping (obj) {
    console.log($(obj).attr('id'));


    $.getJSON( "{{ url_for('api.search_user', search="") }}"+$(obj).text(), function( data ) {

        var txt = "<form method='post' action='{{ url_for('main.emprunt') }}'>"
        txt += '<input type="hidden" name="id_produit" value="'+$(obj).attr('data-idProduit')+'">';
        txt += '<div class="list-group">';
        $.each( data, function( key, val ) {
            txt += '<button type="submit" class="list-group-item list-group-item-action" value="'+val[0]+'" name="id_user">'+val[2]+" "+val[1]+'</button>';
        });

        txt += '</div>';
        txt += '</form>';
        $(obj).attr('data-content', txt);
        $(obj).popover('hide');
        $(obj).popover('show');
    });
}
</script>
{% endblock %}
