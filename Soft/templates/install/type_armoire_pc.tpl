{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% extends "layout_pc.tpl" %}

{% block main %}

<h3>Cette armoire ...</h3>
<p>
  <a class="btn btn-primary" href="#">
    Est la première de son réseau
  </a>
  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#jeSuisEsclave" aria-expanded="false" aria-controls="jeSuisEsclave">
    Doit se rattacher à un réseau existant
  </button>
</p>
<div class="collapse" id="jeSuisEsclave">
    <form class="form-inline">
        <div class="form-row mx-auto">
            <div class="col-auto">
                <div class="form-group">
                    <label class="mr-sm-2" for="inlineFormInputGroup">Adresse IP de l'armoire maitre</label>
                    <div class="input-group mb-2 mb-sm-0">
                        <div class="input-group-addon">http://</div>
                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="192.168.144.4">
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary">Se connecter</button>
            </div>
        </div>
    </form>
</div>

{% endblock %}
