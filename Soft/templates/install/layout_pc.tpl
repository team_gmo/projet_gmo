{# GiveMeOne - Une armoire connectée #}
{# Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari, #}
{#                    Corentin Vretman, Maxence Dehaut #}

{# Developped in French at ESEO, engineering school (IoT Major) #}

{# This program is free software: you can redistribute it and/or modify #}
{# it under the terms of the GNU General Public License as published by #}
{# the Free Software Foundation, either version 3 of the License, or #}
{# any later version. #}

{# This program is distributed in the hope that it will be useful, #}
{# but WITHOUT ANY WARRANTY; without even the implied warranty of #}
{# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the #}
{# GNU General Public License for more details. #}

{# You should have received a copy of the GNU General Public License #}
{# along with this program.  If not, see <http://www.gnu.org/licenses/>. #}

{% import 'macro/form.tpl' as Mform %}
{% import 'macro/table.tpl' as Mtable %}
{% import 'macro/page.tpl' as Mpage %}

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Installation</title>

    <!-- Bootstrap core CSS -->
    <link href="/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="/static/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/static/css/animate.css">

    <!-- Custom styles for this template -->
    <link href="/static/css/dashboard.css" rel="stylesheet">
    <link href="/static/css/timeline.css" rel="stylesheet">
</head>

<body>
    <div class="container-fluid">
        <div class="row justify-content-center">

            <main role="main" class="col-sm-4 col-md-6 pt-3 text-center">
                {% if erreur != "" %}
                <div class="animated slideInRight align-items-center mt-3 alert_over">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Erreur!</strong> {{ erreur }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                {% endif %}

                {% if info != "" %}
                <div class="animated slideInRight align-items-center mt-3 alert_over">
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <strong>Information !</strong> {{ info }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                {% endif %}

                <h1>Installateur <img src="/static/img/logo.png" height="55px"></h1>

                {% block main %}{% endblock %}
            </main>
        </div>
    </div>

    {% block modal %}{% endblock %}

    <script src="/static/js/jquery.min.js"></script>
    <script src="/static/js/popper.min.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>

    <script>
    $(function () {
        $('[rel="tooltip"]').tooltip()
        $('[data-toggle="popover"]').popover()
    })
    </script>

    {% block js %}{% endblock %}
</body>
</html>
