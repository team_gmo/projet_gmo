// GiveMeOne - Une armoire connectée
// Copyright (C) 2018 Pierre-Emmanuel Colas, Matthieu Hennin, Matthieu Cesari,
//                    Corentin Vretman, Maxence Dehaut
//
// Developped in French at ESEO, engineering school (IoT Major)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version. #}
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. #}
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

$(document).ready(function() {

    // Dismiss bootstrap alerts on click anywere in the page
    $("body").click(function() {
        $(".alert").alert("close");
    });

    // On recupere le nombre de grid existantes
    var number_grids = $('[id^=grid]').length;

    // On set un increment qui varira selon la grid affichee
    var current_grid = 1;

    var effect = 0;
    //console.log("1.3");

    // Cache toutes les Grids au chargement '[id^=grid]'
    // $('[id^=grid]').style.display = 'block';
    // force l'affichage de la grid 1 au boot
    document.getElementById('grid1').style.display = 'block';

    // Au chargement de la page, on set le btn-prev en hidden
    document.getElementById("div-btn-prev").style.visibility = "hidden";

    // function onclick on Prev btn
    $('#div-btn-prev').on('click', function() {
        // Sur click, on cache la grid courrante et affiche la n-1. Puis decremente current_grid
        $("#grid" + current_grid).hide(0);
        $("#grid" + (current_grid - 1)).show(0);
        current_grid--;

        // Debug d'un decalage de current_grid en dehors des grides existantes
        if ((current_grid < 1) || (current_grid > number_grids + 1)) {

            $("[id^=grid]").hide(0);
            $("#grid1").show(0);
            current_grid = 1;
        }

        // affichage ou non du btn-prev selon ou l'on se trouve
        if (current_grid == 1) {
            document.getElementById("div-btn-prev").style.visibility = "hidden";
        } else {
            document.getElementById("div-btn-prev").style.visibility = "visible";
        }

        if(number_grids == 1){
            document.getElementById("div-btn-next").style.visibility = "hidden";
        } else {
            document.getElementById("div-btn-next").style.visibility = "visible";
        }
    });

    // function onclick on Next btn
    $('#div-btn-next').on('click', function() {

        if(document.getElementById("div-btn-next").style.visibility == "visible" || document.getElementById("div-btn-next").style.visibility == "")
        {
            $("#grid" + current_grid).hide(0);
            $("#grid" + (current_grid + 1)).show(0);
            current_grid++;

            // Debug d'un decalage de current_grid en dehors des grids existantes
            if ((current_grid < 1) || (current_grid > number_grids + 1)) {
                $("[id^=grid]").hide(0);
                $("#grid1").show(0);
                current_grid = 1;
            }

            // affichage ou non du btn-next selon ou l'on se trouve
            // et cache du btn-prev si scenario de debug (voir ci-dessus)
            document.getElementById("div-btn-prev").style.visibility = "visible";
            if (current_grid == number_grids) {
                document.getElementById("div-btn-next").style.visibility = "hidden";
            } else if (current_grid != number_grids) {
                document.getElementById("div-btn-next").style.visibility = "visible";
            } else if (current_grid == 1) {
                document.getElementById("div-btn-prev").style.visibility = "hidden";
            }
        }
    });
});
