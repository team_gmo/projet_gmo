"""
Le serveur python est le coeur de l'application.

Il génère avec Jinja2 les pages html nécessaires, configure les routes \
associées et communique avec les pages html pour gérer les évènements

:Exemple:

>>> @app.route('/url')
>>> def function_name():
>>>     Instructions


>>> @socketio.on('eventName')
>>> def handle_eventName():
>>>     Instructions
"""

from threading import Lock

import os
import time

from flask import Flask
from flask_socketio import SocketIO

from blueprints.common import api, get_files

from blueprints.ui import (
    index,
    product,
    product_issue,
    admin_menu
)


from blueprints.dashboard import (
    main,
    produit,
    categorie,
    accueil,
    statistique,
    timeline,
    user as userWeb,
    armoire as armoireWeb
)

from blueprints.install import (
    type_armoire
)

from GMO import (
    armoire,
    rfid,
    casier,
    begnaud,
    config,
    bdd,
    user,
    led,
    global_var,
    camera
)

app = Flask(__name__)

app.register_blueprint(api, url_prefix='/api')

app.register_blueprint(index)
app.register_blueprint(product)
app.register_blueprint(get_files)
app.register_blueprint(admin_menu)
app.register_blueprint(product_issue)


app.register_blueprint(main, url_prefix='/admin')
app.register_blueprint(produit, url_prefix='/admin/produit')
app.register_blueprint(categorie, url_prefix='/admin/categorie')
app.register_blueprint(accueil, url_prefix='/admin/accueil')
app.register_blueprint(statistique, url_prefix='/admin/statistique')
app.register_blueprint(timeline, url_prefix='/admin/timeline')
app.register_blueprint(userWeb, url_prefix='/admin/user')
app.register_blueprint(armoireWeb, url_prefix='/admin/armoire')

app.register_blueprint(type_armoire, url_prefix='/install')

app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

thread_rfid_user = None
thread_rfid_user_lock = Lock()

thread_rfid_product = None
thread_rfid_product_lock = Lock()

thread_barcode = None
thread_barcode_lock = Lock()

thread_user_data = None
thread_user_data_lock = Lock()

cardID_prec_lock = Lock()
cardID_prec = "-1"

productID_prec_lock = Lock()
productID_prec = "-1"
productID = "0"


def rfid_user_thread():
    """La tache surveillant le lecteur NFC."""

    # Configuration for a Raspberry Pi:
    param = config.readParam('rfid_user.portsPI')
    cs = param['CS']
    mosi = param['MOSI']
    miso = param['MISO']
    sclk = param['SCLK']
    print("let's connect to rfid user")

    while True:
        try:
            global_var.cardID = rfid.getTagID(cs, mosi, miso, sclk)
        except:
            global_var.cardID = "0"


def rfid_product_thread():
    """La tache surveillant le lecteur NFC."""
    global productID

    # Configuration for a Raspberry Pi:
    param = config.readParam('rfid_product.portsPI')
    cs = param['CS']
    mosi = param['MOSI']
    miso = param['MISO']
    sclk = param['SCLK']
    print("let's connect to rfid product")

    while True:
        try:
            productID = rfid.getTagID(cs, mosi, miso, sclk)
        except:
            productID = "0"


def user_data_thread():
    """Check le statue de la SD et envoi ça à la page web."""
    global cardID_prec
    while True:
        if global_var.cardID != "0" and cardID_prec != global_var.cardID:
            with cardID_prec_lock:
                cardID_prec = global_var.cardID
            print(global_var.cardID)
            user_data = begnaud.send("user", "get_by_card", [global_var.cardID])
            if user_data:
                emprunts = begnaud.send(
                    "user",
                    "get_pending_emprunt",
                    [user_data[2]]
                )
                print("socketio.emit ==> user_connected")
                socketio.emit(
                    'user_connected',
                    {
                        'name': user_data[0],
                        'firstname': user_data[1],
                        'userID': user_data[2],
                        'admin': user_data[3],
                        'emprunts': emprunts
                    })
            else:
                if global_var.cardID != "0":
                    print("socketio.emit ==> unknown_card")
                    socketio.emit('unknown_card')
                    cardPicture = camera.getCardPicture()
                    if cardPicture is not None:
                        print("socketio.emit ==> new_card")
                        socketio.emit('new_card')
                        if user.create(global_var.cardID, cardPicture):
                            with cardID_prec_lock:
                                cardID_prec = "0"
                        else:
                            print("socketio.emit ==> no_card")
                            socketio.emit('no_card')
                    else:
                        print("socketio.emit ==> no_card")
                        socketio.emit('no_card')
                else:
                    print("socketio.emit ==> no_card")
                    socketio.emit('no_card')
        elif cardID_prec != global_var.cardID:
            with cardID_prec_lock:
                cardID_prec = "0"
            socketio.emit('no_card')


def barcode_thread():
    """La tache surveillant le lecteur NFC."""
    global productID_prec
    while True:
        time.sleep(0.1)
        if (productID != "0" and productID_prec != productID):
            socketio.emit('barcode', {'value': productID})
        productID_prec = productID


@socketio.on('connect')
def handle_connect():
    """Quand une page index se connecte on l'ajoute dans notre tableau."""
    global thread_rfid_user, thread_rfid_product, thread_barcode, cardID_prec
    global thread_user_data

    with thread_rfid_user_lock:
        if thread_rfid_user is None:
            thread_rfid_user = socketio.start_background_task(
                target=rfid_user_thread)
    with thread_rfid_product_lock:
        if thread_rfid_product is None:
            thread_rfid_product = socketio.start_background_task(
                target=rfid_product_thread)
    with thread_barcode_lock:
        if thread_barcode is None:
            thread_barcode = socketio.start_background_task(
                target=barcode_thread)
    with thread_user_data_lock:
        if thread_user_data is None:
            thread_user_data = socketio.start_background_task(
                target=user_data_thread)
    with cardID_prec_lock:
        cardID_prec = "-1"


@socketio.on('get_product')
def get_product(data):
    """Quand une page index se connecte on l'ajoute dans notre tableau."""
    # Link to user, unlink from casier, return: id_prd, ligne, colonne
    id_armoire = data['id_armoire']
    id_cat = data['id_cat']

    borrowed_product = begnaud.send(
        "product",
        "emprunt",
        [global_var.cardID, id_cat, id_armoire]
    )

    # On error of missing data
    if borrowed_product is None:
        socketio.emit('error', {'txt': "Ce produit n'est pas disponible."})
        return

    id_casier = borrowed_product[3]
    ligne = borrowed_product[1]
    colonne = borrowed_product[2]
    socketio.emit(
        'casier_opened',
        {'ligne': chr(64 + ligne), 'colonne': colonne, 'id': id_casier}
    )
    casier.open(id_casier)
    socketio.emit('casier_closed')


@socketio.on('giveback_product')
def giveback_product(data):
    """Quand une page index se connecte on l'ajoute dans notre tableau."""
    # Link to user, unlink from casier, return: id_prd, ligne, colonne
    tag_id = data['id_produit']
    if global_var.cardID != "0":
        user_data = begnaud.send("user", "get_by_card", [global_var.cardID])
        notif_msg = begnaud.send("product", "give_back", [tag_id, user_data[3]])
    else:
        notif_msg = begnaud.send("product", "give_back", [tag_id, 0])

    # On error of missing data
    if notif_msg is None:
        socketio.emit(
            'error',
            {'txt': "Erreur inconnue lors du rendu de produit."}
        )
        return

    if notif_msg != "":
        print("give_back error :", notif_msg)
        socketio.emit('error', {'txt': notif_msg})
        return

    # récupération du casier assigné au produit
    id_produit = begnaud.send("product", "convert_id", [tag_id])
    if id_produit is None:
        socketio.emit('error', {'txt': "Produit inconnu"})
    id_produit = id_produit[0]
    assigned_casier = begnaud.send("casier", "get_by_id_produit", [id_produit])

    id_casier = assigned_casier[2]
    ligne = assigned_casier[0]
    colonne = assigned_casier[1]
    socketio.emit(
        'casier_opened',
        {'ligne': chr(64 + ligne), 'colonne': colonne}
    )
    casier.open(id_casier)
    socketio.emit('casier_closed')


@socketio.on('product_issue')
def declareError(data):
    """Redirect to the according error reporting page."""
    tag_id = data['id_produit']
    panne = data['panne']

    # récupération du casier assigné au produit
    id_produit = begnaud.send("product", "convert_id", [tag_id])
    if id_produit is None:
        socketio.emit('error', {'txt': "Produit inconnu"})
        return
    id_produit = id_produit[0]

    # get locker id
    id_casier = casier.get_by_id_produit(id_produit)

    user_data = begnaud.send(
        "big_brother",
        "get_last_emprunteur_for_an_object",
        [id_produit]
    )

    if user_data is None:
        socketio.emit('error', {'txt': "User inconnu"})
        return
    user_data = user_data[0]

    if id_casier is None:
        socketio.emit('error', {'txt': "Casier inconnu"})
        return
    id_casier = id_casier[2]

    # store issue into database
    notif_msg = begnaud.send(
        "product",
        "declare_prbl_user",
        [user_data, id_produit, id_casier, panne]
    )

    if notif_msg is None:
        socketio.emit(
            'error',
            {'txt': "Erreur inconnue lors de la déclartion de prbl."}
        )
        return

    # On error of missing data
    if notif_msg != "":
        socketio.emit('error', {'txt': notif_msg})
        return

    socketio.emit("prbl_saved")


if __name__ == '__main__':
    if not os.path.exists("./config"):
        os.mkdir("./config")
    if not os.path.isfile("./config/param.json"):
        config.setDefault()
    if not os.path.isfile("gmo.db"):
        bdd.initialise()
    if armoire.get_all() == []:
        print("generation keys ....")
        begnaud.register_master()
        print("end !")

    led.init()

    socketio.run(app, debug=True, log_output=False, host='0.0.0.0')
